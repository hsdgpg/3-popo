package com.yedam.app;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.popo.app.show.controller.PaymentController;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PopoApplication {


	
	@Test
	void contextLoads() {
		String[] dataList = {"jdbc:oracle:thin:@3.94.8.146:1521/xe","dev","dev"};
		 PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
	        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
	        config.setPassword("popo");
	        config.setAlgorithm("PBEWithMD5AndDES");
	        config.setKeyObtentionIterations("1000");
	        config.setPoolSize("1");
	        config.setProviderName("SunJCE");
	        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
	        config.setIvGeneratorClassName("org.jasypt.iv.NoIvGenerator");
	        config.setStringOutputType("base64");
	        encryptor.setConfig(config);
	        
	        for(String data : dataList) {
	        	String encryptText = encryptor.encrypt(data);
	        	System.out.println(encryptText+"??");
	        }
	
	}
	


}
