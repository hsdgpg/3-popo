package com.popo.app.messagingstompwebsocket.service;

import lombok.Data;

@Data
public class ChatVO {
	private String memId;
	private String content;
}
