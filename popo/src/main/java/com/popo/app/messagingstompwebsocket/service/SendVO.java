package com.popo.app.messagingstompwebsocket.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor //필드초기화
@NoArgsConstructor  //인수가 어쩌고 ..
public class SendVO {
	String memId;
	String content;
}
