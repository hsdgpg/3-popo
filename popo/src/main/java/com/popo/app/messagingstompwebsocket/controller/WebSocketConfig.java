package com.popo.app.messagingstompwebsocket.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
	
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		// TODO Auto-generated method stub
		 registry.addEndpoint("/chatServer").withSockJS();  //채팅서버 접속요청
	}

	@Override  
	public void configureMessageBroker(MessageBrokerRegistry config) {
		// TODO Auto-generated method stub
	    config.enableSimpleBroker("/topic");  //구독신청
	    config.setApplicationDestinationPrefixes("/app"); //메세지 전송
	}

}
