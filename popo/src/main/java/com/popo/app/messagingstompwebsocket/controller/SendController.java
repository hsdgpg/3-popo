package com.popo.app.messagingstompwebsocket.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import com.popo.app.messagingstompwebsocket.service.ChatVO;
import com.popo.app.messagingstompwebsocket.service.SendVO;


@Controller
public class SendController {
	  @MessageMapping("/hello")//메세지 요청
	  @SendTo("/topic/chat") //토픽신청한 사람 모두에게 메세지 전송
	  public SendVO sendVO(ChatVO message) throws Exception {
	    Thread.sleep(1000); // 1초 지연
	    return new SendVO(HtmlUtils.htmlEscape(message.getMemId()), HtmlUtils.htmlEscape(message.getContent()));  //자동으로 json전송
	  }
}
