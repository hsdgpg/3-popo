package com.popo.app.messagingstompwebsocket.service;

import java.sql.Date;

import lombok.Data;

@Data
public class SpringChatVO {
	String teamName;
	String memId;
	String chatContent;
	Date sendDate;
}
