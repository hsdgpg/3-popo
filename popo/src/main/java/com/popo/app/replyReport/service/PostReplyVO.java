package com.popo.app.replyReport.service;

import java.util.Date;

import com.popo.app.team.service.TeamVO;

import lombok.Data;

@Data
public class PostReplyVO {

//	게시글 신고	
//	POST_REPORT_NO
//	POST_REPORT_CATEGORY
//	NOTICE_NO
//	MEM_NO
//	POST_REPORT_REASON
//	POST_REPORT_CONTENT
//	POST_REPORT_STATUS
//	POST_REPORT_DATE
	
	int postReportNo;
	String postReportCategory;
	int noticeNo;
	Long memNo;
	String postReportReason;
	String postReportContent;
	int postReportStatus;
	Date postReportDate;
	
	
	int practiceNo;
}
