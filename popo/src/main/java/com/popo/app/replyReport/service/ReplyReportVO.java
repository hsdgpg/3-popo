package com.popo.app.replyReport.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class ReplyReportVO {

	int rn;
	String commentReportReason;
	String memName;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date commentReportDate;
	int noticeNo;
	int commentNo;
	Long memNo;
	int teamNo;
	
	String commentReportContent;
}
