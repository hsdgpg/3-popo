package com.popo.app.funding.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class FundingVO {
	
	int fundingNo;
	String fundingName;
	int teamNo;
	String fundingArtist;
	long memNo;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date fundingStart;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date fundingEnd;
	int fundingCPrice;
	int fundingPrice;
	String fundingImage;
	public String fundingContent;
	String fundingRecognize;
	String fundingRDate;
	int fundingStatus;
	String fundingGenre;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date fundingDay;
	
	int rn;
	String memName;
	
	int success; // 펀딩금액 달성률
	int rownum; // 펀딩달성률 순위
	
	int payNo;
	int teamNum;
	
	String merchantUid;
	String impUid;
	
	
	
	
}
