package com.popo.app.funding.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.popo.app.funding.mapper.FundingMapper;
import com.popo.app.funding.service.FundingJJimVO;
import com.popo.app.funding.service.FundingReplyVO;
import com.popo.app.funding.service.FundingReplyVO;
import com.popo.app.funding.service.FundingService;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.Criteria;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@AllArgsConstructor
@Log4j
@Service
public class FundingServiceImpl implements FundingService {
	
	@Autowired
	FundingMapper fundingMapper;

	@Override
	public List<MemberVO> insertFundingForm(MemberVO memVO) {
		// TODO Auto-generated method stub
		return fundingMapper.insertFundingForm(memVO);
	}

	//펀딩신청
	@Override
	public int insertFunding(FundingVO fundingVO) {
		int result = fundingMapper.insertFunding(fundingVO);
		return result;
	}


	//펀딩전체리스트-메인
	@Override
	public List<FundingVO> fundingList() {
		
		return fundingMapper.fundingList();
	}
	//펀딩랭킹
	@Override
	public List<FundingVO> fundingRanking() {
		
		return fundingMapper.fundingRanking();
	}

	@Override
	public List<FundingVO> fundingDrama(Criteria cri) {
		
		return fundingMapper.fundingDrama( cri);
	}

	@Override
	public int getDramaTotal(Criteria cri) {
		
		return fundingMapper.getDramaTotal(cri);
	}

	@Override
	public List<FundingVO> fundingKpop(Criteria cri) {
		// TODO Auto-generated method stub
		return fundingMapper.fundingKpop(cri);
	}

	@Override
	public int getKpopTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return fundingMapper.getKpopTotal(cri);
	}

	@Override
	public List<FundingVO> fundingDance(Criteria cri) {
		// TODO Auto-generated method stub
		return fundingMapper.fundingDance(cri);
	}

	@Override
	public int getDanceTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return fundingMapper.getDanceTotal(cri);
	}

	@Override
	public List<FundingVO> fundingMusical(Criteria cri) {
		// TODO Auto-generated method stub
		return fundingMapper.fundingMusical(cri);
	}

	@Override
	public int getMusicalTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return fundingMapper.getMusicalTotal(cri);
	}
	//펀딩-상세화면
	@Override
	public FundingVO fundingDetails(int fundingNo){
		// TODO Auto-generated method stub
		return fundingMapper.fundingDetails(fundingNo);
	}

	//펀딩-결제
	@Override
	public int fundingPayment(FundingVO fundingVO) {
		// TODO Auto-generated method stub
		int result = fundingMapper.fundingPayment(fundingVO);
		//현재까지 모인금액 변경
		fundingMapper.updateFundingCPrice(fundingVO);
		//팀 1/n
		fundingMapper.teamAdjustment(fundingVO);
		//관리자 수수료
		fundingMapper.adminAdjustment(fundingVO);
		
		return result;
	}
	@Override
	public FundingVO getJJim(int fundingNo, Long memNo) {
		// TODO Auto-generated method stub
		return fundingMapper.getJJim(fundingNo,memNo);
	}


	@Override
	public void insertFundingJJim(FundingVO fundingVO) {
		
		fundingMapper.insertFundingJJim(fundingVO);
	}

	@Override
	public void fundingJJimCancel(FundingVO fundingVO) {
		
		fundingMapper.fundingJJimCancel(fundingVO);
	}



}
