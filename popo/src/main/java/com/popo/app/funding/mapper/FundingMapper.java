package com.popo.app.funding.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.Criteria;



@Mapper
public interface FundingMapper {
	
	//펀딩신청 
	public int insertFunding(FundingVO fundingVO);
	
	//펀딩신청서식
	public List<MemberVO> insertFundingForm(MemberVO memVO);
	
	//펀딩리스트
	public List<FundingVO> fundingList();
	
	//펀딩 랭킹
	public List<FundingVO> fundingRanking();
	
	//연극
	public List<FundingVO> fundingDrama(Criteria cri);
	public int getDramaTotal(Criteria cri); 
	
	//Kpop
	public List<FundingVO> fundingKpop(Criteria cri);
	public int getKpopTotal(Criteria cri); 
	
	//댄스
	public List<FundingVO> fundingDance(Criteria cri);
	public int getDanceTotal(Criteria cri); 
	
	//펀딩뮤지컬
	public List<FundingVO> fundingMusical(Criteria cri);
	public int getMusicalTotal(Criteria cri); 
		
	//펀딩 상세화면
	public FundingVO fundingDetails(int fundingNo);
	
	//펀딩결제
	public int fundingPayment(FundingVO fundingVO);
	
	//현재까지 모인 금액 변경
	public void updateFundingCPrice(FundingVO fundingVO);
	
	//팀정산
	public void teamAdjustment(FundingVO fundingVO);
	
	//관리자 정산
	public void adminAdjustment(FundingVO fundingVO);
	
	//찜 여부
	public FundingVO getJJim(int fundingNo, Long memNo);
	// 찜 목록 추가
	public void insertFundingJJim(FundingVO fundingVO);
	// 찜 해제하기
	public void fundingJJimCancel(FundingVO fundingVO);

	
}
