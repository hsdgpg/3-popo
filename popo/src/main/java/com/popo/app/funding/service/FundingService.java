package com.popo.app.funding.service;

import java.util.List;

import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.Criteria;

public interface FundingService {

	//펀딩신청 
	public int insertFunding(FundingVO fundingVO);
	
	//펀딩신청서식
	public List<MemberVO> insertFundingForm(MemberVO memVO);
	
	//펀딩리스트
	public List<FundingVO> fundingList();
	
	
	//펀딩랭킹
	public List<FundingVO> fundingRanking();
	
	//펀딩드라마 목록
	public List<FundingVO> fundingDrama (Criteria cri);
		
	//펀딩드라마 페이징
	public int getDramaTotal(Criteria cri); 
	
	//펀딩Kpop 목록
	public List<FundingVO> fundingKpop(Criteria cri);
	
	//펀딩KPOP 페이징
	public int getKpopTotal(Criteria cri); 
	
	//펀딩댄스 목록
	public List<FundingVO> fundingDance(Criteria cri);
		
	//펀딩댄스 페이징
	public int getDanceTotal(Criteria cri); 
	
	//펀딩뮤지컬 목록
	public List<FundingVO> fundingMusical(Criteria cri);
	
	//펀딩뮤지컬 페이징
	public int getMusicalTotal(Criteria cri); 
		
	//펀딩 상세화면
	public FundingVO fundingDetails(int fundingNo);
	//펀딩결제
	public int fundingPayment(FundingVO fundingVO);

	//찜 여부
	public FundingVO getJJim(int fundingNo, Long memNo);
	
	// 찜 목록 추가
	public void insertFundingJJim(FundingVO fundingVO);
	// 찜 해제하기
	public void fundingJJimCancel(FundingVO fundingVO);

			
	
	
 
			
	
}
