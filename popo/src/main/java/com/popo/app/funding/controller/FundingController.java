package com.popo.app.funding.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.popo.app.funding.service.FundingJJimVO;
import com.popo.app.funding.service.FundingService;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.show.service.ShowVO;


//오수현
//펀딩관리

@Controller
public class FundingController {

	@Autowired
	FundingService fundingService;

	// 펀딩신청서	
	@GetMapping("/funding/fundingInsertForm")
	public String insertFundingForm(HttpSession session) {

		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		fundingService.insertFundingForm(memVO);
		return "funding/fundingInsertForm";

	}

	// 첨부파일-펀딩 등록
	@ResponseBody
	@PostMapping("/funding/insertFunding")
	public RedirectView insertFunding(Model model, FundingVO fundingVO, BindingResult bindingResult,
			@RequestParam MultipartFile fundingImage) throws IOException {
		// 첨부파일
		if (bindingResult.hasErrors()) {
			List<ObjectError> errors = bindingResult.getAllErrors();
			for (ObjectError error : errors) {
				if (fundingImage == null || fundingImage.isEmpty()) {

					String nullImage = "../img/basic.jpg";
					
					fundingVO.setFundingImage(nullImage);
					fundingService.insertFunding(fundingVO);
					return new RedirectView("fundingList");
				}
				 String uploadDir = "test/";

				// 파일이름
				String fileName = fundingImage.getOriginalFilename();
				// 저장할 파일 경로
				Path ImgPath = Paths.get(uploadDir + fileName);
				// 파일 저장
				Files.write(ImgPath, fundingImage.getBytes());
				// DB에 저장할 파일 경로
				String filePath = "test/" + fileName;

				fundingVO.setFundingImage(filePath);
				fundingService.insertFunding(fundingVO);
			}
			// 오류 처리
		} else {
			// 유효성 검사 통과
		}
		return new RedirectView("fundingList");
	}

	// 펀딩 -상세화면
	@GetMapping("/funding/fundingDetails")
	public String fundingDetails(@RequestParam("fundingNo") int fundingNo, Model model,
								@RequestParam("memNo") Long memNo) {
		
		FundingVO fundingVO = fundingService.fundingDetails(fundingNo);
		FundingVO JJim = fundingService.getJJim(fundingNo, memNo);

		model.addAttribute("funding", fundingVO);
		model.addAttribute("jjim", JJim);

		return "funding/fundingDetails";
	}


	// 펀딩-결제
	@PostMapping("/funding/fundingPayment")
	@ResponseBody
	public String fundingPayment(@RequestBody FundingVO fundingVO) {
		// 결제내역추가
		int insertCount = fundingService.fundingPayment(fundingVO);

		String msg = null;

		if (insertCount == 0) {
			msg = "fail";
		} else if (insertCount == 1) {
			msg = "success";
		}

		return msg;
	}

	//펀딩메인목록
	@GetMapping("/funding/fundingList")
	public String fundingList(FundingVO fundingVO, Model model) {
		
		List<FundingVO> list = fundingService.fundingList();
		
		model.addAttribute("fundingList", list); //펀딩전체리스트 
		model.addAttribute("fundingRanking", fundingService.fundingRanking());
		
		return "funding/fundingList";
	}
	
	//펀딩드라마목록
	@GetMapping("/funding/fundingDrama")
	public String fundingDrama(Criteria cri, Model model) {
		
		List<FundingVO> list = fundingService.fundingDrama(cri);
		int total = fundingService.getDramaTotal(cri);
		
		model.addAttribute("fundingDrama", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		return "funding/fundingDrama";
	}
	
	//펀딩 kpop목록
	@GetMapping("/funding/fundingKpop")
	public String fundingKpop(Criteria cri, Model model) {
		
		List<FundingVO> list = fundingService.fundingKpop(cri);
		int total = fundingService.getKpopTotal(cri);
		
		model.addAttribute("fundingKpop", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		return "funding/fundingKpop";
	}
	
	//펀딩 댄스목록
	@GetMapping("/funding/fundingDance")
	public String fundingDance(Criteria cri, Model model) {
		
		List<FundingVO> list = fundingService.fundingDance(cri);
		int total = fundingService.getDanceTotal(cri);
		
		model.addAttribute("fundingDance", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		return "funding/fundingDance";
	}
		
	//펀딩 뮤지컬목록
	@GetMapping("/funding/fundingMusical")
	public String fundingMusical(Criteria cri, Model model) {
		
		List<FundingVO> list = fundingService.fundingMusical(cri);
		int total = fundingService.getMusicalTotal(cri);
		
		model.addAttribute("fundingMusical", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		return "funding/fundingMusical";
	
	}
	
	// 찜하기
	@PostMapping("/funding/insertFundingJJim")
	public String insertFundingJJim(FundingVO fundingVO) {

		fundingService.insertFundingJJim(fundingVO);

		return "redirect:/funding/fundingList";
	}

	// 찜 해제하기
	@PostMapping("/funding/fundingJJimCancel")
	public String fundingJJimCancel(FundingVO fundingVO) {

		fundingService.fundingJJimCancel(fundingVO);

		return "redirect:/funding/fundingList";
	}



			

}
