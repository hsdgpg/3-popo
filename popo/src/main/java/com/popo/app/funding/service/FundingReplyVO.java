package com.popo.app.funding.service;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;


public class FundingReplyVO {
	int reviewNo;					
	int fundingNo;					
	Long memNo;						
	String replyContent;			
	int commentHier;			
	int commentPno;					
	@DateTimeFormat(pattern="yyyy-mm-dd") 
	Date commentDate;				
	int commentScore;				
	String commentRStatus;			
	int commentStatus;				
	
	int likeBoolean;
	int likeCount;
	
	List<FundingReplyVO> rereply;
	
	String name;
	
}
