package com.popo.app.message.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class MessageVO {
	int msgNo; //메세지번호
	int msgSendMem; //회원번호(발신인)
	String msgName; //제목
	String msgContent; //내용
	int msgReceptionMem; //회원번호(수신인)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date msgDate; //발신일자
	int msgStatus;
	
	String msgSendName;
	
	
	String msgReceptionName;
}
