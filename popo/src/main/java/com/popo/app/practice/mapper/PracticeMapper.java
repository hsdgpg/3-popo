package com.popo.app.practice.mapper;

import java.util.List;

import com.popo.app.port.service.RecordVO;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.show.service.Criteria;

public interface PracticeMapper {
	public List<PracticeVO> selectAllPractice(); //리스트 출력
	
	public int practiceInsert(PracticeVO practiceVO);  //등록
	
	public PracticeVO practiceSelect(int practiceNo);  //단건(상세조회
	
	public int practiceViewUp(int practiceView); //조회수증가
	
	public int practiceMod(PracticeVO practiceVO); // 수정
	
	public int practiceDelete(int practiceNo); //삭제(업데이트
	
	public List<PracticeVO> practiceAllPagingList(Criteria cri); //전체페이징
	
	public int getTotalCount(Criteria cri);//전체건수
	
	public List<PracticeVO> record(Long memNo); //port
	
	public List<PracticeVO> record2(String memId);//portfolio
	
	
}
