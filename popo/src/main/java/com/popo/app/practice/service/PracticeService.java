package com.popo.app.practice.service;

import java.util.List;

import com.popo.app.show.service.Criteria;

public interface PracticeService {
	public List<PracticeVO> selectAllPractice();
	
	public int practiceInsert(PracticeVO practiceVO);
	
	public PracticeVO practiceSelect(int practiceNo);
	
	public int practiceViewUp(int practiceView);
	
	public int practiceMod(PracticeVO practiceVO); // 수정
	
	public int practiceDelete(int practiceNo); //삭제(업데이트
	
	public List<PracticeVO> practiceAllPagingList(Criteria cri);
	public int getTotal(Criteria cri);
	
	public List<PracticeVO> record(Long memNo);// port 
	
	public List<PracticeVO> record2(String memId);//portfolio
}
