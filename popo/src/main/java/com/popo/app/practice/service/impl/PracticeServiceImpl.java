package com.popo.app.practice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.practice.mapper.PracticeMapper;
import com.popo.app.practice.service.PracticeService;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.show.service.Criteria;

@Service
public class PracticeServiceImpl implements PracticeService {
	
	@Autowired
	PracticeMapper practiceMapper;
	
	@Override
	public List<PracticeVO> selectAllPractice() {
		
		return practiceMapper.selectAllPractice();
	}

	@Override
	public int practiceInsert(PracticeVO practiceVO) {
		int result = practiceMapper.practiceInsert(practiceVO);
		return result;
	}

	@Override
	public PracticeVO practiceSelect(int practiceNo) {
		
		return practiceMapper.practiceSelect(practiceNo);
	}

	@Override
	public int practiceViewUp(int practiceView) {
		int result = practiceMapper.practiceViewUp(practiceView);
		return result;
	}

	@Override
	public int practiceMod(PracticeVO practiceVO) {
		int result = practiceMapper.practiceMod(practiceVO);
		return result;
	}

	@Override
	public int practiceDelete(int practiceNo) {
		int result = practiceMapper.practiceDelete(practiceNo);
		return result;
	}

	@Override
	public List<PracticeVO> practiceAllPagingList(Criteria cri) {
		// TODO Auto-generated method stub
		return practiceMapper.practiceAllPagingList(cri);
	}

	@Override
	public int getTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return practiceMapper.getTotalCount(cri);
	}

	@Override
	public List<PracticeVO> record(Long memNo) {
		// TODO Auto-generated method stub
		return practiceMapper.record(memNo);
	}

	@Override
	public List<PracticeVO> record2(String memId) {
		// TODO Auto-generated method stub
		return practiceMapper.record2(memId);
	}

	
}
