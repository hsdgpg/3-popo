package com.popo.app.practice.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class PracticeVO {
	/*
	 * PRACTICE_NO      NOT NULL NUMBER(5)      
MEM_NO           NOT NULL NUMBER(5)      
PRACTICE_NAME    NOT NULL VARCHAR2(50)   
PRACTICE_CONTENT NOT NULL VARCHAR2(1000) 
PRACTICE_VIEW             NUMBER(10)     
PRACTICE_DATE             DATE           
PRACTICE_STATUS           NUMBER(1)  
	 * */
	
	public int practiceNo;
	public String matchName;
	public String memId;
	public Long memNo;
	public String practiceName;
	public String practiceContent;
	public int practiceView;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	public Date practiceDate;
	public int practiceStatus;
	
	String name;
	int count;
	
	int teamNo;
	String memNickName;
	
}
