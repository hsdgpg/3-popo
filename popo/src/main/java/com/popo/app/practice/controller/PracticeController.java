package com.popo.app.practice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.practice.service.PracticeService;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.team.service.ReplyVO;
// 이재현 연습기록 게시판 
@Controller
public class PracticeController {
	
	@Autowired
	PracticeService practiceService;
	
//	@RequestMapping(value="/page/practiceList",method=RequestMethod.GET)
//	public String practiceList(Model model) {
//		model.addAttribute("practiceList",practiceService.selectAllPractice());
//		return "/page/practiceList";
//	}
	
	//연습 글 리스트 페이징 
	@GetMapping("/page/practiceList") 
	public String practiceList(Criteria cri,Model model) {
		
		List<PracticeVO> list = practiceService.practiceAllPagingList(cri);
		int total = practiceService.getTotal(cri);
		
		model.addAttribute("practiceList",list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		System.err.println(list);
		System.err.println(cri);
		
		return "page/practiceList";
	}
	
	//인설트 페이지
	@GetMapping("/page/practiceInsertForm")
	public String practiceInsertForm() {
		
		return "page/practiceInsertForm";
	}
	
//	@GetMapping("/page/editor")
//	public String editor() {
//		
//		return "page/editor";
//	}
	
	//인설트 기능
	@PostMapping("/page/practiceInsert")
	public String practiceInsert(PracticeVO practiceVO,HttpSession session) {
		
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		
		practiceVO.setMemNo(memVO.getMemNo());
		
		practiceService.practiceInsert(practiceVO);
		return "redirect:/page/practiceList";
	}
	
	//상세조회 
	@RequestMapping(value={ "/page/practiceSelecter"})
	public String practiceSelecter( int practiceNo,Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		practiceService.practiceViewUp(practiceNo);
		 //practiceService.practiceViewCount(practiceNo,practiceView);	
		model.addAttribute("practice", practiceService.practiceSelect(practiceNo));
		
		return "page/practiceSelecter";
	}
	
	//수정폼
	@RequestMapping(value={ "/page/practiceModForm"})
	public String practiceModForm() {
		
		
		
		
		return "page/practiceModForm";
	}
	
	//수정처리
	@PostMapping("/page/practiceMod")
	public String practiceMod(PracticeVO practiceVO) {
		practiceService.practiceMod(practiceVO);
		return "redirect:/page/practiceList";
	}
	
	//삭제처리
	@RequestMapping(value={ "/page/practiceDelete"})
	public String practiceDelete(@RequestParam(value="practiceNo", required=false) Integer practiceNo) {
		System.out.println(practiceNo);
		practiceService.practiceDelete(practiceNo);
		
		return "redirect:/page/practiceList";
	}
}
