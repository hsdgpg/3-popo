package com.popo.app.show.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.TeamVO;

@Mapper
public interface ShowMapper {
	
	// 랭킹
	public List<ShowVO> showAllRankList();
	public List<ShowVO> showTheaterRankList();
	public List<ShowVO> showMusicalRankList();
	public List<ShowVO> showKpopRankList();
	public List<ShowVO> showDanceRankList();
	// 공간대여 여부
	public int spaceStatsu(Long memNo);
	// 등록된 공연이 있는지 여부
	public int showStatus(int teamNo);
	
	// 페이징
	public List<ShowVO> showAllPagingList(Criteria cri); // 전체
	public List<ShowVO> showTheaterPagingList(Criteria cri); // 연극
	public List<ShowVO> showMusicalPagingList(Criteria cri); // 뮤지컬
	public List<ShowVO> showKpopPagingList(Criteria cri); // kPop
	public List<ShowVO> showDancePagingList(Criteria cri); // 댄스
	
	// 전체건수
	public int getTotalCount(Criteria cri);
	public int getTheaterTotalCount(Criteria cri);
	public int getMusicalTotalCount(Criteria cri);
	public int getKpopTotalCount(Criteria cri);
	public int getDanceTotalCount(Criteria cri);
	
	// 단건조회(상세화면)
	public ShowVO getShow(int showNo);
	// 예매기간 지났는지 여부
	public int showDateStatus(int showNo);
	// 찜 데이터가 있는 여부
	public ShowVO getShow2(int showNo, Long memNo);
	// 찜 목록 추가
	public void insertShowJJim(ShowVO showVO);
	// 찜 해제하기
	public void showJJimDeleteUpdate(ShowVO showVO);
	// 공연 상세페이지 리뷰 조회
	public List<ShowReviewVO> showReviewList(Long memNo,int showNo);	
	// 공연 상세페이지 리뷰 추가
	public int addReview(int showNo, Long memNo,String commentContent, int commentScope);
	// 공연 상세페이지 리뷰 삭제
	public int deleteReview(Long memNo, int reviewNo);
	// 리뷰 좋아요
	public int reviewLike(Long memNo,int reviewNo,int showNo);
	// 리뷰 좋아요 해제
	public int reviewUnLike(Long memNo,int reviewNo,int showNo);
	//리뷰 좋아요 수 +
	public void reviewUpCount(Long memNo,int reviewNo,int showNo);
	//리뷰 좋아요 수 -
	public void reviewDownCount(Long memNo,int reviewNo,int showNo);
	
	
	// 좌석가격
	public ShowVO getSeatSPrice(int showNo);
	public ShowVO getSeatAPrice(int showNo);
	public ShowVO getSeatBPrice(int showNo);
	
	// 예매내역 추가
	public int ticketingInsert(ShowVO showVO);
	// 결제내역 추가
	public void showPayment(ShowVO showVO);
	// 선택된 좌석 추가
	public void insertSeat(ShowVO showVO);
	// 팀 정산
	public void teamSettlement(ShowVO showVO);
	// 관리자 정산
	public void adminSettlement(ShowVO showVO);
	
	// 선택된 좌석 목록
	public List<ShowVO> selectedSeat(int showNo);
	
	// 결제대기목록 추가(장바구니 담기)
	public int waitingPayment(ShowVO showVO);
	// 예매내역 추가(대기)
	public int waitingTicketingInsert(ShowVO showVO);
	// 선택된 좌석 추가(대기)
	public void waitingInsertSeat(ShowVO showVO);
	
	// 공연등록
	public int showInsert(ShowVO showVO);
	// 공연 등록시 팀의 공연번호 업데이트
	public void teamUpdate(ShowVO showVO);
	// 좌석등록
	public void seatInsert(ShowVO showVO);
	// 이미지 등록
	public int uploadImage(ShowVO showVO);
	// 허용된 공간목록
	public List<ShowVO> spaceList(Long memNo);
	
	
		
}
