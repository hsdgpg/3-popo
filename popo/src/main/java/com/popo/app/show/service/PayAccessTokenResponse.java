package com.popo.app.show.service;

import lombok.Data;

@Data
public class PayAccessTokenResponse {
	private String access_token;
	private long now;
	private long expired_at;
}
