package com.popo.app.show.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.popo.app.member.service.MemberVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.mapper.ShowMapper;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowService;
import com.popo.app.show.service.ShowVO;

@Service
public class ShowServiceImpl implements ShowService {

	@Autowired
	ShowMapper showMapper;

	@Override
	public List<ShowVO> showAllPagingList(Criteria cri) {
		return showMapper.showAllPagingList(cri);
	}

	@Override
	public List<ShowVO> showTheaterPagingList(Criteria cri) {
		return showMapper.showTheaterPagingList(cri);
	}

	@Override
	public List<ShowVO> showMusicalPagingList(Criteria cri) {
		return showMapper.showMusicalPagingList(cri);
	}

	@Override
	public List<ShowVO> showKpopPagingList(Criteria cri) {
		return showMapper.showKpopPagingList(cri);
	}

	@Override
	public List<ShowVO> showDancePagingList(Criteria cri) {
		return showMapper.showDancePagingList(cri);
	}
	
	@Override
	public int spaceStatsu(Long memNo) {
		return showMapper.spaceStatsu(memNo);
	}
	
	@Override
	public int showStatus(int teamNo) {
		return showMapper.showStatus(teamNo);
	}


	@Override
	public int getTotal(Criteria cri) {
		return showMapper.getTotalCount(cri);
	}

	@Override
	public int getTheaterTotalCount(Criteria cri) {
		return showMapper.getTheaterTotalCount(cri);
	}

	@Override
	public int getMusicalTotalCount(Criteria cri) {
		return showMapper.getMusicalTotalCount(cri);
	}

	@Override
	public int getKpopTotalCount(Criteria cri) {
		return showMapper.getKpopTotalCount(cri);
	}

	@Override
	public int getDanceTotalCount(Criteria cri) {
		return showMapper.getDanceTotalCount(cri);
	}

	@Override
	public List<ShowVO> showAllRankList() {
		return showMapper.showAllRankList();
	}

	@Override
	public List<ShowVO> showTheaterRankList() {
		return showMapper.showTheaterRankList();
	}

	@Override
	public List<ShowVO> showMusicalRankList() {
		return showMapper.showMusicalRankList();
	}

	@Override
	public List<ShowVO> showKpopRankList() {
		return showMapper.showKpopRankList();
	}

	@Override
	public List<ShowVO> showDanceRankList() {
		return showMapper.showDanceRankList();
	}

	@Override
	public ShowVO getShow(int showNo) {
		return showMapper.getShow(showNo);
	}

	@Override
	public ShowVO getShow2(int showNo, Long memNo) {
		return showMapper.getShow2(showNo,memNo);
	}
	
	@Override
	public int showDateStatus(int showNo) {
		return showMapper.showDateStatus(showNo);
	}

	@Override
	public void insertShowJJim(ShowVO showVO) {
		showMapper.insertShowJJim(showVO);
	}

	@Override
	public void showJJimDeleteUpdate(ShowVO showVO) {
		showMapper.showJJimDeleteUpdate(showVO);
	}

	@Override
	public List<ShowReviewVO> showReviewList(Long memNo, int showNo) {
		return showMapper.showReviewList(memNo, showNo);
	}

	@Override
	public int addReview(int showNo, Long memNo, String commentContent, int commentScope) {
		return showMapper.addReview(showNo, memNo, commentContent, commentScope);
	}

	@Override
	public int deleteReview( Long memNo, int reviewNo) {
		return showMapper.deleteReview( memNo, reviewNo);
	}

	@Override
	public int reviewLike(Long memNo, int reviewNo, int showNo) {
		showMapper.reviewUpCount(memNo, reviewNo, showNo);
		
		return showMapper.reviewLike(memNo, reviewNo, showNo);
	}

	@Override
	public int reviewUnLike(Long memNo, int reviewNo, int showNo) {
		showMapper.reviewDownCount(memNo, reviewNo, showNo);

		return showMapper.reviewUnLike(memNo, reviewNo, showNo);
	}


	@Override
	public ShowVO getSeatSPrice(int showNo) {
		return showMapper.getSeatSPrice(showNo);
	}

	@Override
	public ShowVO getSeatAPrice(int showNo) {
		return showMapper.getSeatAPrice(showNo);
	}

	@Override
	public ShowVO getSeatBPrice(int showNo) {
		return showMapper.getSeatBPrice(showNo);
	}

	@Override
	public int ticketingInsert(ShowVO showVO) {

		// 예매 추가
		int result = showMapper.ticketingInsert(showVO);
		// 결제 내역 추가
		showMapper.showPayment(showVO);
		// ajax 통신을 통해 받아온 좌석값을 ,기준으로 나누어서 DB에 좌석별로 저장
		String[] list = showVO.getSeatNo().split(",");

		for (int i = 0; i < list.length; i++) {

			showVO.setSeatNo(list[i]);
			showMapper.insertSeat(showVO);
		}
		// 맴버 정산
		showMapper.teamSettlement(showVO);
		// 관리자 수수료
		showMapper.adminSettlement(showVO);

		return result;
	}

	@Override
	public List<ShowVO> selectedSeat(int showNo) {
		return showMapper.selectedSeat(showNo);
	}

	@Override
	public int waitingPayment(ShowVO showVO) {
		
		// 예매 추가
		showMapper.waitingTicketingInsert(showVO);
		
		// 선택된 좌석 추가
		String[] list = showVO.getSeatNo().split(",");

		for (int i = 0; i < list.length; i++) {

			showVO.setSeatNo(list[i]);
			showMapper.waitingInsertSeat(showVO);
		}
		
		// 결제내역 추가
		return showMapper.waitingPayment(showVO);
	}

	@Override
	public int showInsert(ShowVO showVO) {
		
		return showMapper.showInsert(showVO);
		
	}

	@Override
	public void seatInsert(ShowVO showVO) {
		String[] grade = { "S", "A", "B" };
		int[] seatPrice = { showVO.getSsPrice(), showVO.getAaPrice(), showVO.getBbPrice() };
		
		

		for (int i = 0; i < grade.length; i++) {

			System.out.println(seatPrice[i]);
			
			showVO.setSeatGrade(grade[i]);
			showVO.setSeatPrice(seatPrice[i]);

			showMapper.teamUpdate(showVO);
			showMapper.seatInsert(showVO);
		}
	}

	@Override
	public int uploadImage(ShowVO showVO) {
		return showMapper.uploadImage(showVO);
	}


	@Override
	public List<ShowVO> spaceList(Long memNo) {
		return showMapper.spaceList(memNo);
	}

	
	

	

}
