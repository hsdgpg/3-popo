package com.popo.app.show.service;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;


import lombok.Data;

@Data
public class ShowReviewVO {

//	review_No        NOT NULL NUMBER(5)     
//	show_NO         NOT NULL NUMBER(5)     
//	MEM_NO            NOT NULL NUMBER(5)     
//	COMMENT_CONTENT   NOT NULL VARCHAR2(500) 
//	COMMENT_HIERARCHY          NUMBER(5)     
//	COMMENT_P_NO               NUMBER(5)     
//	COMMENT_DATE               DATE          
//	COMMENT_SCOPE              NUMBER(5)     
//	COMMENT_R_STATUS           VARCHAR2(20)  
//	COMMENT_STATUS             NUMBER(1)    
	
	int reviewNo;					//리뷰번호
	int showNo;					//공연번호
	Long memNo;						//리뷰 작성자
	String commentContent;			//리뷰 내용
	int commentHierarchy;			//계층
	int commentPno;					//부모댓글번호
	@DateTimeFormat(pattern="yyyy-mm-dd") 
	Date commentDate;				//작성일시 default sysdate
	int commentScope;				//별점
	String commentRStatus;			//신고처리상태
	int commentStatus;				//공개상태 default 0
	
	int likeBoolean;
	int likeCount;
	
	List<ShowReviewVO> rereply;
	
	String name;
	String memName;
	String showName;
	
}
