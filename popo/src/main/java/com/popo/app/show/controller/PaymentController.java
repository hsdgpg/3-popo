package com.popo.app.show.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.popo.app.show.service.PayAccessToken;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.popo.app.show.service.RefundRequest;

@RestController
public class PaymentController {
  
	
	public String getToken() {
		
		RestTemplate restTemplate = new RestTemplate();
	
		Map<String, Object> data = new HashMap<>();
		data.put("imp_key", "3767787884130823");
		data.put("imp_secret", "NJcLtICndpTqZiigsGOsnj3ASPucUt6xEWSgpQGEbniObZxdw1x9eptqOgfsBLOkkC5mzJdBfSp1JW0I");
		
		String apiUrl = "https://api.iamport.kr/users/getToken";
		 
		//서버로 요청할 Header
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    
	    HttpEntity<Map<String, Object>> request = new HttpEntity<>(data, headers);
	    ResponseEntity<PayAccessToken> response = restTemplate.postForEntity(apiUrl, request, PayAccessToken.class);
		
	    String token = response.getBody().getResponse().getAccess_token();
	    
	    System.out.println("응답 토큰 값 : " + token);
	    
		return token;
		
	}
	
  
  @PostMapping("/api/refund")
  public ResponseEntity<String> refund(@RequestBody RefundRequest refundRequest) {
	
	  System.out.println(refundRequest);
	  
	  // RestTemplate 객체 생성
	  RestTemplate restTemplate = new RestTemplate();

	  // 환불 요청을 위한 데이터 생성
	  Map<String, Object> data = new HashMap<>();
	  data.put("imp_uid", refundRequest.getImpUid());
	  data.put("amount", refundRequest.getAmount());
	  data.put("reason", refundRequest.getReason());
	  data.put("merchant_uid", refundRequest.getMerchantUid());

	  // 환불 요청
	  String apiUrl = "https://api.iamport.kr/payments/cancel";
	  HttpHeaders headers = new HttpHeaders();
	  headers.setContentType(MediaType.APPLICATION_JSON);
	  headers.set("Authorization", "Bearer " + getToken());
	  HttpEntity<Map<String, Object>> request = new HttpEntity<>(data, headers);
	  ResponseEntity<String> response = restTemplate.postForEntity(apiUrl, request, String.class);
	  
	  // 환불 결과 확인
	  if (response.getStatusCode() == HttpStatus.OK) {
	      System.out.println("환불이 성공적으로 처리되었습니다.");
	  } else {
	      System.out.println("환불 처리 중 오류가 발생하였습니다.");
	  }
	  
	  return response;
	  
  }

}
