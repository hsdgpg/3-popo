package com.popo.app.show.controller;

import java.io.IOException;
import java.net.http.HttpHeaders;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

// 김준혁 예매관리

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.show.service.RefundRequest;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowService;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.TeamVO;

@Controller
public class ShowController {

	@Autowired
	ShowService showService;

	// 전체, 장르별 랭킹 조회
	@GetMapping("/show/showList")
	public String showList(ShowVO showVO, Model model, HttpSession session) {
		
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		if( memVO != null ) {
			
			Long memNo = memVO.getMemNo();
			int teamNo = memVO.getTeamNo();
			model.addAttribute("spaceStatus", showService.spaceStatsu(memNo));
			model.addAttribute("showStatus", showService.showStatus(teamNo));
			
		}
		
		
		
		model.addAttribute("showAllList", showService.showAllRankList());
		model.addAttribute("showTheaterList", showService.showTheaterRankList());
		model.addAttribute("showMusicalList", showService.showMusicalRankList());
		model.addAttribute("showKpopList", showService.showKpopRankList());
		model.addAttribute("showDanceList", showService.showDanceRankList());


		return "show/showList";
	}

	// 전체 페이징 리스트
	@GetMapping("/show/showAllList")
	public String showAllList(Criteria cri, Model model) {

		List<ShowVO> list = showService.showAllPagingList(cri);
		int total = showService.getTotal(cri);

		model.addAttribute("showList", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));

		System.err.println(list);
		System.err.println(cri);

		return "show/showAllList";
	}

	// 연극 페이징 리스트
	@GetMapping("/show/showTheaterList")
	public String showTheaterList(Criteria cri, Model model) {

		List<ShowVO> list = showService.showTheaterPagingList(cri);
		int total = showService.getTheaterTotalCount(cri);

		model.addAttribute("showList", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "show/showTheaterList";
	}

	// 뮤지컬 페이징 리스트
	@GetMapping("/show/showMusicalList")
	public String showMusicalList(Criteria cri, Model model) {

		List<ShowVO> list = showService.showMusicalPagingList(cri);
		int total = showService.getMusicalTotalCount(cri);

		model.addAttribute("showList", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "show/showMusicalList";
	}

	// k-pop 페이징 리스트
	@GetMapping("/show/showKpopList")
	public String showKpopList(Criteria cri, Model model) {

		List<ShowVO> list = showService.showKpopPagingList(cri);
		int total = showService.getKpopTotalCount(cri);

		model.addAttribute("showList", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "show/showKpopList";
	}

	// 댄스 페이징 리스트
	@GetMapping("/show/showDanceList")
	public String showDanceList(Criteria cri, Model model) {

		List<ShowVO> list = showService.showDancePagingList(cri);
		int total = showService.getDanceTotalCount(cri);

		model.addAttribute("showList", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "show/showDanceList";
	}

	// 상세화면
	@GetMapping("/show/showDetail")
	public String showDetail(@RequestParam("showNo") int showNo,
						     @RequestParam(value = "memNo", required = false) Long memNo, 
						     Model model) {

		ShowVO vo = showService.getShow(showNo);
		// 찜 여부
		ShowVO vo2 = showService.getShow2(showNo,memNo);
		// 예매기간 지났는지 여부
		int vo3 = showService.showDateStatus(showNo);

		model.addAttribute("show", vo);
		model.addAttribute("show2", vo2);
		model.addAttribute("showStatus", vo3);

		return "show/showDetail";
	}

	// 예매화면
	@GetMapping("/show/showTicketing")
	public String showTicketing(@RequestParam("showNo") int showNo, Model model, ShowVO showVO) {
		
		
		// 예매 정보
		ShowVO vo = showService.getShow(showNo);
		// 좌석 가격
		ShowVO vo2 = showService.getSeatSPrice(showNo);
		ShowVO vo3 = showService.getSeatAPrice(showNo);
		ShowVO vo4 = showService.getSeatBPrice(showNo);
		// 선택된 좌석정보
		List<ShowVO> vo5 = showService.selectedSeat(showNo);

		model.addAttribute("show", vo);
		model.addAttribute("showSPrice", vo2);
		model.addAttribute("showAPrice", vo3);
		model.addAttribute("showBPrice", vo4);
		model.addAttribute("selectSeat", vo5);


		return "show/showTicketing";
	}

	// 찜하기
	@PostMapping("/show/showJJim")
	public String showJJim(ShowVO showVO, Model model) {

		showService.insertShowJJim(showVO);

		return "redirect:/show/showList";
	}

	// 찜 해제하기
	@PostMapping("/show/showJJimDeleteUpdate")
	public String showJJimDeleteUpdate(ShowVO showVO) {

		showService.showJJimDeleteUpdate(showVO);

		return "redirect:/show/showList";
	}

	
	// 리뷰 조회 
	@PostMapping("/show/reviewList")
	@ResponseBody
	public List<ShowReviewVO> reviewList(@RequestParam(value="showNo") int showNo,
								        @RequestParam(value = "memNo", required = false) Long memNo) {

		List<ShowReviewVO> review = showService.showReviewList(memNo, showNo);

		return review;
	}
	
	
	// 리뷰 등록
	@PostMapping("/show/addReview")
	@ResponseBody
	public int addReview(@RequestParam(value = "showNo", required = false) int showNo,
						 @RequestParam(value = "memNo", required = false) Long memNo,
						 @RequestParam(value = "commentContent", required = false) String commentContent,
						 @RequestParam(value = "commentScope", required = false) int commentScope) {
		
	    int addReview = showService.addReview(showNo,memNo,commentContent,commentScope);
	    
	    return addReview;
	}
	
	// 리뷰 삭제
	@PostMapping("/show/deleteReview")
	@ResponseBody
	public int deleteReview(@RequestParam(value = "memNo") Long memNo,
						 @RequestParam(value="reviewNo") int reviewNo) {
		
	    int deleteReview = showService.deleteReview(memNo,reviewNo);
	    	
	    return deleteReview;
	        
	}

	
	// 공연 상세페이지 리뷰 좋아요
	@PostMapping("/show/reviewLike")
	@ResponseBody
	public String reviewLike(@RequestParam(value="memNo" ) Long memNo,
						   @RequestParam(value="reviewNo") int reviewNo,
						   @RequestParam(value = "showNo") int showNo) {
	
		String msg ="success";
		
			showService.reviewLike(memNo,reviewNo, showNo);
			
			return msg ;
		
	}
								
		
	// 공연 상세페이지 리뷰 좋아요해제
	@PostMapping("/show/reviewUnLike")
	@ResponseBody
	public String reviewUnLike(@RequestParam(value="memNo" ) Long memNo,
			   				 @RequestParam(value="reviewNo") int reviewNo,
			   				 @RequestParam(value = "showNo") int showNo) {
		
		showService.reviewUnLike(memNo,reviewNo, showNo);
		
		String msg = "success";
		
		
		return msg;
	}
	
	

	// 결제내역 추가, 예매내역 추가, 선택좌석 추가
	@PostMapping("/show/showPayment")
	@ResponseBody
	public String showPayment(@RequestBody ShowVO showVO, Model model) {

		int insertCount = showService.ticketingInsert(showVO);
		
		System.out.println(showVO);

		String msg = null;

		if (insertCount == 0) {

			msg = "fail";

		} else if (insertCount == 1) {

			msg = "success";
		}

		return msg;
	}
	

	// 결제대기목록 추가(장바구니 담기)
	@PostMapping("/payment/waitingPayment")
	@ResponseBody
	public String waitingPayment(@RequestBody ShowVO showVO, Model model) {

		int result = showService.waitingPayment(showVO);
		
		String msg = null;

		if (result == 0) {

			msg = "fail";

		} else if (result == 1) {

			msg = "success";
		}

		return msg;
	}

	// 공연등록화면
	@GetMapping("/show/showInsert")
	public String showInsertForm(Model model, HttpSession session) {

		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		
		model.addAttribute("spaceList", showService.spaceList(memNo));
		
		return "show/showInsert";
	}

	// 공연등록
	@PostMapping("/show/showInsert")
	@ResponseBody
	public String showInsert(@RequestBody ShowVO showVO) {

		System.out.println(showVO);
		
		String msg = "success";
		
		showService.showInsert(showVO);
		showService.seatInsert(showVO);
		


		return msg;
	}
	
	// 공연등록 이미지 업로드
		//d:/test 폴더필요 , basic.jpg 파일 필요 - > 기본이미지
		
		@PostMapping("/show/uploadImage")
		@ResponseBody
		public RedirectView  uploadImage( Model model,
									ShowVO showVO,BindingResult bindingResult,
									@RequestParam MultipartFile showImage) throws IOException{
								
			if (bindingResult.hasErrors()) {
		        List<ObjectError> errors = bindingResult.getAllErrors();
		        for (ObjectError error : errors) {
		        	
		        	
		        	 if(showImage == null || showImage.isEmpty()){
		        		 
		        		 String nullImage = "test/basic.jpg";
			    	        
			    	    	showVO.setShowImage(nullImage);
			    	    	showService.uploadImage(showVO);
			    	        return new RedirectView("/boot/show/showList");
			    	    }
		        	
		        	 String uploadDir = "test/";

		    		// 파일이름
		    		String fileName = showImage.getOriginalFilename();
		    		// 저장할 파일 경로
		    	    Path path = Paths.get(uploadDir + fileName);
		    	    // 파일 저장
		    	    Files.write(path, showImage.getBytes());
		    	    // DB에 저장할 파일 경로
		    	    String filePath = uploadDir + fileName;
		    	  
		    	   
		    	    
		            showVO.setShowImage(filePath);
		            showService.uploadImage(showVO);
		        }
		        // 오류 처리
		    } else {
		        // 유효성 검사 통과
		    }
			
			
			
			  return new RedirectView("/boot/show/showList");
			
		}

	

}
