package com.popo.app.show.service;

import lombok.Data;

@Data
public class RefundRequest {

	   String impUid;
	   int amount;
	   String reason;
	   String merchantUid;
	  
	}
