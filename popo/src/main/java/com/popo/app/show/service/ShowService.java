package com.popo.app.show.service;

import java.util.List;

import com.popo.app.member.service.MemberVO;
import com.popo.app.payment.service.PaymentVO;


public interface ShowService {

	// 랭킹
	public List<ShowVO> showAllRankList();
	public List<ShowVO> showTheaterRankList();
	public List<ShowVO> showMusicalRankList();
	public List<ShowVO> showKpopRankList();
	public List<ShowVO> showDanceRankList();
	// 공간대여 여부
	public int spaceStatsu(Long memNo);
	// 등록된 공연이 있는지 여부
	public int showStatus(int teamNo);
	
	// 페이징
	public List<ShowVO> showAllPagingList(Criteria cri);
	public List<ShowVO> showTheaterPagingList(Criteria cri);
	public List<ShowVO> showMusicalPagingList(Criteria cri);
	public List<ShowVO> showKpopPagingList(Criteria cri);
	public List<ShowVO> showDancePagingList(Criteria cri);
	
	// 전체건수
	public int getTotal(Criteria cri);
	public int getTheaterTotalCount(Criteria cri);
	public int getMusicalTotalCount(Criteria cri);
	public int getKpopTotalCount(Criteria cri);
	public int getDanceTotalCount(Criteria cri);
	
	// 단건조회(상세화면)
	public ShowVO getShow(int showNo);
	// 예매기간 지났는지 여부
	public int showDateStatus(int showNo);
	// 찜 데이터가 있는 여부
	public ShowVO getShow2(int showNo, Long memNo);
	// 찜 목록 추가
	public void insertShowJJim(ShowVO showVO);
	// 찜 해제하기
	public void showJJimDeleteUpdate(ShowVO showVO);
	// 공연 상세페이지 리뷰 조회
	public List<ShowReviewVO> showReviewList(Long memNo,int showNo);
	// 공연 상세페이지 리뷰 추가
	public int addReview(int showNo, Long memNo,String commentContent, int commentScope);
	// 공연 상세페이지 리뷰 삭제
	public int deleteReview(Long memNo, int reviewNo);
	// 리뷰 좋아요
	public int reviewLike(Long memNo,int reviewNo,int showNo);
	// 리뷰 좋아요 해제
	public int reviewUnLike(Long memNo,int reviewNo,int showNo);
	
	// 좌석가격
	public ShowVO getSeatSPrice(int showNo);
	public ShowVO getSeatAPrice(int showNo);
	public ShowVO getSeatBPrice(int showNo);
	
	// 결제내역 추가
	public int ticketingInsert(ShowVO showVO);
	
	// 선택된 좌석 목록
	public List<ShowVO> selectedSeat(int showNo);
	
	// 결제대기목록 추가(장바구니 담기)
	public int waitingPayment(ShowVO showVO);
	
	// 공연등록
	public int showInsert(ShowVO showVO);
	// 좌석등록
	public void seatInsert(ShowVO showVO);
	// 이미지 등록
	public int uploadImage(ShowVO showVO);
	// 허용된 공간목록
	public List<ShowVO> spaceList(Long memNo);
	
}
