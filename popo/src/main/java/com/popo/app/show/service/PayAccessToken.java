package com.popo.app.show.service;

import lombok.Data;

@Data
public class PayAccessToken {
	private int code;
	private String message;
	private PayAccessTokenResponse response;
}
