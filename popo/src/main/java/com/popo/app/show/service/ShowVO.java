package com.popo.app.show.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class ShowVO {
/*
	SHOW_NO      NOT NULL NUMBER(5)      
	TEAM_NO      NOT NULL NUMBER(5)      
	MEM_NO       NOT NULL NUMBER(5)      
	SHOW_NAME    NOT NULL VARCHAR2(60)   
	SHOW_DATE    NOT NULL DATE           
	SHOW_TIME    NOT NULL DATE           
	SHOW_PLACE   NOT NULL VARCHAR2(300)  
	SHOW_TYPE    NOT NULL VARCHAR2(24)   
	SHOW_IMAGE            VARCHAR2(50)   
	SHOW_P_NUM   NOT NULL NUMBER(5)      
	SHOW_CONTENT          VARCHAR2(1000) 
	SEAT_PRICE   NOT NULL VARCHAR2(20)   
	SHOW_CAUTION          VARCHAR2(100)  
	SHOW_STATUS           NUMBER(1)   
*/
	
	
	// 공연정보
	int showNo;
	int teamNo;
	Long memNo;
	String showName;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date showDate;
	Date showDate2;
	String showTime;
	String showPlace;
	String showType;
	String showImage;
	int showPNum;
	String showContent;
	String showCaution;
	int showStatus;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date reservStart;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date reservEnd;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date showDay;
	int showView;
	Long a; // 예매건수
	int totalCount; // 총 예매건수
	
	// 순위정보
	int rownum;
	
	// 팀 정보
	String teamName;
	String teamTarget;
	String teamSpecificity;
	String teamImage;
	int teamNum;
	
	// 찜 정보
	int jjimBoolean;
	int jjimNo;
	
	// 결제정보
	int amountPrice; // 총금액
	int peopleNum; // 결제시 선택한 좌석수
	String merchantUid;
	String reason;
	String impUid; // 결제 고유번호
	
	// 좌석정보
	String seatNo;
	String seatGrade;
	int seatPrice;
	int ssPrice;
	int aaPrice;
	int bbPrice;
	
	// 계좌정보
	String accountNumber; // 계좌번호
	int accountPrice; // 계좌에 들어있는 금액
	
	// 예매정보
	int reservNo;
	
	// 공간대여 여부 확인
	int count;
	
	
	
	//show 달력 정보 
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date startDate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date endDate;
	String title;
	
	
}
