package com.popo.app.port.service;

import java.util.List;

import com.popo.app.practice.service.PracticeVO;

public interface PortService {
	public List<RecordVO> participated(Long memNo); //참여했던 팀 
	
	public List<RecordVO> participat(Long memNo); //참여중인 팀
	
	public int recordInsert(RecordVO recordVO);//레코드 등록
	public int recordInsert2(RecordVO recordVO);//레코드 등록
	
	public List<RecordVO> participated2(String memId);//portfolio 
	
	public List<RecordVO> participat2(String memId); //portfolio
	
}
