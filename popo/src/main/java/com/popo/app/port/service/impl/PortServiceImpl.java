package com.popo.app.port.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.port.mapper.PortMapper;
import com.popo.app.port.service.PortService;
import com.popo.app.port.service.RecordVO;
import com.popo.app.practice.service.PracticeService;
import com.popo.app.practice.service.PracticeVO;

@Service
public class PortServiceImpl implements PortService{
	
	@Autowired
	PortMapper portMapper;
	
	@Override
	public List<RecordVO> participated(Long memNo) {
		// TODO Auto-generated method stub
		System.out.println(memNo);
		return portMapper.participated(memNo);
	}

	@Override
	public List<RecordVO> participat(Long memNo) {
		// TODO Auto-generated method stub
		return portMapper.participat(memNo);
	}

	@Override
	public int recordInsert(RecordVO recordVO) {
		// TODO Auto-generated method stub
		return portMapper.recordInsert(recordVO);
	}

	@Override
	public int recordInsert2(RecordVO recordVO) {
		// TODO Auto-generated method stub
		return portMapper.recordInsert2(recordVO);
	}

	@Override
	public List<RecordVO> participated2(String memId) {
		// TODO Auto-generated method stub
		return portMapper.participated2(memId);
	}

	@Override
	public List<RecordVO> participat2(String memId) {
		// TODO Auto-generated method stub
		return portMapper.participat2(memId);
	}

	
}
