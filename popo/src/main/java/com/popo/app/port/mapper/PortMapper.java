package com.popo.app.port.mapper;

import java.util.List;

import com.popo.app.port.service.RecordVO;
import com.popo.app.practice.service.PracticeVO;

public interface PortMapper {
	public List<RecordVO> participated(Long memNo);//port 
	
	public List<RecordVO> participat(Long memNo); //port
	
	public int recordInsert(RecordVO recordVO);//레코드 등록 
	
	public int recordInsert2(RecordVO recordVO);//레코드 등록
	
	public List<RecordVO> participated2(String memId);//portfolio 
	
	public List<RecordVO> participat2(String memId); //portfolio
}
