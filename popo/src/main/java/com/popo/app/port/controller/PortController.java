package com.popo.app.port.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.popo.app.member.service.MemberVO;
import com.popo.app.port.service.PortService;
import com.popo.app.practice.service.PracticeService;
import com.popo.app.practice.service.PracticeVO;

@Controller
public class PortController {
	
	@Autowired
	PortService portService;
	@Autowired
	PracticeService practiceService;
	
	@RequestMapping(value="/port/port",method=RequestMethod.GET)
	public String port(Model model,HttpSession session) {
	    MemberVO memVO = (MemberVO)session.getAttribute("memVO");
	    
	    model.addAttribute("participated",portService.participated(memVO.getMemNo()));
		model.addAttribute("participat",portService.participat(memVO.getMemNo()));
		List<PracticeVO> list = practiceService.record(memVO.getMemNo());
		model.addAttribute("record",list);
		model.addAttribute("memId",memVO.getMemId());
		return "port/port";
	}
	
	@RequestMapping(value="/port/portfolio",method=RequestMethod.GET)
	public String portfolio(@RequestParam(value="memId" ,required=false) String memId,
						    @RequestParam(value="matchTeamName" ,required=false) String matchTeamName,
						    @RequestParam(value="matchNo" ,required=false) int matchNo,
			              Model model,HttpSession session) {
		
	    model.addAttribute("participated",portService.participated2(memId));
		model.addAttribute("participat",portService.participat2(memId));
		List<PracticeVO> list = practiceService.record2(memId);
		model.addAttribute("record",list);
		model.addAttribute("memId",memId);
		model.addAttribute("matchTeamName",matchTeamName);
		model.addAttribute("matchNo",matchNo);
		return "port/portfolio"; 
	}
}
