package com.popo.app.port.service;

import java.util.Date;

import lombok.Data;

@Data
public class RecordVO {
//	RECORD_NO     NOT NULL NUMBER(5)    
//	RECORD_START           DATE         
//	RECORD_END             DATE         
//	RECORD_STATUS          VARCHAR2(10) 
//	MEM_NO        NOT NULL NUMBER(5)    
//	TEAM_NO       NOT NULL NUMBER(5)
	int rownum;
	int recordNo;
	Date recordStart;
	Date recordEnd;
	String recordStatus;
	Long memNo;
	int teamNo;
	String teamName;
	String memId;
	
}
