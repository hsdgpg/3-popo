package com.popo.app.payment.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class PaymentVO {
	
	
	// 예매
	
	int payNo;
	String payType;
	Long memNo;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date payDate;
	int payPrice;
	int payStatus;
	int rentalNo;
	
	
	// 번호
	int rn;
	
	// 결제된 공연 이름
	String showName;
	
	// 공연정보
		int showNo;
		int teamNo;
		@DateTimeFormat(pattern = "yyyy-MM-dd")
		Date showDate;
		@DateTimeFormat(pattern = "yyyy-MM-dd")
		Date payTime;
		
		// 순위정보
		int rownum;
		
		int teamNum;
		String teamName;
		
		// 결제정보
		int amountPrice; // 총금액
		int peopleNum; // 결제시 선택한 좌석수
		String reason;
		String impUid;
		String merchantUid;
		
		// 예매정보
		int reservNo;
		
		
		
		// ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
		// 펀딩
		
		
		int fundingNo;
		String fundingName;
		String fundingArtist;
		@DateTimeFormat(pattern="yyyy-MM-dd")
		Date fundingStart;
		@DateTimeFormat(pattern="yyyy-MM-dd")
		Date fundingEnd;
		int fundingCPrice;
		int fundingPrice;
		String fundingImage;
		public String fundingContent;
		String fundingRecognize;
		String fundingRDate;
		int fundingStatus;
		String fundingGenre;
		@DateTimeFormat(pattern="yyyy-MM-dd")
		Date fundingDay;
		
		String memName;
		
		int success; // 펀딩금액 달성률
		
		
		// ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
		// 공간 대여
		
		int placeNo;
		String placeType;
		String placeName;
		String placeRepPhoto;
		int placeCost;
		String placeOpen;
		String placeClose;
		String placeIntroduce;
		int placeSeat;
		String placeHoliday;
		String placeRefundRule;
		String placeTel;
		String addr;
		String detailAddress;
		String placeAddress;
		String placeApproval;
		@DateTimeFormat(pattern="yyyy-MM-dd")
		Date placeApprovalDate;
		int placeStatus;
		
		@DateTimeFormat(pattern="yyyy-MM-dd")
		Date rentalStart;
		String rentalStartTime;
		
		@DateTimeFormat(pattern="yyyy-MM-dd")
		Date rentalEnd;
		String rentalEndTime;
		
		
}
