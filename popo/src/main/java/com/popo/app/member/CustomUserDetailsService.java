package com.popo.app.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.popo.app.member.mapper.MemberMapper;
import com.popo.app.member.service.MemberVO;

import groovy.util.logging.Log4j;

@Log4j
public class CustomUserDetailsService implements UserDetailsService{
	@Autowired
	MemberMapper memberMapper;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("아아ㅏㅏㅏㅏㅏㅏㅏ"+username);
				
		MemberVO memVO = memberMapper.login(username);
		
		if(memVO==null) {
			throw new UsernameNotFoundException("no user");
		}
		System.out.println(memVO);
				
		return memVO ;
	}	
}
