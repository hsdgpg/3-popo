package com.popo.app.member.service;

import java.util.List;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceVO;


public interface MemberService {
	public List<MemberVO> getAllList();
	
	public boolean addMember(MemberVO memberVO);
	//공간 관리자 회원가입
	public boolean addSpaceMember(MemberVO memberVO);

	public int idCheck(String id);

	public MemberVO login(String memId, String memPw);

	public Long FindPwData(MemberVO memberVO);

	public void sendPwEmail(Long checkNo, String memEmail);

	public String FindIdData(MemberVO memberVO);
	
	public int memberRatingUpdaet(Long memNo); //팀 리더 업데이트 시켜줌
	
	public List<ShowVO> imminentShowList(); // 공연 날짜 임박한 공연 5개 
	
	public List<FundingVO> imminentfundingList(); // 마감 날짜 임박한 펀딩 5개
	
	public List<ShowReviewVO> recentShowReviewList(); // 최신 공연리뷰 5개
	
	public List<SpaceVO> spaceList(); // 최신 공간 5개

	public MemberVO loginInfo(String name);
	
	
}
