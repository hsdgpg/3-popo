package com.popo.app.member;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


import lombok.AllArgsConstructor;
 
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfiguration {
 
//	@Autowired
//	private DataSource dataSource;
	

	@Bean
	public static BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}	
	
	
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		/* @formatter:off */
		http
			.authorizeRequests()
				.requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()				
				.antMatchers("/page/index","/funding/**","/show/**","/user/**","/css/**","/js/**","/img/**","fonts/**").permitAll() // 설정한 리소스의 접근을 인증절차 없이 허용
				.antMatchers("/mypage/**","/notice/**","/page/practice**","/show/showTicketing","/show/showInsert","/match/**","/page/spaceList","/page/spaceInfo","port/**","/page/fundingInsert","/team/**").hasAnyAuthority("ROLE_MEMBER","ROLE_TEAMLEADER","ROLE_SPACEOWNER","ROLE_ADMIN") // 일반 유저
				.antMatchers("/teampage/teampage","teampage/teamPageMod").hasAnyAuthority("ROLE_TEAMLEADER") // 팀리더
                .antMatchers("/page/spaceRegist","/page/spaceModifyForm").hasAnyAuthority("ROLE_SPACEOWNER") // 공간대여자
                .antMatchers("/admin/**").hasAnyAuthority("ROLE_ADMIN") // 관리자    
//              .anyRequest().authenticated()
				.and()
			.csrf().disable()	//csrf 꺼놈, 이거 켜면 ajax post 다 멎음
//				.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()
			.formLogin()//.disable()
	            .loginPage("/user/login")
				.usernameParameter("memId")
				.passwordParameter("memPw")
//	            .loginProcessingUrl("boot/user/login")
//				.defaultSuccessUrl("/page/index")
				.permitAll()
				.successHandler(successhandler())
	            .and()
			.logout()
				.permitAll()
				.logoutUrl("/user/logout") // 로그아웃 URL (기본 값 : /logout)
				.logoutSuccessUrl("/user/login?logout") // 로그아웃 성공 URL (기본 값 : "/login?logout")
				.logoutRequestMatcher(new AntPathRequestMatcher("/user/logout")) // 주소창에 요청해도 포스트로 인식하여 로그아웃
				.deleteCookies("JSESSIONID") // 로그아웃 시 JSESSIONID 제거
				.invalidateHttpSession(true) // 로그아웃 시 세션 종료
				.clearAuthentication(true) // 로그아웃 시 권한 제거
				.and()
			.exceptionHandling()
				.accessDeniedPage("/error/errorpage");
							
		return http.build();
		/* @formatter:on */
	}

	@Bean
	public CustomLoginSuccessHandler successhandler() {
		return new CustomLoginSuccessHandler();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		return new CustomUserDetailsService();
	}

}
