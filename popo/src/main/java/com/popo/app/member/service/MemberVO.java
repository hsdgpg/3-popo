package com.popo.app.member.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;

@Data
public class MemberVO implements UserDetails {
	Long memNo;
	String memId;
	String memPw;
	String memName;
	String memNickname;
	String memRegistNo1;
	String memRegistNo2;
	String memTel;
	String memEmail;
	String addr;
	String detailAddress;
	String memAccountNo;
	String memRating;
	int teamNo;
	int memReportCount;
	String memProfilePic;
	String memPlaceCertiInfo;
	String positionName;
	int chatListNo;
	int memStatus;
	String tempPw;
	String role;
		
	int rn;
	String memRegistNo;
	String memAddress;
	String memDetailaddr;
	
	String newPw;
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		List<GrantedAuthority> auth = new ArrayList<>();

		String role = "";
		if (memRating.equals("MR01")) {
			role = "ROLE_MEMBER"; // 일반 유저
		} else if (memRating.equals("MR02")) {
			role = "ROLE_SPACEOWNER"; // 공간대여자
		} else if (memRating.equals("MR03")) {
			role = "ROLE_ADMIN"; // 관리자
		} else if (memRating.equals("MR04")) {
			role = "ROLE_TEAMLEADER";
		}
		System.out.println(role);
		auth.add(new SimpleGrantedAuthority(role));

		return auth;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return memPw;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return memId;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
