package com.popo.app.member.mapper;

import java.util.List;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceVO;


public interface MemberMapper {
	
	public List<MemberVO> selectAllEmpList();
	
	//일반회원 회원가입
	public boolean addMember(MemberVO memberVO);
	//공간 관리자 회원가입
	public boolean addSpaceMember(MemberVO memberVO);
	
	public int idCheck(String id);

	public MemberVO login(String memId);

	public MemberVO FindPwData(MemberVO memberVO);

	public void modifyPW(MemberVO memNo);

	public MemberVO FindIdData(MemberVO memberVO);		
	
	public int memberRatingUpdaet(Long memNo); //팀 리더 업데이트 시켜줌
	
	public List<ShowVO> imminentShowList(); // 공연 날짜 임박한 공연 5개   
	
	public List<FundingVO> imminentfundingList(); // 마감 날짜 임박한 펀딩 5개
	
	public List<ShowReviewVO> recentShowReviewList(); // 최신 공연리뷰 5개
	
	public List<SpaceVO> spaceList(); // 최신 공간 5개
	
	public MemberVO read(String memId);
	
	public int leaderPosition(Long memNo);
}
