package com.popo.app.member.service.impl;

import java.util.List;
import java.util.Random;

import javax.mail.internet.MimeMessage;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.mapper.MemberMapper;
import com.popo.app.member.service.MemberService;
import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceVO;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

	@Autowired
	MemberMapper memberMapper; 
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public List<MemberVO> getAllList() {
		// TODO Auto-generated method stub
		return memberMapper.selectAllEmpList();
	}

	@Override
	public boolean addMember(MemberVO memberVO) {
		// TODO Auto-generated method stub
		
		memberVO.setMemPw(bCryptPasswordEncoder.encode(memberVO.getMemPw()));

		return memberMapper.addMember(memberVO);		
	}

	@Override
	public int idCheck(String id) {
		// TODO Auto-generated method stub
		int cnt = memberMapper.idCheck(id);
		System.out.println("cnt: " + cnt);
		return cnt;
	}

	@Override
	public MemberVO login(String memId, String memPw) { //비교
		// TODO Auto-generated method stub
		MemberVO memberVO = memberMapper.login(memId);
		
		System.out.println(memPw);
		
		try {
//		if(memberVO.getMemPw().equals(memPw))
		if(bCryptPasswordEncoder.matches(memPw, memberVO.getMemPw()))	
			return memberVO;
		}catch(Exception e){
			return null;
		}
		return null;
	}
	
	@Override
	public String FindIdData(MemberVO memberVO) {
		// TODO Auto-generated method stub
		MemberVO memVO = memberMapper.FindIdData(memberVO);
		try {
		if(memVO.getMemEmail().equals(memberVO.getMemEmail())) {
			return memVO.getMemId();
		}else {
			return null;
		}}
		catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}	

	@Override
	public Long FindPwData(MemberVO memberVO) {
		// TODO Auto-generated method stub
		MemberVO memVO = memberMapper.FindPwData(memberVO);
		try {
		if(memVO.getMemEmail().equals(memberVO.getMemEmail())) {
			return memVO.getMemNo();
		}else {
			return null;
		}}
		catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public void sendPwEmail(Long checkNo, String memEmail) { 
		// TODO Auto-generated method stub
		String title = "포포몬스 임시 패스워드 발송";
		
		String tempPassword = RandomString(); // 랜덤 문자열 생성
		
		String body =  "<h1> 임시패스워드 : " + tempPassword + "</h1><br><a href= http://107.23.12.226:8081/boot/user/login> 포포몬스 로그인 하러 가기 </a>";
		
		sendMail(memEmail, title, body); // 이메일 전송
		
		insertTempPassword(checkNo, tempPassword); // 임시 비밀번호 db에 저장
		
	}
	
	// 임시 비번 저장
	private void insertTempPassword(Long checkNo, String tempPassword) { //insert
		MemberVO memVO = new MemberVO();
		memVO.setMemNo(checkNo);
		memVO.setTempPw(bCryptPasswordEncoder.encode(tempPassword)); // 암호화
		memberMapper.modifyPW(memVO);
	}
	
	@Autowired
	public JavaMailSender javaMailSender;
	
	public void sendMail(String memEmail, String title, String body) {
		// TODO Auto-generated method stub
		
		MimeMessage message = javaMailSender.createMimeMessage();
		
		try {
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
        
        // 1. 메일 수신자 설정
        messageHelper.setTo(memEmail);

        // 2. 메일 제목 설정
        messageHelper.setSubject(title);

        // 3. 메일 내용 설정
        // HTML 적용됨
        String content = body;
        messageHelper.setText(content,true);

        // 4. 메일 전송
        javaMailSender.send(message);
		}catch(Exception e) {

		}
	}

	@Override
	public int memberRatingUpdaet(Long memNo) {
		// TODO Auto-generated method stub
		return memberMapper.memberRatingUpdaet(memNo);
	}

	@Override
	public List<ShowVO> imminentShowList() {
		// TODO Auto-generated method stub
		return memberMapper.imminentShowList();
	}

	@Override
	public List<FundingVO> imminentfundingList() {
		// TODO Auto-generated method stub
		return memberMapper.imminentfundingList();
	}

	@Override
	public List<ShowReviewVO> recentShowReviewList() {
		// TODO Auto-generated method stub
		return memberMapper.recentShowReviewList();
	}

	@Override
	public List<SpaceVO> spaceList() {
		// TODO Auto-generated method stub
		return memberMapper.spaceList();
	}


	@Override
	public MemberVO loginInfo(String name) {
		MemberVO memberVO = memberMapper.login(name);
		return memberVO;
	}

	@Override
	public boolean addSpaceMember(MemberVO memberVO) {
		memberVO.setMemPw(bCryptPasswordEncoder.encode(memberVO.getMemPw()));
		return memberMapper.addSpaceMember(memberVO);	
	}
	
	
	// 랜덤 문자열 생성
	public static String RandomString() {
	    int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	                                   .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	                                   .limit(targetStringLength)
	                                   .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	                                   .toString();
	    System.out.println(generatedString);
	    return generatedString;
	 }	
	
	
}

