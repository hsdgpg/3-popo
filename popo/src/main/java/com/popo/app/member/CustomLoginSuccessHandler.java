package com.popo.app.member;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.popo.app.member.service.MemberService;
import com.popo.app.member.service.MemberVO;


public class CustomLoginSuccessHandler implements AuthenticationSuccessHandler {
	
	@Autowired
	private MemberService memberService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		HttpSession session = request.getSession();
		
		if(session != null) {
			
			MemberVO member =(MemberVO)session.getAttribute("login");
//			MemberVO member = (MemberVO)session.getAttribute("login");
			if(member == null) {
				member = memberService.loginInfo(authentication.getName());
				session.setAttribute("memVO", member);
				session.setAttribute("memId", member.getMemId());
				session.setAttribute("grade", member.getMemRating());
				session.setAttribute("login", member);
			}
		}	
		response.sendRedirect("/boot/page/index");
	}
}

