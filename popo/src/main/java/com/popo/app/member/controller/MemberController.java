package com.popo.app.member.controller;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.popo.app.admin.service.AdminService;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberService;
import com.popo.app.member.service.MemberVO;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceVO;
import com.popo.app.team.service.TeamService;

@Controller
public class MemberController {
	
	@Autowired
	MemberService memberService;
	
	@Autowired
	TeamService teamService;
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	// 메인페이지
	@GetMapping("/page/index")
	public String inputEmpForm(Model model,HttpSession session) {
		List<ShowVO> list = memberService.imminentShowList();
		model.addAttribute("imminentShowList",list);//임박 공연 5개
		List<FundingVO> fundingList = memberService.imminentfundingList();
		model.addAttribute("imminentfundingList",fundingList);//임박 펀딩 5개
		List<ShowReviewVO> showReviewList = memberService.recentShowReviewList();
		model.addAttribute("showReviewList",showReviewList);//최신 리뷰 4개
		List<SpaceVO> spaceList = memberService.spaceList();
		model.addAttribute("spaceList",spaceList);//최신 공간 5개
		List<ShowVO> ticketingRK = adminService.ticketingRanking();
		model.addAttribute("ticketingRK", ticketingRK);

		
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		
		if(memVO != null) {
			Long memNo = memVO.getMemNo();
			model.addAttribute("memNo",memNo);
			return "page/index";
		}
		return "page/index";
	}
	
	//아이디 중복체크
	@PostMapping("/user/join/idCheck")
	@ResponseBody
	public int idCheck(@RequestParam("memId") String memId) {
		int cnt = memberService.idCheck(memId);
		return cnt;		
	}
	
	// 회원가입
	@GetMapping("/user/JoinMemType")
	public String joinMemType() {
		return "user/JoinMemType";
	}
	
	//일반회원 회원가입
	@GetMapping("/user/join")
	public String joinpage() {
		
		return "user/join";
	}
	@PostMapping("/user/join")
	public String addMember(MemberVO memberVO) {
		memberService.addMember(memberVO);
		return "redirect:/user/joinComplete";
	}
	
	//공간대여 회원가입
	@GetMapping("/user/spaceJoin")
	public String spaceJoin() {
		
		return "user/spaceJoin";
	}
	
	@PostMapping("/user/spaceJoin")
	public String spaceJoin1(MemberVO memberVO) {
		memberService.addSpaceMember(memberVO);
		return "redirect:user/spaceJoinComplete";
	}
	
	//회원가입 완료
	@GetMapping("user/joinComplete")
	public String joinComplete() {
		return "user/joinComplete";
	}
	
	@GetMapping("user/spaceJoinComplete")
	public String spaceJoinComplete() {
		return "user/spaceJoinComplete";
	}
	
	// 로그인
	@GetMapping("/user/login")
	public String loginpage(HttpSession session) {
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		if(memVO != null) {
			return "redirect:/page/index"; // 로그인 돼서 세션 있을때
		}		
		return "user/login"; // 로그인 안됐을때
	}
	@PostMapping("/user/login")
	public String login(String memId, String memPw, HttpSession session, HttpServletResponse response) {
		MemberVO memVO = memberService.login(memId,memPw);
		
		if(memVO == null) {
			alert(response,"올바르지 않은 회원정보입니다.");// alert
			return "redirect:/user/login"; // 로그인 실패했을때
		}		
		session.setAttribute("memVO",memVO);
		
		return "redirect:/page/index"; // 로그인 성공했을때
	}
	
//	// 로그아웃
//	@RequestMapping("/user/logout")
//	public String logout(HttpSession session) {
//		session.invalidate();
//		return "redirect:/page/index";
//	}
	
	// 아이디 찾기
	@GetMapping("/user/idFind")
	public String idFind() {
		return "user/idFind";
	}
	@PostMapping("/user/idFind")
	public String idetificationFind(String memName, String memEmail,Model model,HttpServletResponse response) {
		
		MemberVO memberVO = new MemberVO();
		memberVO.setMemName(memName);
		memberVO.setMemEmail(memEmail);		
		
		String checkID = memberService.FindIdData(memberVO);
		
		if(checkID == null) {
			alert(response,"올바르지 않은 회원정보입니다."); // alert 
			return "redirect:/page/idFind"; // 실패
		}
		model.addAttribute("checkID",checkID);
		return "user/informId"; // 성공
	}
	@GetMapping("/user/informId")
	public String informId() {
		return "user/informId";
	}
	
	
	// 비번 찾기
	@GetMapping("/user/pwFind")
	public String PwFind() {
		return "user/pwFind";
	}
	@PostMapping("/user/pwFind")
	public String passwordFind(String memId, String memEmail, Model model,HttpServletResponse response){
		
		MemberVO memberVO = new MemberVO();
		memberVO.setMemId(memId);
		memberVO.setMemEmail(memEmail);
		
		
		Long checkNo = memberService.FindPwData(memberVO);
			
		if(checkNo == null) {
			alert(response,"올바르지 않은 회원정보입니다."); // alert
			return "redirect:/user/pwFind"; // 실패
		}
		
		memberService.sendPwEmail(checkNo,memEmail);
		
		model.addAttribute("memEmail",memEmail);
		
		return "user/informPw"; // 성공
	}
	@GetMapping("/user/informPw")
	public String informPw() {
		return "user/informPw";
	}
	
	@GetMapping("/error/errorpage")
	public String errorPage() {
		return "error/errorpage";
	}
	
	
	
	
	//알림창 띄우기
	public static void alert(HttpServletResponse response, String msg) {
	    try {
	        response.setContentType("text/html; charset=utf-8");
	        PrintWriter w = response.getWriter();
	        w.write("<script>alert('"+msg+"');history.go(-1);</script>");
	        w.flush();
	        w.close();
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	}
	

	
}
