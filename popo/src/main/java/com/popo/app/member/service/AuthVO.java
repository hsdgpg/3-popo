package com.popo.app.member.service;

import lombok.Data;

@Data
public class AuthVO {
	private String memId;
	private String memRating;
}
