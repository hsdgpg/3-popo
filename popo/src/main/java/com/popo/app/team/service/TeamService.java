package com.popo.app.team.service;

import java.util.List;

import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.replyReport.service.PostReplyVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;

public interface TeamService {
	
	//멤버 조회
	public MemberVO findMemberInfo(Long memNo);
		
	public int concertPlanTotalCount(int teamNo,Criteria cri);
	public int concertHistoryTotal(int teamNo,Criteria cri);
	
	//관리자가 조회하는용
	public MemberVO getTeam(int memNo);
	public MemberVO getTeam1(Long memNo);
	
	//연습영상 =======================================================
	//페이징
	public List<PracticeVO> teamPracticePagingList(int teamNo,int pageNum, int amount);
	//전체건수
	public int teamPracticeTotalCount(int teamNo,Criteria cri);
	//글쓰기
	public int practiceInsert(Long memNo, String  practiceName,String practiceContent,int teamNo);
	//상세페이지 단건조회.
	public PracticeVO teamPracticeDetail(int practiceNo, int teamNo);
	//게시글마다 댓글 개수
	public List<PracticeVO> CountNoticeReply(int teamNo) ;
	// 수정
	public int practiceMod(PracticeVO practiceVO); 
	//삭제(업데이트
	public int practiceDelete(int practiceNo); 
	//조회수 업데이트
	public void teamPracticeViewCount(int practiceNo,int teamNo);
	//댓글 전체 건수
	public int teamPracticeReplyTotalCount(int teamNo,int noticeNo,Criteria cri);
	//댓글 조회
	public List<ReplyVO> practiceReplyIs(int practiceNo,int teamNo,int pageNum, int amount);		
	//공지사항 댓글 추가
	public int pracaddReply(int noticeNo, Long memNo,String content,int teamNo);
	//댓글 삭제
	public void pracdeleteReply( int noticeNo, Long memNo, int commentNo);
	// 댓글 수정
	public int pracmodReply(String commentContent, Long memNo,int teamNo, int noticeNo, int commentNo);
	//연습 신고
	public void addReportPractice(PostReplyVO postReplyVO);	
	//연습 댓글 신고
	public void reportPracticeReply(ReplyReportVO replyReportVO);
		
	
	//팀 좋아요 여부 조회
	public Integer getTeamLike(Long memNo, int teamNo);
	
	//팀 좋아요
	public int teamLike(Long memNo, int teamNo);
	public int teamLikeUpdate(Long memNo, int teamNo);
	//팀 좋아요 해제
	public int teamUnLike(Long memNo, int teamNo);
		
	//팀별 멤버조회
	public List<TeamVO> getTeamMember(int teamNo);
	//팀멤버 정보 조회
	public TeamVO findTeamName(int teamNo);
	//공연이력 조회
	public List<ShowVO> concertHistory(int teamNo,Criteria cri);
	//공연일정
	public List<ShowVO> concertPlan(int teamNo,Criteria cri);
	//팀히스토리 팀 공지
	//public List<NoticeVO> teamNotice();
	
	
	
	
	//공지사항 ======================================================================
	//글쓰기
	public int noticeInsert(int memNo, String noticeName,String noticeContent, int teamNo);
	//상세페이지 //단건조회.
	public NoticeVO teamNoticeDetail(int noticeNo,int teamNo);
	// 수정
	public int noticeMod(NoticeVO noticeVO); 
	//삭제(업데이트
	public int noticeDelete(int noticeNo); 
	//공지사항 조회수 업데이트
	public void NoticeViewCount( int noticeNo, int teamNo);
	//공지사항 게시판 댓글 조회
	public List<ReplyVO> NoticeReplyIs(int noticeNo,int teamNo,int pageNum, int amount);	
	//공지 신고
	public int addReportNotice(PostReplyVO postReplyVO);
	//공지 댓글 신고	
	public void reportNoticeReply(ReplyReportVO replyReportVO);
	//댓글 건수
	public int NoticeReplyTotalCount(Criteria cri,int teamNo,int noticeNo);
	//공지사항 댓글 추가
	//public void addReply(int noticeNo, String content);
	public int addReply(int noticeNo,Long memNo, String content, int teamNo);
	//공지사항 댓글 수정
	public int modReply(String commentContent, int itemMemNo, int commentNo);
	//댓글 삭제
	public void deleteReply( int commentNo);
	
	//공지사항 대댓글 조회
	public List<ReplyVO> reReply(int commentNo,int noticeNo, int teamNo);
	//List<TeamReplyVO> reReply(int noticeNo, int commentNo);
	
	
	
	
	//소통창구 댓글 조회
	public List<ReplyVO> comuReplyList(int teamNo,int pageNum);
	//페이징 확인
	//public List<ReplyVO> comuReplyList1(Criteria cri);
	//소통창구 전체 댓글 건수
	public int ComuReplyTotalCount(int teamNo,Criteria cri);
	//소통창구 댓글 추가
	public int ComuaddReply( Long memNo,String content, int teamNo);
	//소통 댓글 수정
	public void ComuModReply(String commentContent, int commentNo, int itemMemNo, int teamNo);
	//소통 댓글 삭제
	public void ComuDeleteReply(int commentNo);
		
	
	//팀등록
	public int teamInsert(TeamVO teamVO);
	
		
}
