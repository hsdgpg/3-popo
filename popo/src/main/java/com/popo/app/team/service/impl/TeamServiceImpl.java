package com.popo.app.team.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.replyReport.service.PostReplyVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.mapper.TeamMapper;
import com.popo.app.team.service.TeamLikeVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamService;
import com.popo.app.team.service.TeamVO;

@Service
public class TeamServiceImpl implements TeamService {

	@Autowired
	TeamMapper teamMapper;

	
	@Override
	public MemberVO findMemberInfo(Long memNo) {
		// TODO Auto-generated method stub
		return teamMapper.findMemberInfo(memNo);
	}

	@Override
	public MemberVO getTeam(int memNo) {
		// TODO Auto-generated method stub
		return teamMapper.getTeam(memNo);
	}
	
	@Override
	public MemberVO getTeam1(Long memNo) {
		// TODO Auto-generated method stub
		return teamMapper.getTeam1(memNo);
	}


	@Override
	public int concertPlanTotalCount(int teamNo, Criteria cri) {
		// TODO Auto-generated method stub
		return teamMapper.concertPlanTotalCount(teamNo, cri.getPageNum());
	}

	@Override
	public int concertHistoryTotal(int teamNo, Criteria cri) {
		// TODO Auto-generated method stub
		return teamMapper.concertHistoryTotal(teamNo,cri.getPageNum());
	}

	
	
	@Override
	public List<TeamVO> getTeamMember(int teamNo) {
		
		return teamMapper.getTeamMember(teamNo);
	}

	@Override
	public TeamVO findTeamName(int teamNo){
		
		return teamMapper.findTeamName(teamNo);
	}

	@Override
	public List<ShowVO> concertHistory(int teamNo,Criteria cri) {
		
		return teamMapper.concertHistory(teamNo,cri.getPageNum(), cri.getAmount());
	}

	@Override
	public List<ShowVO> concertPlan(int teamNo,Criteria cri) {
		
		//공연일자 비교해서 시간이 지나면 공연예정 -> 공연완료로 업데이트
		List<ShowVO> result = teamMapper.concertPlan(teamNo,cri.getPageNum(),cri.getAmount());
		return result;
			
	}

	
	@Override
	public NoticeVO teamNoticeDetail(int noticeNo,int teamNo) {
		return teamMapper.teamNoticeDetail(noticeNo, teamNo);
	}

	@Override
	public void NoticeViewCount( int noticeNo, int teamNo) {
		teamMapper.NoticeViewCount(noticeNo, teamNo);
	}


	@Override
	public List<ReplyVO> comuReplyList(int teamNo,int pageNum) {
		return teamMapper.comuReplyList(teamNo,pageNum);
	}

	@Override
	public int teamLike(Long memNo, int teamNo) {
		return teamMapper.teamLike(memNo,teamNo);
	}

	@Override
	public int teamUnLike(Long memNo, int teamNo) {
		return teamMapper.teamUnLike(memNo,teamNo);
	}
	
	@Override
	public int teamLikeUpdate(Long memNo, int teamNo) {
		return teamMapper.teamLikeUpdate(memNo,teamNo);
	}

	@Override
	public Integer getTeamLike(Long memNo ,int teamNo) {
		Integer result1 = teamMapper.getTeamLike(memNo,teamNo);
		int result ;
		
		if(result1 != null) {
			return result1; 
		}else {
			result = 0;
			return result;
		}

	}

	@Override
	public List<ReplyVO> NoticeReplyIs(int noticeNo,int teamNo,int pageNum, int amount){
		return teamMapper.NoticeReplyIs(noticeNo,teamNo,pageNum,amount);
	}

	@Override
	public int NoticeReplyTotalCount(Criteria cri,int teamNo, int noticeNo) {
		return teamMapper.NoticeReplyTotalCount(cri,teamNo, noticeNo);
	}
	
	@Override
	public void deleteReply( int commentNo) {
		teamMapper.deleteReply( commentNo);
		
	}

	@Override
	public int teamInsert(TeamVO teamVO) {
		// TODO Auto-generated method stub
		int result = teamMapper.teamInsert(teamVO);
		return result;
	}

	@Override
	public int addReply(int noticeNo,Long memNo, String content,int teamNo) {
				int result = teamMapper.addReply(noticeNo,memNo,content,teamNo);
				return  result;
	}

	@Override
	public List<ReplyVO> reReply(int commentNo,int noticeNo, int teamNo) {
		return teamMapper.reReply(commentNo, noticeNo,teamNo);
	}
	
	@Override
	public int modReply(String commentContent, int memNo, int commentNo) {
		
		//return teamMapper.modReply(commentContent,memNo,teamNo,noticeNo,commentNo);
		return teamMapper.modReply(commentContent,memNo,commentNo);
		
	}

	@Override
	public int ComuReplyTotalCount(int teamNo,Criteria cri) {
		// TODO Auto-generated method stub
		return teamMapper.ComuReplyTotalCount(teamNo,cri);
	}

	@Override
	public int ComuaddReply(Long memNo, String content,int teamNo) {
		return teamMapper.ComuaddReply(memNo, content,teamNo);
	}

	@Override
	public void ComuModReply(String commentContent, int commentNo, int itemMemNo, int teamNo) {
		
		teamMapper.ComuModReply(commentContent, commentNo, itemMemNo,teamNo);
	}

	@Override
	public void ComuDeleteReply(int commentNo) {
		
		teamMapper.ComuDeleteReply(commentNo);
		
	}

//	@Override
//	public List<ReplyVO> comuReplyList1(int teamNo,int pageNum) {
//		
//		return teamMapper.comuReplyList1(teamNo,pageNum);
//	}

	@Override
	public List<PracticeVO> teamPracticePagingList(int teamNo,int pageNum, int amount) {
		
		
		return teamMapper.teamPracticePagingList(teamNo,pageNum,amount);
	}

	@Override
	public int teamPracticeTotalCount(int teamNo,Criteria cri) {
		// TODO Auto-generated method stub
		return teamMapper.teamPracticeTotalCount(teamNo,cri);
	}

	@Override
	public PracticeVO teamPracticeDetail(int practiceNo, int teamNo) {
		
		return teamMapper.teamPracticeDetail(practiceNo,teamNo);
	}

	@Override
	public void teamPracticeViewCount(int practiceNo, int teamNo) {
		
		teamMapper.teamPracticeViewCount(practiceNo,teamNo);
	}

	@Override
	public int teamPracticeReplyTotalCount(int teamNo,int noticeNo,Criteria cri ) {
		// TODO Auto-generated method stub
		return teamMapper.teamPracticeReplyTotalCount(teamNo,noticeNo,cri);
	}

	@Override
	public List<ReplyVO> practiceReplyIs(int practiceNo,int teamNo, int pageNum, int amount) {
		// TODO Auto-generated method stub
		return teamMapper.practiceReplyIs(practiceNo,teamNo, pageNum, amount);
	}

	@Override
	public int pracaddReply(int noticeNo, Long memNo,String content,int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.pracaddReply(noticeNo, memNo,content, teamNo );
	}

	@Override
	public void pracdeleteReply(int noticeNo, Long memNo, int commentNo) {
		teamMapper.pracdeleteReply(noticeNo, memNo,commentNo);
		
	}

	@Override
	public int pracmodReply(String commentContent, Long memNo, int teamNo,int noticeNo, int commentNo) {
		// TODO Auto-generated method stub
		return teamMapper.pracmodReply(commentContent, memNo,teamNo,noticeNo,commentNo);
	}

	@Override
	public int noticeInsert(int memNo, String noticeName,String noticeContent, int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.noticeInsert(memNo,noticeName,noticeContent,teamNo);
	}

	@Override
	public int practiceInsert(Long memNo, String  practiceName,String practiceContent,int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.practiceInsert(memNo,practiceName, practiceContent,teamNo);
	}

	@Override
	public int practiceMod(PracticeVO practiceVO) {
		// TODO Auto-generated method stub
		return teamMapper.practiceMod(practiceVO);
	}

	@Override
	public int practiceDelete(int practiceNo) {
		// TODO Auto-generated method stub
		return teamMapper.practiceDelete(practiceNo);
	}

	@Override
	public int noticeMod(NoticeVO noticeVO) {
		// TODO Auto-generated method stub
		return teamMapper.noticeMod(noticeVO);
	}

	@Override
	public int noticeDelete(int noticeNo) {
		// TODO Auto-generated method stub
		return teamMapper.noticeDelete(noticeNo);
	}

	@Override
	public List<PracticeVO> CountNoticeReply(int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.CountNoticeReply(teamNo);
	}

	@Override
	public void addReportPractice(PostReplyVO postReplyVO) {
		// TODO Auto-generated method stub
		 teamMapper.addReportPractice(postReplyVO);
	}

	@Override
	public int addReportNotice(PostReplyVO postReplyVO) {
		// TODO Auto-generated method stub
		return teamMapper.addReportNotice(postReplyVO);
	}

	@Override
	public void reportNoticeReply(ReplyReportVO replyReportVO) {
		// TODO Auto-generated method stub
		teamMapper.reportNoticeReply(replyReportVO);
	}

	@Override
	public void reportPracticeReply(ReplyReportVO replyReportVO) {
		teamMapper.reportPracticeReply(replyReportVO);
		
	}

	

	





	
	

	

	

	
	
	
	
	
}
