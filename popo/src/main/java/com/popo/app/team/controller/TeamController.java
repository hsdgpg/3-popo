package com.popo.app.team.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.replyReport.service.PostReplyVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.team.service.TeamLikeVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamService;
import com.popo.app.team.service.TeamVO;

//지은 팀 히스토리 관리
@Controller
public class TeamController {

	@Autowired
	TeamService teamService;

	// 팀 히스토리 선택시 팀 조회-> 그 팀의 정보 보여줄 필요 있음.
	// teamNo 조건 수정해야함.

	// 팀 히스토리 공간
	// 좋아요====================================================================================================
	@RequestMapping(value = "/team/teamLike", method = RequestMethod.POST)
	@ResponseBody
	public String teamLike( Model model, 
							HttpServletRequest request) {

		String msg = null;
		HttpSession session = request.getSession();
		TeamLikeVO mem = new TeamLikeVO();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		TeamVO teamInfo = (TeamVO) session.getAttribute("teamInfoForHistory");
		
		// 좋아요 여부, 0이면 정보 없음, 1이면 좋아요한상태, 2이면 좋아요 하지 않았지만 db에 정보가 있는 상태.
		// 삭제날짜 걸어주기 ->
		// teamNo VO에 담아서 한꺼번에 넘겨주기 ===================== 수정필요 (전체)
		MemberVO memInfo = teamService.findMemberInfo(memVO.getMemNo());
		session.setAttribute("memVO", memInfo);
		MemberVO memVO1 = (MemberVO) session.getAttribute("memVO");
		
		if( teamInfo == null  || teamInfo.getTeamNo() == 0) {
			
			int likeInfo = teamService.getTeamLike(memVO1.getMemNo(),memVO1.getTeamNo());
			if (likeInfo == 0) { // 0이면 insert
				teamService.teamLike(memVO1.getMemNo(),memVO1.getTeamNo());
				msg = "fail";
			}else if (likeInfo == 2) { // 2이면 update
				teamService.teamLikeUpdate(memVO1.getMemNo(),memVO1.getTeamNo());
				msg = "success";
			}
			
			model.addAttribute("teamlikeInfo", likeInfo);
		}else {
			
			int likeInfo = teamService.getTeamLike(memVO1.getMemNo(),teamInfo.getTeamNo());
			if (likeInfo == 0) { // 0이면 insert
				teamService.teamLike(memVO1.getMemNo(),teamInfo.getTeamNo());
				msg = "fail";
			}else if (likeInfo == 2) { // 2이면 update
				teamService.teamLikeUpdate(memVO1.getMemNo(),teamInfo.getTeamNo());
				msg = "success";
			}
			
			
		}
		
		return msg;
	}

	
	
	// 팀 히스토리 공간 좋아요 해제
	@RequestMapping(value = "/team/teamUnLike", method = RequestMethod.POST)
	@ResponseBody
	public String teamUnLike(Model model, 
							 HttpServletRequest request) {

		HttpSession session = request.getSession();
		TeamLikeVO mem = new TeamLikeVO();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		TeamVO teamInfo = (TeamVO) session.getAttribute("teamInfoForHistory");
		
		MemberVO memInfo = teamService.findMemberInfo(memVO.getMemNo());
		session.setAttribute("memVO", memInfo);
		MemberVO memVO1 = (MemberVO) session.getAttribute("memVO");
		
		String msg = null;
		
		
		if( teamInfo == null  || teamInfo.getTeamNo() == 0) {
			int result = teamService.teamUnLike(memVO1.getMemNo(),memVO1.getTeamNo());
			if (result > 0) {
				msg = "success";
			} else {
				msg = "fail";
			}
		}else{
			
			int result = teamService.teamUnLike(memVO1.getMemNo(),teamInfo.getTeamNo());
			if (result > 0) {
				msg = "success";
			} else {
				msg = "fail";
			}
		}

		return msg;

	}

	// 팀 히스토리 -
	// 팀정보,공연이력====================================================================================================
	// @GetMapping("/team/teamhispage")
	//공연이력에서 넘어오는 경우 -> teamVO 로 받음. 
	@RequestMapping(value = "/team/teamhispage")
	public String teamhispage(  Model model, Criteria cri,
								TeamVO teamVO,
								HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamLikeVO mem = new TeamLikeVO();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		//show에서 team정보가 넘어온다면.  
		session.setAttribute("teamInfoForHistory", teamVO);
		
		//session 다시 저장하기 위해서 .
		MemberVO memInfo = teamService.findMemberInfo(memVO.getMemNo());
		session.setAttribute("memVO", memInfo);
		MemberVO memVO1 = (MemberVO) session.getAttribute("memVO");
		
		
		//로그인시-team_no 팀이 결성되었을때 
		if(teamVO == null  || teamVO.getTeamNo() == 0 && memVO1.getTeamNo()>0) {
			System.out.println("===============================");
			System.out.println(memVO1);
			
			//팀 전체 정보 
			TeamVO team = teamService.findTeamName(memVO1.getTeamNo());
			
			int likeInfo = teamService.getTeamLike(memVO1.getMemNo(),memVO1.getTeamNo());
			int total  = teamService.concertHistoryTotal(memVO1.getTeamNo(),cri);
			
			model.addAttribute("teamlikeInfo", likeInfo);
			model.addAttribute("teamMemberList", teamService.getTeamMember(memVO1.getTeamNo()));
			session.setAttribute("teamList", team);
			
			model.addAttribute("concertHistory", teamService.concertHistory(memVO1.getTeamNo(),cri));
			model.addAttribute("pageMaker", new PageDTO(cri,total));
			return "team/teamhistory";
		}else if(teamVO.getTeamNo()>0) {
			
			//팀 전체 정보 
			TeamVO team = teamService.findTeamName(teamVO.getTeamNo());
			// 좋아요 여부 ===================== mem 정보만 바로 넘기기
			
			int likeInfo = teamService.getTeamLike(memVO1.getMemNo(),teamVO.getTeamNo());
			int total  = teamService.concertHistoryTotal(teamVO.getTeamNo(),cri);
			model.addAttribute("teamlikeInfo", likeInfo);
			model.addAttribute("teamMemberList", teamService.getTeamMember(teamVO.getTeamNo()));
			session.setAttribute("teamList", team);
			model.addAttribute("concertHistory", teamService.concertHistory(teamVO.getTeamNo(),cri));
			model.addAttribute("pageMaker", new PageDTO(cri,total));
			return "team/teamhistory";
		}else if(memVO1.getTeamNo() == 0 || teamVO.getTeamNo() == 0 || teamVO == null) {
			return "team/noneTeam";
		}
		
		
		return "team/noneTeam";

	}

	
	
	//========================================================================================TeamLike수정중
	// 팀 히스토리-공연일정
	@GetMapping("/team/teamplan")
	public String teamplan( Model model, Criteria cri,
							HttpServletRequest request
							) {
		HttpSession session = request.getSession();
		TeamVO teamInfo = (TeamVO) session.getAttribute("teamInfoForHistory");

		// 좋아요 여부
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");

		
		if( teamInfo == null  || teamInfo.getTeamNo() == 0) {
			int likeInfo = teamService.getTeamLike(memVO.getMemNo(),memVO.getTeamNo());
			int total = teamService.concertPlanTotalCount(memVO.getTeamNo(),cri);
			model.addAttribute("teamlikeInfo", likeInfo);
			model.addAttribute("concertHistory", teamService.concertPlan(memVO.getTeamNo(),cri));
			model.addAttribute("pageMaker", new PageDTO(cri,total));
			
		}else{
			int likeInfo = teamService.getTeamLike(memVO.getMemNo(),teamInfo.getTeamNo());
			int total = teamService.concertPlanTotalCount(teamInfo.getTeamNo(),cri);
			model.addAttribute("teamlikeInfo", likeInfo);
			model.addAttribute("concertHistory", teamService.concertPlan(teamInfo.getTeamNo(),cri));
			model.addAttribute("pageMaker", new PageDTO(cri,total));
		}

		return "team/teamplan";
	}

	// 팀 연습영상
	// 게시판==============================================================================================================
	@GetMapping("/team/practice")
	public String practice( Criteria cri, 
							Model model,
							HttpServletRequest request) {

		cri.setAmount(10);
		cri.setKeyword("");
		System.out.println(cri);
		System.out.println("=============================================cri");
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		TeamVO teamInfo = (TeamVO) session.getAttribute("teamInfoForHistory");
		
		if(teamInfo == null  || teamInfo.getTeamNo() == 0) {
			List<PracticeVO> list = teamService.teamPracticePagingList(memVO.getTeamNo(),cri.getPageNum(),cri.getAmount());
			List<PracticeVO> reply = teamService.CountNoticeReply(memVO.getTeamNo());
			int total = teamService.teamPracticeTotalCount(memVO.getTeamNo(),cri);
			int likeInfo = teamService.getTeamLike(memVO.getMemNo(),memVO.getTeamNo());
			model.addAttribute("teamlikeInfo", likeInfo);
			model.addAttribute("PracticeNotice", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			model.addAttribute("CountReply", reply);
			
			return "team/practice";
			
		}else  {
			List<PracticeVO> list = teamService.teamPracticePagingList(teamInfo.getTeamNo(),cri.getPageNum(),cri.getAmount());
			List<PracticeVO> reply = teamService.CountNoticeReply(teamInfo.getTeamNo());
			int total = teamService.teamPracticeTotalCount(teamInfo.getTeamNo(),cri);
			int likeInfo = teamService.getTeamLike(memVO.getMemNo(),memVO.getTeamNo());
			model.addAttribute("teamlikeInfo", likeInfo);
			model.addAttribute("PracticeNotice", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			model.addAttribute("CountReply", reply);
			return "team/practice";
		}
		
	}

	
	
	// 글 단건조회
	@RequestMapping(value = { "/team/teamPracticeDetail"})
	public String teamPracticeDetail(PracticeVO practice, Model model, 
									HttpServletRequest request, 
									Criteria cri) {

		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		System.out.println(practice);
		//practiceNo, memNo만 쓸 수 있음.
		
		//관리자 접근
		if(teamVO == null ) {
			
			System.out.println("=============================================");
			System.out.println("여기냐고~~~~~~~~");
			
			MemberVO memberInfo = teamService.getTeam1(practice.getMemNo());
			int total = teamService.teamPracticeReplyTotalCount( memberInfo.getTeamNo(),practice.getPracticeNo(),cri);
			teamService.teamPracticeViewCount(practice.getPracticeNo(),memberInfo.getTeamNo());
			PracticeVO vo = teamService.teamPracticeDetail(practice.getPracticeNo(),memberInfo.getTeamNo());
			//팀 전체 정보 
			TeamVO team = teamService.findTeamName(memberInfo.getTeamNo());
			System.out.println(team);
			System.out.println("=======================================================");
			System.out.println(vo);
			session.setAttribute("teamList", team);
			model.addAttribute("teamNoticeboard", vo);
			model.addAttribute("NoticeNo", practice.getPracticeNo());
			model.addAttribute("viewCount", practice.getPracticeView());
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			
			return "team/practiceDetail";
			
		}
		
		int total = teamService.teamPracticeReplyTotalCount(  practice.getTeamNo(),practice.getPracticeNo(),cri);
		teamService.teamPracticeViewCount(practice.getPracticeNo(),teamVO.getTeamNo());
		PracticeVO vo = teamService.teamPracticeDetail(practice.getPracticeNo(),teamVO.getTeamNo());

		model.addAttribute("teamNoticeboard", vo);
		model.addAttribute("NoticeNo", practice.getPracticeNo());
		model.addAttribute("viewCount", practice.getPracticeView());
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		return "team/practiceDetail";

	}
	
	@PostMapping("/team/reportPractice")
	@ResponseBody
	public void reportPractice(PostReplyVO postReplyVO, Model model) {
		try {
			teamService.addReportPractice(postReplyVO);
		} catch (Exception e) {
			// 예외 처리 코드
			e.printStackTrace();
		}
	}

	// 글 작성
	@GetMapping("/team/practiceInsertForm")
	public String practiceInsertForm() {
		return "team/practiceInsertForm";
	}

	@PostMapping("/team/practiceInsert")
	public String practiceInsert(PracticeVO practice, HttpSession session) {
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		
		
		teamService.practiceInsert(memNo,practice.getPracticeName(),practice.getPracticeContent(),practice.getTeamNo());
		return "redirect:/team/practice";
	}

	// 글 수정
	@RequestMapping(value = { "/team/practiceModForm" })
	public String practiceModForm(@RequestParam(value = "practiceNo") Integer practiceNo,
								@RequestParam(value = "practiceName") String practiceName,
								@RequestParam(value = "practiceContent") String practiceContent,
								Model model) {

		model.addAttribute("practiceNo", practiceNo);
		model.addAttribute("practiceName", practiceName);
		model.addAttribute("practiceContent", practiceContent);
		return "team/practiceModForm";
	}

	@PostMapping("/team/practiceMod")
	public String practiceMod(PracticeVO practice) {
		teamService.practiceMod(practice);
		return "redirect:/team/practice";
	}

	// 삭제
	@RequestMapping("/team/practiceDelete")
	public String practiceDelete(@RequestParam(value = "practiceNo", required = false) Integer practiceNo) {
		teamService.practiceDelete(practiceNo);
		return "redirect:/team/practice";
	}

	// 댓글조회
	@ResponseBody
	@PostMapping("/team/prareplyList")
	public List<ReplyVO> prareplyList(Criteria cri,
										@RequestParam(value = "noticeNo") Integer noticeNo,
										@RequestParam(value = "pageNum") Integer pageNum, HttpServletRequest request,
										Model model,
										HttpSession session
										) {
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		
		List<ReplyVO> reply = teamService.practiceReplyIs(noticeNo, teamVO.getTeamNo(),pageNum, 5);
		return reply;
	}

	// 댓글달기 ajax 형식
		@ResponseBody
		@RequestMapping(value = "/team/pracaddReply2", method = RequestMethod.POST)
		public int pracaddReply2(@RequestParam(value = "noticeNo") Integer noticeNo,
								 @RequestParam(value = "commentContent") String commentContent, 
								 HttpServletRequest request) {
			HttpSession session = request.getSession();
			ReplyVO mem = new ReplyVO();
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
			Long memId = memVO.getMemNo();
			

			int msg;
			// content.equals()

			return teamService.pracaddReply(noticeNo, memId, commentContent,teamVO.getTeamNo());

		}

		
	//댓글신고
		@ResponseBody
		@PostMapping("/team/reportPracticeReply")
		public void reportPracticeReply(ReplyReportVO replyReportVO) {
			
			System.out.println(replyReportVO);
			teamService.reportPracticeReply(replyReportVO);
			
		}
		
		
		
	// 댓글 삭제
	// int noticeNo, int memNo, int commentNo
		@ResponseBody
		@PostMapping("/team/pracdeleteReply")
		public void pracdeleteReply(@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
									HttpServletRequest request,
									@RequestParam(value = "commentNo", required = false) Integer commentNo) {
			HttpSession session = request.getSession();
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memId = memVO.getMemNo();
			teamService.pracdeleteReply(noticeNo, memId, commentNo);

		}


	// 댓글 수정
		@ResponseBody
		@PostMapping("/team/pracmodReply")
		public void pracmodReply(@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
								HttpServletRequest request,
								@RequestParam(value = "commentContent", required = false) String commentContent,
								@RequestParam(value = "commentNo", required = false) Integer commentNo) {
			HttpSession session = request.getSession();
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
			Long memId = memVO.getMemNo();
			int result = teamService.pracmodReply(commentContent, memId,teamVO.getTeamNo(), noticeNo, commentNo);
			
		}

	// 팀 공지사항 상세페이지
	// ====================================================================================================
	@RequestMapping(value = { "/team/teamNoticeDetail" })
	public String get(
					 NoticeVO noticeVO,
					 Model model,
					 HttpServletRequest request, Criteria cri) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		
		
		if(teamVO == null || noticeVO != null){
			
			MemberVO memberInfo = teamService.getTeam(noticeVO.getMemNo());
			teamService.NoticeViewCount( noticeVO.getNoticeNo(),memberInfo.getTeamNo());
			NoticeVO vo = teamService.teamNoticeDetail(noticeVO.getNoticeNo(),memberInfo.getTeamNo());
			TeamVO team = teamService.findTeamName(memberInfo.getTeamNo());
			session.setAttribute("teamList", team);
			model.addAttribute("teamNoticeboard", vo);
			model.addAttribute("NoticeNo", noticeVO.getNoticeNo());
			model.addAttribute("viewCount", noticeVO.getViewCount());

			return "team/teamNoticeDetail";
		}else if(teamVO != null) {
			teamService.NoticeViewCount( noticeVO.getNoticeNo(),teamVO.getTeamNo());
			NoticeVO vo = teamService.teamNoticeDetail(noticeVO.getNoticeNo(),teamVO.getTeamNo());
			model.addAttribute("teamNoticeboard", vo);
			model.addAttribute("NoticeNo", noticeVO.getNoticeNo());
			model.addAttribute("viewCount", noticeVO.getViewCount());

			return "team/teamNoticeDetail";			
		}
		
		MemberVO memberInfo = teamService.getTeam(noticeVO.getMemNo());
		teamService.NoticeViewCount( noticeVO.getNoticeNo(),memberInfo.getTeamNo());
		NoticeVO vo = teamService.teamNoticeDetail(noticeVO.getNoticeNo(),memberInfo.getTeamNo());
		model.addAttribute("teamNoticeboard", vo);
		model.addAttribute("NoticeNo", noticeVO.getNoticeNo());
		model.addAttribute("viewCount", noticeVO.getViewCount());

		return "team/teamNoticeDetail";

	}
	
	@ResponseBody
	@PostMapping("/team/reportTeamNotice")
	public int addReportNotice(HttpServletRequest request,
								@RequestParam(value = "noticeNo") Integer noticeNo,
								@RequestParam(value = "memNo") Long memNo,
								@RequestParam(value = "postReportReason") String postReportReason,
								@RequestParam(value = "postReportContent") String postReportContent
								) {
		
		PostReplyVO postReplyVO = new PostReplyVO();
		postReplyVO.setNoticeNo(noticeNo);
		postReplyVO.setMemNo(memNo);
		postReplyVO.setPostReportReason(postReportReason);
		postReplyVO.setPostReportContent(postReportContent);
		
		System.out.println("==================================신고결과값");
		System.out.println(postReplyVO);
		return teamService.addReportNotice(postReplyVO);
	}
	

	@GetMapping("/team/reply")
	public String reply(Model model,
			            Criteria cri ,
						@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
						HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");

		int total = teamService.NoticeReplyTotalCount(cri,teamVO.getTeamNo(), noticeNo);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		return "team/Reply";
	}

	//공지 댓글 신고
	@PostMapping("/team/reportNoticeReply")
	@ResponseBody
	public void reportNoticeReply(ReplyReportVO replyReportVO, Model model) {
		try {
			
			teamService.reportNoticeReply(replyReportVO);
		} catch (Exception e) {
			// 예외 처리 코드
			e.printStackTrace();
		}
	}

	// 글 작성
	@GetMapping("/team/NoticeInsertForm")
	public String NoticeInsertForm() {
		return "team/teamnoticeInsertForm";
	}

	@PostMapping("/team/noticeInsert")
	public String NoticeInsert( NoticeVO noticeVO, 
								HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		int memNo = Math.toIntExact(memVO.getMemNo());
		teamService.noticeInsert(memNo,noticeVO.getNoticeName(),noticeVO.getNoticeContent(), teamVO.getTeamNo());
		return "redirect:/team/teamNotice";
	}

	// 글 수정
	@RequestMapping(value = { "/team/noticeModForm" })
	public String noticeModForm(@RequestParam(value = "noticeNo") Integer noticeNo,
			@RequestParam(value = "noticeName", required = false) String noticeName,
			@RequestParam(value = "memNo", required = false) Integer memNo,
			@RequestParam(value = "noticeContent", required = false) String noticeContent, Model model) {

		model.addAttribute("noticeNo", noticeNo);
		model.addAttribute("noticeName", noticeName);
		model.addAttribute("memNo", memNo);
		model.addAttribute("noticeContent", noticeContent);

		return "team/noticeModForm";
	}

	@PostMapping("/team/noticeMod")
	public String noticeMod(NoticeVO noticeVO) {
		teamService.noticeMod(noticeVO);
		return "redirect:/team/teamNotice";
	}

	// 삭제
	@RequestMapping(value = { "/team/NoticeDelete" })
	public String NoticeDelete(@RequestParam(value = "noticeNo") Integer noticeNo) {
		teamService.noticeDelete(noticeNo);
		return "redirect:/team/teamNotice";
	}
	

	// 댓글달기 ajax 형식
		@ResponseBody
		@RequestMapping(value = "/team/addReply2", method = RequestMethod.POST)
		public int addReply2(@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
				@RequestParam(value = "commentContent", required = false) String commentContent,
				HttpServletRequest request) {
			// System.out.println("ajax 댓글 noticeNo "+noticeNo);
			// System.out.println("ajax 댓글 content "+ commentContent );
			HttpSession session = request.getSession();
			
			ReplyVO mem = new ReplyVO();
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
			Long memId = memVO.getMemNo();
			

			int msg;

			if (commentContent != null || noticeNo != null) {

				return teamService.addReply(noticeNo, memId, commentContent, teamVO.getTeamNo());

			} else {
				// msg = "fail";
				msg = 0;
				return msg;
			}

		}

//			//댓글 조회 -> ajax 통신 -> collection for(List)댓글 -> List대댓글
		@ResponseBody
		@PostMapping("/team/replyList")
		public List<ReplyVO> replyList(Criteria cri, @RequestParam(value = "noticeNo", required = false) Integer noticeNo,
				@RequestParam(value = "pageNum", required = false) Integer pageNum, HttpServletRequest request,
				Model model) {
			// 댓글 5개만 가져오기.
			HttpSession session = request.getSession();
			TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
			List<ReplyVO> reply = teamService.NoticeReplyIs(noticeNo, teamVO.getTeamNo(),pageNum, 5);

			String msg;
			if (reply != null) {
				// model.addAttribute("AjaxreplyList", reply);

				return reply;
			} else {
				msg = "fail";
			}

			return null;
		}

	
	// 댓글 수정
	@ResponseBody
	@PostMapping("/team/modReply")
	public int modReply(@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
			@RequestParam(value = "itemMemNo", required = false) Integer itemMemNo,
			// @RequestParam(value="itemTeamNo" ,required=false) Integer itemTeamNo,
			@RequestParam(value = "commentContent", required = false) String commentContent,
			@RequestParam(value = "commentNo", required = false) Integer commentNo) {

		// int result =
		// teamService.modReply(commentContent,itemMemNo,itemTeamNo,noticeNo,commentNo);
		int result = teamService.modReply(commentContent, itemMemNo, commentNo);
		if (result > 0) {
			return 1;
		}
		return 0;
	}

	// 댓글 삭제
	// int noticeNo, int memNo, int commentNo
	@ResponseBody
	@PostMapping("/team/deleteReply")
	public void deleteReply(@RequestParam(value = "noticeNo", required = true) Integer noticeNo,
							@RequestParam(value = "memNo", required = false) Integer memNo,
							@RequestParam(value = "commentNo", required = false) Integer commentNo,
							HttpServletRequest request) {
		
		teamService.deleteReply(commentNo);

	} 

	// 소통창구======================================================================================================================
	@GetMapping("/team/communicate")
	public String communicate(Criteria cri, Model model,
							  HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		int total = teamService.ComuReplyTotalCount(teamVO.getTeamNo(),cri);
		int likeInfo = teamService.getTeamLike(memVO.getMemNo(),memVO.getTeamNo());
		model.addAttribute("teamlikeInfo", likeInfo);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		return "team/communicate";

	}

	// 소통 댓글 리스트
	@ResponseBody
	@PostMapping("/team/communicate1")
	public List<ReplyVO> communicate(Criteria cri, Model model, HttpServletRequest request,
									@RequestParam(value = "teamNo", required = false) Integer teamNo) {

		// memNo session 값 가져오기
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		
		List<ReplyVO> reply = teamService.comuReplyList(teamVO.getTeamNo(),cri.getPageNum());
		int total = teamService.ComuReplyTotalCount(teamVO.getTeamNo(),cri);

		String msg;
		if (reply != null) {

			model.addAttribute("pageMaker", new PageDTO(cri, total));
			return reply;
		} else {
			msg = "fail";
		}
		return null;

	}

	// 페이징
	@ResponseBody
	@GetMapping("/team/communicate3")
	public String communicate3(Criteria cri, HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		// 댓글 5개만 가져오기.
		List<ReplyVO> reply = teamService.comuReplyList(teamVO.getTeamNo(),cri.getPageNum());
		int total = teamService.ComuReplyTotalCount(teamVO.getTeamNo(),cri);

		model.addAttribute("comuList", reply);
		model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "team/communicate";
	}

	// 소통 댓글 달기
	@ResponseBody
	@RequestMapping(value = "/team/ComuaddReply2", method = RequestMethod.POST)
	public int ComuaddReply(@RequestParam(value = "commentContent", required = false) String commentContent,
			HttpServletRequest request) {

		HttpSession session = request.getSession();
		ReplyVO mem = new ReplyVO();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		Long memId = memVO.getMemNo();
		

		int msg;

		if (commentContent != null) {

			return teamService.ComuaddReply(memId, commentContent,teamVO.getTeamNo());

		} else {
			// msg = "fail";
			msg = 0;
			return msg;
		}

	}

	// 소통 댓글 수정
	@ResponseBody
	@RequestMapping(value = "/team/ComuModReply", method = RequestMethod.POST)
	public void ComuModReply(@RequestParam(value = "itemMemNo", required = false) Integer itemMemNo,
							// @RequestParam(value="itemTeamNo" ,required=false) Integer itemTeamNo,
							@RequestParam(value = "commentContent", required = false) String commentContent,
							@RequestParam(value = "commentNo", required = false) Integer commentNo,
							HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		teamService.ComuModReply(commentContent, commentNo, itemMemNo, teamVO.getTeamNo());

	}

	// 소통 댓글 삭제
	@ResponseBody
	@PostMapping("/team/ComudeleteReply")
	public void deleteReply(
			@RequestParam(value = "commentNo") Integer commentNo) {

		teamService.ComuDeleteReply(commentNo);

	}
	
	//팀 리더가 하는 삭제
	@ResponseBody
	@PostMapping("/team/ComudeleteReply2")
	public void deleteReply2(
			@RequestParam(value = "commentNo") Integer commentNo) {

		teamService.ComuDeleteReply(commentNo);

	}
	
	
	
	
	
	
	
	

}
