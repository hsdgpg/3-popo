package com.popo.app.team.service;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class ReplyVO {

//	COMMENT_NO        NOT NULL NUMBER(5)     
//	NOTICE_TYPE       NOT NULL VARCHAR2(20)  
//	NOTICE_NO         NOT NULL NUMBER(5)     
//	MEM_NO            NOT NULL NUMBER(5)     
//	COMMENT_CONTENT   NOT NULL VARCHAR2(500) 
//	COMMENT_HIERARCHY          NUMBER(5)     
//	COMMENT_P_NO               NUMBER(5)     
//	COMMENT_DATE               DATE          
//	COMMENT_SCOPE              NUMBER(5)     
//	COMMENT_R_STATUS           VARCHAR2(20)  
//	COMMENT_STATUS             NUMBER(1)    
	
	int commentNo;					//댓글번호
	String noticeType;				//게시글종류
	int noticeNo;					//글번호
	Long memNo;						//댓글 작성자
	String commentContent;			//댓글 내용
	int commentHierarchy;			//계층
	int commentPno;					//부모댓글번호
	@DateTimeFormat(pattern="yyyy-mm-dd") 
	Date commentDate;				//작성일시 default sysdate
	int commentScope;				//별점
	String commentRStatus;			//신고처리상태
	int commentStatus;				//공개상태 default 0
	
	List<ReplyVO> rereply;
	
	
	String name;
	@DateTimeFormat(pattern="yyyy-mm-dd")
	Date startDate,endDate;
	String title;
	
	int rn;
	String memName;
	String memNickName;
	
}
