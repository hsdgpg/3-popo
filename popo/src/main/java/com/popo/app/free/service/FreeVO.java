package com.popo.app.free.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class FreeVO {
	int freeNo; //글번호
	String freeName; //글제목
	int memNo; //작성자
	String freeContent; //글내용
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date freeTime; //작성시간
	String freeView; //조회수
	String freeStatus; //공개상태
}
