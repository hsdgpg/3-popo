package com.popo.app.notice.service;

import org.springframework.format.annotation.DateTimeFormat;
import java.sql.Date;

import lombok.Data;

@Data
public class NoticeVO {

	int noticeNo;
	int memNo;
	String noticeType;
	String noticeName;
	String noticeContent;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date noticeDate;
	int noticeStatus;
//	NOTICE_NO      NOT NULL NUMBER(5)      
//	NOTICE_TYPE    NOT NULL VARCHAR2(5)    
//	MEM_NO         NOT NULL NUMBER(5)      
//	NOTICE_NAME    NOT NULL VARCHAR2(50)   
//	NOTICE_CONTENT NOT NULL VARCHAR2(1000) 
//	NOTICE_DATE             DATE           
//	NOTICE_STATUS           NUMBER(1)     
//	view_count				number(3)  -> 조회수	

	
	 int viewCount;
	 int teamNo;
	 int count;
	 String memId;
	 
	 int rn;
	 String memName;
	 int commentNo;

	 String name;
	 String memNickName;

	
}
