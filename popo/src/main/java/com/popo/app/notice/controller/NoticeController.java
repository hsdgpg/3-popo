package com.popo.app.notice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.popo.app.match.service.MatchVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeService;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamService;
import com.popo.app.team.service.TeamVO;




@Controller
public class NoticeController {

	@Autowired
	NoticeService noticeService;
	
	@Autowired
	TeamService teamService;
	
	//팀 히스토리 - 공지사항 
	@GetMapping("/team/teamNotice")
	public String teamNotice(Criteria cri,Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamVO teamInfo = (TeamVO) session.getAttribute("teamInfoForHistory");
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		
		if( teamInfo == null  || teamInfo.getTeamNo() == 0) {
			
			List<NoticeVO> list = noticeService.teamNoticePagingList(memVO.getTeamNo(),cri.getPageNum());
			List<NoticeVO> reply = noticeService.CountNoticeReply(memVO.getTeamNo());
			int total = noticeService.teamNoticepageTotalCount(memVO.getTeamNo(),cri);
			int likeInfo = teamService.getTeamLike(memVO.getMemNo(),memVO.getTeamNo());
			model.addAttribute("teamlikeInfo", likeInfo);
			
			model.addAttribute("CountReply", reply);
			model.addAttribute("teamNotice", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			return "team/teamNotice";
		
		}else {
			List<NoticeVO> list = noticeService.teamNoticePagingList(teamInfo.getTeamNo(),cri.getPageNum());
			List<NoticeVO> reply = noticeService.CountNoticeReply(teamInfo.getTeamNo());
			int total = noticeService.teamNoticepageTotalCount(teamInfo.getTeamNo(),cri);
			int likeInfo = teamService.getTeamLike(memVO.getMemNo(),memVO.getTeamNo());
			model.addAttribute("teamlikeInfo", likeInfo);
			
			//model.addAttribute("CurrentMemId", memId);
			model.addAttribute("CountReply", reply);
			model.addAttribute("teamNotice", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			return "team/teamNotice";
		}
		
		
	}
	
	
	//----------------        QnA    ---------------------//
//	@RequestMapping(value="/notice/qnaList",method=RequestMethod.GET)
//	public String qnaList(Model model) {
//		model.addAttribute("qnaList",noticeService.qnaList());
//		return "notice/qnaList";
//	}
	
	@GetMapping("/notice/qnaList") 
	public String qnaList(Criteria cri,Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		List<NoticeVO> list = noticeService.qnaAllPagingList(cri);
		int total = noticeService.qnaTotal(cri);
		
		model.addAttribute("qnaList",list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		System.err.println(list);
		System.err.println(cri);
		
		return "notice/qnaList";
	}
	
	
	@GetMapping("/notice/qnaInsertForm")
	public String qnaInsertForm() {
		
		return "notice/qnaInsertForm";
	}
	
	@PostMapping("/notice/qnaInsert")
	public String qnaInsert(NoticeVO noticeVO,HttpSession session) {
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		int memNo = Math.toIntExact(memVO.getMemNo());
		noticeVO.setMemNo(memNo);
		noticeService.qnaInsert(noticeVO);
		return "redirect:/notice/qnaList";
	}
	
	@RequestMapping(value={ "/notice/qnaSelecter"})
	public String qnaSelecter(@RequestParam(value="noticeNo" ,required=false) Integer noticeNo,Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		noticeService.noticeViewUp(noticeNo);
			
		model.addAttribute("qna",noticeService.noticeSelecter(noticeNo));
		
		return "notice/qnaSelecter";
	}
	
	
	@RequestMapping(value={ "/notice/qnaModForm"})
	public String qnaModForm(@RequestParam(value="noticeNo" ,required=false) Integer noticeNo,
							 @RequestParam(value="noticeName" ,required=false) String noticeName,
			   				 @RequestParam(value="memId" ,required=false) String memId,
			   				 @RequestParam(value="noticeContent" ,required=false) String noticeContent,Model model) {
		
		model.addAttribute("noticeNo", noticeNo);
		model.addAttribute("noticeName", noticeName);
		model.addAttribute("memId", memId);
		model.addAttribute("noticeContent", noticeContent);
		
		return "notice/qnaModForm";
	}
	
	
	@PostMapping("/notice/qnaMod")
	public String qnaMod(NoticeVO noticeVO) {
		noticeService.noticeMod(noticeVO);
		return "redirect:/notice/qnaList";
	}
	
	@RequestMapping(value={ "/notice/qnaDelete"})
	public String qnaDelete(@RequestParam(value="noticeNo", required=false) Integer noticeNo) {
		noticeService.noticeDelete(noticeNo);
		
		return "redirect:/notice/qnaList";
	}
	
	/*              QnA 댓글                       */
	@GetMapping("/notice/qnaReply")
	public String reply(Model model,
			            Criteria cri ,
						@RequestParam(value = "noticeNo", required = false) Integer noticeNo) {
		
		int total = noticeService.qnaReplyTotalCount(cri, noticeNo);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		return "notice/qnaReply";
	}
	
	@ResponseBody
	@PostMapping("/notice/qnaReplyList")
	public List<ReplyVO> qnaReplyList(Criteria cri, Integer noticeNo,
									   Integer pageNum, HttpServletRequest request,Model model) {

		List<ReplyVO> reply = noticeService.qnaReplyIs(noticeNo, pageNum, 5);

		String msg;
		if (reply != null) {
			return reply;
		} else {
			msg = "fail";
		}
		return null;
	}
	 
	// QnA 댓글 달기 
	@ResponseBody
	@RequestMapping(value = "/notice/addQnaReply", method = RequestMethod.POST)
	public int addQnaReply(Integer noticeNo,String commentContent,HttpServletRequest request) {

		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		
		int msg;

		if (commentContent != null || noticeNo != null) {

			return noticeService.addQnaReply(noticeNo, memNo, commentContent);

		} else {
			// msg = "fail";
			msg = 0;
			return msg;
		}
	}
	
	//QnA댓글 수정
	@ResponseBody
	@PostMapping("/notice/modQnaReply")
	public int modQnaReply(Integer noticeNo,Integer itemMemNo,String commentContent,Integer commentNo) {

		int result = noticeService.modQnaReply(commentContent, itemMemNo, noticeNo, commentNo);
		if (result > 0) {
			
			return 1;
		}
		return 0;
	}
	
	//QnA댓글 삭제
  	@ResponseBody
	@PostMapping("/notice/deleteQnaReply")
	public void deleteQnaReply(Integer noticeNo,Integer memNo,Integer commentNo,HttpServletRequest request) {

		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memId = memVO.getMemNo();
		noticeService.deleteQnaReply(noticeNo, memId, commentNo);

	} 	
  	//--------------------------------------------------------------------------------------------------------------------------------------
  	//공지사항
  	
  	//공지사항 리스트
	@GetMapping("/notice/noticeList") 
	public String noticeList(Criteria cri,Model model,HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		List<NoticeVO> list = noticeService.noticeAllPagingList(cri);
		int total = noticeService.noticeTotal(cri);
		
		model.addAttribute("noticeList",list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		System.err.println(list);
		System.err.println(cri);
		
		return "notice/noticeList";
	}
	
	
	@GetMapping("/notice/noticeInsertForm")
	public String noticeInsertForm() {
		
		return "notice/noticeInsertForm";
	}
	
	
	@PostMapping("/notice/noticeInsert")
	public String noticeInsert(NoticeVO noticeVO,HttpSession session) {
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		int memNo = Math.toIntExact(memVO.getMemNo());
		noticeVO.setMemNo(memNo);
		noticeService.noticeInsert(noticeVO);
		return "redirect:/notice/noticeList";
	}
	
	@RequestMapping(value={ "/notice/noticeSelecter"})
	public String noticeSelecter(@RequestParam(value="noticeNo" ,required=false) Integer noticeNo,Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		noticeService.noticeViewUp(noticeNo);
			
		model.addAttribute("notice",noticeService.noticeSelecter(noticeNo));
		
		return "notice/noticeSelecter";
	}
	
	@RequestMapping(value={ "/notice/noticeModForm"})
	public String noticeModForm(NoticeVO noticeVO,Model model) {
		
		model.addAttribute("noticeVO", noticeVO);
		
		return "notice/noticeModForm";
	}
	
	@PostMapping("/notice/noticeMod")
	public String noticeMod(NoticeVO noticeVO) {
		
		noticeService.noticeMod(noticeVO);
		return "redirect:/notice/noticeList";
	}
	
	@RequestMapping(value={ "/notice/noticeDelete"})
	public String noticeDelete(@RequestParam(value="noticeNo", required=false) Integer noticeNo) {
		noticeService.noticeDelete(noticeNo);
		
		return "redirect:/notice/noticeList";
	}
	
}
