package com.popo.app.notice.service;

import java.util.List;

import com.popo.app.show.service.Criteria;
import com.popo.app.team.service.ReplyVO;


public interface NoticeService {

	
	//게시글마다 댓글 개수
	public List<NoticeVO> CountNoticeReply(int teamNo) ;
	
	//페이징
	public List<NoticeVO> teamNoticePagingList(int teamNo,int pageNum);  //전체 페이징
		
	//전체건수
	public int teamNoticepageTotalCount(int teamNo,Criteria cri);
	
	//public List<NoticeVO> getList(Criteria cri);//목록.
	//public int getTotal(Criteria cri);
	
	//qna 리스트
	public List<NoticeVO> qnaList();
	
	//qna insert
	public int qnaInsert(NoticeVO noticeVO);
	
	//notice view up
	public int noticeViewUp(int noticeNo);
	
	//notice 상세조회
	public NoticeVO noticeSelecter(int noticeNo);
	
	//notice 수정
	public int noticeMod(NoticeVO noticeVO);
	
	//notice 삭제
	public int noticeDelete(int noticeNo);
	
	 //QnA페이징
	public List<NoticeVO> qnaAllPagingList(Criteria cri);
	 //QnA토탈	
	public int qnaTotal(Criteria cri);
	
	//QnA 댓글 전체건수
	public int qnaReplyTotalCount(Criteria cri,int noticeNo);
	
	//qna 댓글 리스트
	public List<ReplyVO> qnaReplyIs(int noticeNo,int pageNum,int amount);
	
	//QnA 댓글 달기
	public int addQnaReply(int noticeNo,Long memNo,String commentContent);
	
	//QnA 댓글 수정
	public int modQnaReply(String commentContent,int memNo,int noticeNo,int commentNo);
	
	//QnA 댓글 삭제
	public int deleteQnaReply(int noticeNo,Long memNo,int commentNo);
	
	//공지사항 insert
	public int noticeInsert(NoticeVO noticeVO);
	
	//공지사항 페이징
	public List<NoticeVO> noticeAllPagingList(Criteria cri);
	
	//공지사항 토탈
	public int noticeTotal(Criteria cri);
}
