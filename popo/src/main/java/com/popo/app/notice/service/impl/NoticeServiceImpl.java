package com.popo.app.notice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.notice.mapper.NoticeMapper;
import com.popo.app.notice.service.NoticeService;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.team.service.ReplyVO;


@Service
public class NoticeServiceImpl implements NoticeService{

	
	@Autowired
	NoticeMapper noticeMapper;

	@Override
	public List<NoticeVO> teamNoticePagingList(int teamNo,int pageNum) {
		// TODO Auto-generated method stub
		return noticeMapper.teamNoticePagingList(teamNo,pageNum);
	}

	@Override
	public int teamNoticepageTotalCount(int teamNo,Criteria cri) {
		return noticeMapper.teamNoticepageTotalCount(teamNo,cri);
	}


	@Override
	public List<NoticeVO> CountNoticeReply(int teamNo) {
		return noticeMapper.CountNoticeReply(teamNo);
	}

	@Override
	public List<NoticeVO> qnaList() {
		// TODO Auto-generated method stub
		return noticeMapper.qnaList();
	}

	@Override
	public int qnaInsert(NoticeVO noticeVO) {
		// TODO Auto-generated method stub
		return noticeMapper.qnaInsert(noticeVO);
	}

	@Override
	public int noticeViewUp(int noticeNo) {
		// TODO Auto-generated method stub
		return noticeMapper.noticeViewUp(noticeNo);
	}

	@Override
	public NoticeVO noticeSelecter(int noticeNo) {
		// TODO Auto-generated method stub
		return noticeMapper.noticeSelecter(noticeNo);
	}

	@Override
	public int noticeMod(NoticeVO noticeVO) {
		// TODO Auto-generated method stub
		return noticeMapper.noticeMod(noticeVO);
	}

	@Override
	public int noticeDelete(int noticeNo) {
		// TODO Auto-generated method stub
		return noticeMapper.noticeDelete(noticeNo);
	}

	@Override
	public List<NoticeVO> qnaAllPagingList(Criteria cri) {
		// TODO Auto-generated method stub
		return noticeMapper.qnaAllPagingList(cri);
	}

	@Override
	public int qnaTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return noticeMapper.qnaTotal(cri);
	}

	@Override
	public int qnaReplyTotalCount(Criteria cri, int noticeNo) {
		// TODO Auto-generated method stub
		return noticeMapper.qnaReplyTotalCount(cri, noticeNo);
	}

	@Override
	public List<ReplyVO> qnaReplyIs(int noticeNo, int pageNum, int amount) {
		// TODO Auto-generated method stub
		return noticeMapper.qnaReplyIs(noticeNo, pageNum, amount);
	}

	@Override
	public int addQnaReply(int noticeNo, Long memNo, String commentContent) {
		// TODO Auto-generated method stub
		return noticeMapper.addQnaReply(noticeNo, memNo, commentContent);
	}

	@Override
	public int modQnaReply(String commentContent, int memNo, int noticeNo, int commentNo) {
		// TODO Auto-generated method stub
		return noticeMapper.modQnaReply(commentContent, memNo, noticeNo, commentNo);
	}

	@Override
	public int deleteQnaReply(int noticeNo, Long memNo, int commentNo) {
		// TODO Auto-generated method stub
		return noticeMapper.deleteQnaReply(noticeNo, memNo, commentNo);
	}

	@Override
	public int noticeInsert(NoticeVO noticeVO) {
		// TODO Auto-generated method stub
		return noticeMapper.noticeInsert(noticeVO);
	}

	@Override
	public List<NoticeVO> noticeAllPagingList(Criteria cri) {
		// TODO Auto-generated method stub
		return noticeMapper.noticeAllPagingList(cri);
	}

	@Override
	public int noticeTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return noticeMapper.noticeTotal(cri);
	}

	

}
