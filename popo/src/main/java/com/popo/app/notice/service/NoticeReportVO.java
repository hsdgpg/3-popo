package com.popo.app.notice.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class NoticeReportVO {

int rn;
String postReportReason;
String memName;
@DateTimeFormat(pattern="yyyy-MM-dd")
Date postReportDate;
int noticeNo;
int memNo;

String postReportCategory;
	
}
