package com.popo.app.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.popo.app.admin.mapper.AdminMapper;
import com.popo.app.admin.service.AdminService;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.message.service.MessageVO;
import com.popo.app.notice.service.NoticeReportVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.mapper.ShowMapper;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowService;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceVO;
import com.popo.app.team.service.ReplyVO;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminMapper adminMapper;
	
	@Override
	public List<MemberVO> userList(Criteria cri) {
		return adminMapper.userList(cri);
	}

	@Override
	public List<MemberVO> spaceRentalList(Criteria cri) {
		return adminMapper.spaceRentalList(cri);
	}


	@Override
	public List<SpaceVO> spaceAddList(Criteria cri) {
		return adminMapper.spaceAddList(cri);
	}

	@Override
	public List<SpaceVO> spaceSuccessList(Criteria cri) {
		return adminMapper.spaceSuccessList(cri);
		
	}
	
	@Override
	public void spacePermission(SpaceVO spaceVO) {
		adminMapper.spacePermission(spaceVO);
		adminMapper.spacePermissionMsg(spaceVO);
		
	}

	@Override
	public void spaceRefuse(SpaceVO spaceVO) {
		adminMapper.spaceRefuse(spaceVO);
		adminMapper.spaceRefuseMsg(spaceVO);
		
		
	}

	@Override
	public List<FundingVO> fundingAddList(Criteria cri) {
		return adminMapper.fundingAddList(cri);
	}

	@Override
	public List<FundingVO> fundingProgressList(Criteria cri) {
		return adminMapper.fundingProgressList(cri);
	}

	@Override
	public List<FundingVO> fundingEndList(Criteria cri) {
		return adminMapper.fundingEndList(cri);
	}
	
	@Override
	public void fundingPermission(FundingVO fundingVO) {
		adminMapper.fundingPermission(fundingVO);
		adminMapper.fundingPermissionMsg(fundingVO);
	}

	@Override
	public void fundingRefuse(FundingVO fundingVO) {
		adminMapper.fundingRefuse(fundingVO);
		adminMapper.fundingRefuseMsg(fundingVO);
	}

	@Override
	public List<NoticeVO> noticeList(Criteria cri) {
		return adminMapper.noticeList(cri);
	}
	
	@Override
	public void deleteNotice(NoticeVO noticeVO) {
		adminMapper.deleteNotice(noticeVO);
	}

	@Override
	public List<NoticeVO> QnAList(Criteria cri) {
		return adminMapper.QnAList(cri);
	}

	@Override
	public List<NoticeReportVO> reportNoticeList(Criteria cri) {
		return adminMapper.reportNoticeList(cri);
	}
	
	// 게시글 신고
	@Override
	public void deletePostReport(NoticeVO noticeVO) {
		// 게시글 비활성화
		adminMapper.deleteNotice(noticeVO);
		// 신고내역에서 삭제
		adminMapper.deletePostReport(noticeVO);
		// 메세지 전송
		adminMapper.deletePostReportMsg(noticeVO);
		// 글 작성자의 신고 카운트 UP
		adminMapper.postReportCountUp(noticeVO);
		// 신고 카운트 3회이상시 맴버 비활성화
		adminMapper.memberDisable(noticeVO);
	}
	
	@Override
	public void cancelPostReport(NoticeVO noticeVO) {
		adminMapper.cancelPostReport(noticeVO);
		adminMapper.cancelPostReportMsg(noticeVO);
	}

	@Override
	public List<ReplyReportVO> reportReplyList(Criteria cri) {
		return adminMapper.reportReplyList(cri);
	}
	
	@Override
	public void deleteReplyReport(NoticeVO noticeVO) {
		adminMapper.deleteReply(noticeVO);
		adminMapper.deleteReplyReport(noticeVO);
		adminMapper.deleteReplyReportMsg(noticeVO);
		adminMapper.postReportCountUp(noticeVO);
		adminMapper.memberDisable(noticeVO);
	}

	@Override
	public void cancelReplyReport(NoticeVO noticeVO) {
		adminMapper.cancelReplyReport(noticeVO);
		adminMapper.cancelReplyReportMsg(noticeVO);
	}
	

	@Override
	public int userCount(Criteria cri) {
		return adminMapper.userCount(cri);
	}

	@Override
	public int spaceRentalCount(Criteria cri) {
		return adminMapper.spaceRentalCount(cri);
	}

	@Override
	public int spaceAddCount(Criteria cri) {
		return adminMapper.spaceAddCount(cri);
	}

	@Override
	public int spaceSuccessCount(Criteria cri) {
		return adminMapper.spaceSuccessCount(cri);
	}

	@Override
	public int fundingAddCount(Criteria cri) {
		return adminMapper.fundingAddCount(cri);
	}

	@Override
	public int fundingProgressCount(Criteria cri) {
		return adminMapper.fundingProgressCount(cri);
	}

	@Override
	public int fundingEndCount(Criteria cri) {
		return adminMapper.fundingEndCount(cri);
	}

	@Override
	public int noticeCount(Criteria cri) {
		return adminMapper.noticeCount(cri);
	}

	@Override
	public int QnACount(Criteria cri) {
		return adminMapper.QnACount(cri);
	}

	@Override
	public int reportNoticeCount(Criteria cri) {
		return adminMapper.reportNoticeCount(cri);
	}

	@Override
	public int reportReplyCount(Criteria cri) {
		return adminMapper.reportReplyCount(cri);
	}

	@Override
	public PaymentVO fundingRevenue() {
		return adminMapper.fundingRevenue();
	}

	@Override
	public PaymentVO ticketingRevenue() {
		return adminMapper.ticketingRevenue();
	}

	@Override
	public PaymentVO spaceRevenue() {
		return adminMapper.spaceRevenue();
	}

	@Override
	public List<ShowVO> ticketingRanking() {
		return adminMapper.ticketingRanking();
	}

	@Override
	public List<FundingVO> fundingRanking() {
		return adminMapper.fundingRanking();
	}

	@Override
	public int ticketingTotal() {
		return adminMapper.ticketingTotal();
	}

}
