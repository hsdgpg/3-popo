package com.popo.app.admin.controller;

// 김준혁 관리자 페이지

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.popo.app.admin.service.AdminService;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeReportVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceVO;

@Controller
public class AdminController {

	@Autowired
	AdminService adminService;

	// 유저 목록
	@GetMapping("/admin/userList")
	public String userList(Criteria cri, Model model) {
			
		List<MemberVO> list = adminService.userList(cri);
		int total = adminService.userCount(cri);

		model.addAttribute("userList", list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
			

		return "admin/userList";
	}
	
	// 공간대여자 목록
		@GetMapping("/admin/spaceRentalList")
		public String spaceRentalList(Criteria cri, Model model) {
				
			List<MemberVO> list = adminService.spaceRentalList(cri);
			int total = adminService.spaceRentalCount(cri);

			model.addAttribute("spaceRentalList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/spaceRentalList";
		}
		
	// 공간등록신청 관리목록
		@GetMapping("/admin/spaceAddList")
		public String spaceAddList(Criteria cri, Model model) {
				
			List<SpaceVO> list = adminService.spaceAddList(cri);
			int total = adminService.spaceAddCount(cri);

			model.addAttribute("spaceAddList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/spaceAddList";
		}
		
		// 공간등록 허가
		@PostMapping("/admin/spacePermission")
		public String spacePermission(SpaceVO spaceVO) {

			adminService.spacePermission(spaceVO);

			return "redirect:/admin/spaceAddList";
		}
		
		// 공간등록 거절
		@PostMapping("/admin/spaceRefuse")
		public String spaceRefuse(SpaceVO spaceVO) {

			adminService.spaceRefuse(spaceVO);

			return "redirect:/admin/spaceAddList";
		}
		
		
		// 등록 완료공간 목록
		@GetMapping("/admin/spaceSuccessList")
		public String spaceSuccessList(Criteria cri, Model model) {
				
			List<SpaceVO> list = adminService.spaceSuccessList(cri);
			int total = adminService.spaceSuccessCount(cri);

			model.addAttribute("spaceSuccessList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/spaceSuccessList";
		}
		
		
		// 펀딩신청 관리목록
		@GetMapping("/admin/fundingAddList")
		public String fundingAddList(Criteria cri, Model model) {
				
			List<FundingVO> list = adminService.fundingAddList(cri);
			int total = adminService.fundingAddCount(cri);

			model.addAttribute("fundingAddList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/fundingAddList";
		}
		
		
		// 펀딩등록 허가
		@PostMapping("/admin/fundingPermission")
		public String fundingPermission(FundingVO fundingVO) {

			adminService.fundingPermission(fundingVO);

			return "redirect:/admin/fundingAddList";
		}
		
		// 펀딩등록 거절
		@PostMapping("/admin/fundingRefuse")
		public String fundingRefuse(FundingVO fundingVO) {

			adminService.fundingRefuse(fundingVO);

			return "redirect:/admin/fundingAddList";
		}
		
		
		
		// 진행중인 펀딩 목록
		@GetMapping("/admin/fundingProgressList")
		public String fundingProgressList(Criteria cri, Model model) {
				
			List<FundingVO> list = adminService.fundingProgressList(cri);
			int total = adminService.fundingProgressCount(cri);

			model.addAttribute("fundingProgressList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/fundingProgressList";
		}
		
		// 종료된 펀딩목록
		@GetMapping("/admin/fundingEndList")
		public String fundingEndList(Criteria cri, Model model) {
				
			List<FundingVO> list = adminService.fundingEndList(cri);
			int total = adminService.fundingEndCount(cri);

			model.addAttribute("fundingEndList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/fundingEndList";
		}
		
		// 공지사항 목록(관리)
		@GetMapping("/admin/noticeList")
		public String noticeList(Criteria cri, Model model) {
				
			List<NoticeVO> list = adminService.noticeList(cri);
			int total = adminService.noticeCount(cri);

			model.addAttribute("noticeList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/noticeList";
		}
		
		// 공지사항 삭제( status 2로 변경)
		@PostMapping("/admin/deleteNotice")
		public String deleteNotice(NoticeVO noticeVO) {

			adminService.deleteNotice(noticeVO);

			return "redirect:/admin/noticeList";
		}
		
		// QnA 목록(관리)
		@GetMapping("/admin/QnAList")
		public String QnAList(Criteria cri, Model model) {
				
			List<NoticeVO> list = adminService.QnAList(cri);
			int total = adminService.QnACount(cri);

			model.addAttribute("QnAList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			
			System.out.println(list);
				

			return "admin/QnAList";
		}
		
		// 게시글 신고 목록
		@GetMapping("/admin/reportNoticeList")
		public String reportNoticeList(Criteria cri, Model model) {
				
			List<NoticeReportVO> list = adminService.reportNoticeList(cri);
			int total = adminService.reportNoticeCount(cri);

			model.addAttribute("reportNoticeList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/reportNoticeList";
		}
		
		// 신고된 게시글 삭제, 신고 카운트 증가, 메세지 전송, 누적신고 3회이상시 회원 비활성화
		@PostMapping("/admin/postReport")
		public String postReport(NoticeVO noticeVO) {

			adminService.deletePostReport(noticeVO);

			return "redirect:/admin/reportNoticeList";
		}
		
		// 게시글에 신고된건 취소
		@PostMapping("/admin/postReportCancel")
		public String postReportCancel(NoticeVO noticeVO) {
			
			adminService.cancelPostReport(noticeVO);


			return "redirect:/admin/reportNoticeList";
		}
			
		// 댓글 신고 목록
		@GetMapping("/admin/reportReplyList")
		public String reportReplyList(Criteria cri, Model model) {
				
			List<ReplyReportVO> list = adminService.reportReplyList(cri);
			int total = adminService.reportReplyCount(cri);

			model.addAttribute("reportReplyList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
				

			return "admin/reportReplyList";
		}
			
		
		// 신고된 댓글 삭제, 신고 카운트 증가, 메세지 전송, 누적신고 3회이상시 회원 비활성화
		@PostMapping("/admin/replyReport")
		public String replyReport(NoticeVO noticeVO) {

			adminService.deleteReplyReport(noticeVO);

			return "redirect:/admin/reportReplyList";
		}
		
		// 댓글에 신고된건 취소
		@PostMapping("/admin/replyReportCancel")
		public String replyReportCancel(NoticeVO noticeVO) {

			adminService.cancelReplyReport(noticeVO);

			return "redirect:/admin/reportReplyList";
		}
		
		
		// 수익 대쉬보드
		@GetMapping("/admin/revenue")
		public String revenue(Model model) {
				
			PaymentVO funding = adminService.fundingRevenue();
			PaymentVO ticketing = adminService.ticketingRevenue();
			PaymentVO space =  adminService.spaceRevenue();
			
			model.addAttribute("funding", funding);
			model.addAttribute("ticketing", ticketing);
			model.addAttribute("space", space);
			
			return "admin/revenue";
		}
		
		// 메인페이지 배너 관리
		@GetMapping("/admin/banner")
		public String banner(Model model) {
				
			List<ShowVO> ticketingRK = adminService.ticketingRanking();
			List<FundingVO> fundingRanking = adminService.fundingRanking();
			int ticketingTotal = adminService.ticketingTotal();
			
			model.addAttribute("ticketingRK", ticketingRK);
			model.addAttribute("ticketingTotal", ticketingTotal);
			model.addAttribute("fundingRanking", fundingRanking);
			
			return "admin/banner";
		}
		
		
}
