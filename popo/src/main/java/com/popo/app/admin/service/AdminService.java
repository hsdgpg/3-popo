package com.popo.app.admin.service;

import java.util.List;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.message.service.MessageVO;
import com.popo.app.notice.service.NoticeReportVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceVO;
import com.popo.app.team.service.ReplyVO;


public interface AdminService {

	// 유저목록
	public List<MemberVO> userList(Criteria cri); 
	// 공간대여자 목록
	public List<MemberVO> spaceRentalList(Criteria cri); 
	
	// 공간등록신청 관리목록
	public List<SpaceVO> spaceAddList(Criteria cri); 
	// 등록 완료공간 목록
	public List<SpaceVO> spaceSuccessList(Criteria cri); 
	// 공간등록 허가
	public void spacePermission(SpaceVO spaceVO);
	// 공간등록 거절
	public void spaceRefuse(SpaceVO spaceVO);
	
	// 펀딩신청 관리목록
	public List<FundingVO> fundingAddList(Criteria cri); 
	// 진행중인 펀딩 목록
	public List<FundingVO> fundingProgressList(Criteria cri); 
	// 종료된 펀딩목록
	public List<FundingVO> fundingEndList(Criteria cri); 
	// 펀딩등록 허가
	public void fundingPermission(FundingVO fundingVO);
	// 펀딩등록 거절
	public void fundingRefuse(FundingVO fundingVO);
	
	// 공지사항 목록(관리)
	public List<NoticeVO> noticeList(Criteria cri);
	// 공지사항 삭제
	public void deleteNotice(NoticeVO noticeVO);
	// QnA 목록(관리)
	public List<NoticeVO> QnAList(Criteria cri); 
	
	// 게시글 신고 목록(관리)
	public List<NoticeReportVO> reportNoticeList(Criteria cri);
	// 게시글 신고 삭제
	public void deletePostReport(NoticeVO noticeVO);
	// 게시글 신고 취소
	public void cancelPostReport(NoticeVO noticeVO);
	
	
	// 댓글 신고 목록(관리)
	public List<ReplyReportVO> reportReplyList(Criteria cri); 
	// 게시글 신고 삭제
	public void deleteReplyReport(NoticeVO noticeVO);
	// 게시글 신고 취소
	public void cancelReplyReport(NoticeVO noticeVO);
		
		
	// 유저 건수
	public int userCount(Criteria cri);
	// 공간대여자 건수
	public int spaceRentalCount(Criteria cri);
	// 공간등록신청 건수
	public int spaceAddCount(Criteria cri);
	// 등록 완료공간 건수
	public int spaceSuccessCount(Criteria cri);
	// 펀딩신청 건수
	public int fundingAddCount(Criteria cri);
	// 진행중인 펀딩 건수
	public int fundingProgressCount(Criteria cri);
	// 종료된 펀딩 건수
	public int fundingEndCount(Criteria cri);
	// 공지사항 건수
	public int noticeCount(Criteria cri);
	// QnA 건수
	public int QnACount(Criteria cri);
	// 게시글신고 건수
	public int reportNoticeCount(Criteria cri);
	// 댓글신고 건수
	public int reportReplyCount(Criteria cri);
	
	// 펀딩 수익
	public PaymentVO fundingRevenue();
	// 예매 수익
	public PaymentVO ticketingRevenue();
	// 공간대여 수익
	public PaymentVO spaceRevenue();
	
	// 메인 배너 관리(예매)
	public List<ShowVO> ticketingRanking();
	// 메인 배너 관리(펀딩)
	public List<FundingVO> fundingRanking();
	// 총 예매 건수
	public int ticketingTotal();
	
}
