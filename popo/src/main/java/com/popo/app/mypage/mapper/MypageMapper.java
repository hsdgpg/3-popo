package com.popo.app.mypage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.popo.app.free.service.FreeVO;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.message.service.MessageVO;
import com.popo.app.mypage.service.JjimVO;
import com.popo.app.mypage.service.MyPaymentVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamVO;

@Mapper
public interface MypageMapper {
	
		//마이페이지 조회
		public MemberVO mypage(Long memNo);	
		//마이페이지 수정
		public int mypageMod(MemberVO memberVO);
		//회원탈퇴
		public String deleteMember(String memId);
		//비밀번호 변경
		public ModelAndView newPw(String memId, String memPw, Long memNo, String newPw);
		public int newPw(String memPw, Long memNo);
		public MemberVO findByUserId(String memId);
		
		//찜 목록-펀딩
		public List<JjimVO> fundingJjim(Long memNo); 
		//public int getfundingJjim(Criteria cri, Long memNo);
		
		//찜 목록 공연
		public List<JjimVO> showJjim(Long memNo,int pageNum, int amount);
		public int getShowJjim(Criteria cri, Long memNo); 	
		
		// 예매 결제목록 조회
		public List<PaymentVO> paymentList(Long memNo, int pageNum, int amount);
		// 예매 결제 건수
		public int getPaymentCount(Long memNo,Criteria cri);
			
		// 예매 결제취소시 선택된 좌석 삭제
		public int deleteSeat(Long memNo, int showNo, int reservNo);
		// 예매 결제취소시 예매 삭제
		public int deleteReserv(Long memNo, int showNo, int reservNo);
		// 예매 결제내역 취소 상태로 변경
		public int updatePayment(Long memNo, int showNo, int amountPrice, int teamNum, int reservNo, int teamNo);
		// 예매 팀 정산 된것 빼기
		public int updateTeamPayment(int teamNo, int amountPrice, int teamNum);
		// 예매 관리자 정산 된것 빼기
		public int updateAdminPayment(int amountPrice);
		
		// 펀딩 결제목록 조회
		public List<PaymentVO> fundingPaymentList(Long memNo, int pageNum, int amount);
		// 펀딩 결제 건수
		public int getFundingPaymentCount(Long memNo,Criteria cri);
		
		// 결제취소시 펀딩 누적금액 -
		public int minusFundingPrice(int fundingNo, int amountPrice);
		// 펀딩 결제내역 취소 상태로 변경
		public int updateFundingPayment(Long memNo, int fundingNo, int amountPrice, int teamNum, int teamNo, int payNo);
		// 펀딩 팀 정산 된것 빼기
		public int updateFundingTeamPayment(int teamNo, int amountPrice, int teamNum);
		// 펀딩 관리자 정산 된것 빼기
		public int updateFundingAdminPayment(int amountPrice);
		// 공간 결제목록 조회
		public List<PaymentVO> spacePaymentList(Long memNo, int pageNum, int amount);
		// 공간 결제 건수
		public int getSpacePaymentCount(Long memNo,Criteria cri);
		
		// 결제취소시 공간대여 목록에서 삭제
		public int deleteSpace(Long memNo, int placeNo);
		// 공간 결제내역 취소 상태로 변경
		public int updateSpacePayment(Long memNo, int placeNo, int rentalNo, int amountPrice);
		// 공간대여자 정산 된것 빼기
		public int updateSpaceRentalPay(int placeNo, int amountPrice);
		// 관리자 정산 된것 빼기
		public int updateSpaceAdminPay(int amountPrice);
		// 공간을 빌리는 사람의 정보 업데이트
		public int updateSpaceBorrower(int placeNo);
		
		//환불
		public List<MyPaymentVO> refund(Long memNo, int pageNum, int amount);
		public int getRefund(Criteria cri,  Long memNo); 
		
		public List<MyPaymentVO> refundFunding(Long memNo, int pageNum, int amount);
		public int getRefundFunding(Criteria cri,  Long memNo);
		
		public List<MyPaymentVO> refundShow(Long memNo, int pageNum, int amount);
		public int getRefundShow(Criteria cri,  Long memNo);
		
		public List<MyPaymentVO> refundPlace(Long memNo, int pageNum, int amount);
		public int getRefundPlace(Criteria cri,  Long memNo);
		
		//메시지 
		public List<MessageVO> messageList(Long memNo,int pageNum, int amount);
		public int getMessageTotal(Criteria cri,  Long memNo); 
		public MessageVO messageDetail(int msgNo);
		
		//작성한 글=QnA 
		public List<NoticeVO> noticeListPaging(Long memNo,int pageNum, int amount); 
		public int getNoticeTotal(Criteria cri,  Long memNo); 
		
		//작성한 글=펀딩
		public int getFundingTotal(Criteria cri,  Long memNo);
		public List<FundingVO> funding(Long memNo,int pageNum, int amount);
		
		//작성한 글=공연
		public List<ShowVO> show(Long memNo,int pageNum, int amount); 
		public int getShowTotal(Criteria cri,  Long memNo);
		
		//작성한 댓글
		public List<ReplyVO> myReply(Long memNo,int pageNum, int amount); 
		public int getMyReplyTotal(Criteria cri, Long memNo);
		
		//작성한 리뷰
		public List<ReplyVO> myReview(Long memNo,int pageNum, int amount); 
		public int getMyReviewTotal(Criteria cri, Long memNo);
		
		//좋아하는 아티스트
		public List<TeamVO> favorit(Long memNo,int pageNum, int amount);
		public int getfavoritTotal(Criteria cri, Long memNo);	
	
		//메세지 상세페이지
		public MessageVO getMsgInfo(int msgNo);
		public int deleteMsg(int msgNo);
	


}
