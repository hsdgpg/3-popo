
package com.popo.app.mypage.service;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.popo.app.free.service.FreeVO;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.message.service.MessageVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamVO;

public interface MypageService {
	
	//마이페이지 조회
	public MemberVO mypage(Long memNo);	
	//마이페이지 수정
	public int mypageMod(MemberVO memberVO);
	//회원탈퇴
	public String deleteMember(String memId);
	//비밀번호 변경
	public ModelAndView newPw(String memId, String memPw, Long memNo, String newPw);
	
	//찜 목록-펀딩
	public List<JjimVO> fundingJjim(Long memNo); 
	//public int getfundingJjim(Criteria cri,  Long memNo);
	
	//찜 목록 공연
	public List<JjimVO> showJjim(Long memNo,int pageNum, int amount);
	public int getShowJjim(Criteria cri,  Long memNo); 
	
	// 결제목록 조회
	public List<PaymentVO> paymentList(Long memNo, int pageNum, int amount);
	// 결제 건수
	public int getPaymentCount(Long memNo,Criteria cri);	
	// 결제내역 취소 상태로 변경
	public int updatePayment(Long memNo, int showNo, int amountPrice, int teamNum, int reservNo, int teamNo);
	
	// 펀딩 결제목록 조회
	public List<PaymentVO> fundingPaymentList(Long memNo, int pageNum, int amount);
	// 펀딩 결제 건수
	public int getFundingPaymentCount(Long memNo,Criteria cri);	
	// 펀딩 결제내역 취소 상태로 변경
	public int updateFundingPayment(Long memNo, int fundingNo, int amountPrice, int teamNum, int teamNo, int payNo);
	
	// 공간 결제목록 조회
	public List<PaymentVO> spacePaymentList(Long memNo, int pageNum, int amount);
	// 공간 결제 건수
	public int getSpacePaymentCount(Long memNo,Criteria cri);	
	// 공간 결제내역 취소 상태로 변경
	public int updateSpacePayment(Long memNo, int placeNo, int rentalNo, int amountPrice);
	
	//환불
	public List<MyPaymentVO> refund(Long memNo, int pageNum, int amount);
	public int getRefund(Criteria cri,  Long memNo); 
	
	//메시지 
	public List<MessageVO> messageList(Long memNo,int pageNum, int amount);
	public int getMessageTotal(Criteria cri,  Long memNo); 
	public MessageVO messageDetail(int msgNo);
	
	//작성한 글=QnA 
	public List<NoticeVO> noticeListPaging(Long memNo,int pageNum, int amount); 
	public int getNoticeTotal(Criteria cri,  Long memNo); 
	
	//작성한 글=펀딩
	public int getFundingTotal(Criteria cri,  Long memNo);
	public List<FundingVO> funding(Long memNo,int pageNum, int amount);
	
	//작성한 글=공연
	public List<ShowVO> show(Long memNo,int pageNum, int amount); 
	public int getShowTotal(Criteria cri,  Long memNo);
	
	//작성한 댓글
	public List<ReplyVO> myReply(Long memNo,int pageNum, int amount); 
	public int getMyReplyTotal(Criteria cri, Long memNo);
	
	//작성한 리뷰
	public List<ReplyVO> myReview(Long memNo,int pageNum, int amount); 
	public int getMyReviewTotal(Criteria cri, Long memNo);
	
	//좋아하는 아티스트
	public List<TeamVO> favorit(Long memNo,int pageNum, int amount);
	public int getfavoritTotal(Criteria cri, Long memNo);
	

	//메세지 상세페이지
	public MessageVO getMsgInfo(int msgNo);
	public int deleteMsg(int msgNo);	
	
}

