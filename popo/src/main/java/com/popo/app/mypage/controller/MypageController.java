package com.popo.app.mypage.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.popo.app.free.service.FreeVO;
import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.message.service.MessageVO;
import com.popo.app.mypage.service.JjimVO;
import com.popo.app.mypage.service.MyPaymentVO;
import com.popo.app.mypage.service.MypageService;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamVO;

@Controller
public class MypageController {

	@Autowired
	MypageService mypageService;

	// 내정보조회
	@GetMapping("/mypage/mypage")
	public String mypage(Model model, HttpSession session) {

		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		MemberVO list= mypageService.mypage(memNo);

		System.out.println(list);
		model.addAttribute("myInfo", list);

		return "mypage/mypage";
	}
	
	// 내정보 수정 화면
	@GetMapping("/mypage/mypageModForm")
	public String mypageModForm(HttpSession session, Model model) {

		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		
		
		return "mypage/mypageModForm";
	}

	// 내정보 수정
	@PostMapping("/mypage/mypageMod")
	public RedirectView mypageMod(MemberVO memberVO, 
							BindingResult bindingResult,
							@RequestParam MultipartFile memProfilePic) throws IOException{
		// 첨부파일
		if (bindingResult.hasErrors()) {
			List<ObjectError> errors = bindingResult.getAllErrors();
			for (ObjectError error : errors) {
				if (memProfilePic == null || memProfilePic.isEmpty()) {
					String nullImage = "../img/myImg.png";
					memberVO.setMemProfilePic(nullImage);
					mypageService.mypageMod(memberVO);
					return new RedirectView("mypage");
				}
				String uploadDir = "test/";
				// 파일이름
				String fileName = memProfilePic.getOriginalFilename();
				// 저장할 파일 경로
				Path ImgPath = Paths.get(uploadDir + fileName);
				// 파일 저장
				Files.write(ImgPath, memProfilePic.getBytes());
				// DB에 저장할 파일 경로
				String filePath = "test/" + fileName;

				memberVO.setMemProfilePic(filePath);
				mypageService.mypageMod(memberVO);
			}
			// 오류 처리
		} else {
			// 유효성 검사 통과
		}
		return new RedirectView("mypage");
	}
	
	
	//비밀번호 변경-화면
	@GetMapping("/mypage/password")
	public String password(HttpSession session, Model model) {
		
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		
		
		return "mypage/password";
	}
	
	//비밀번호변경
	 @PostMapping("/mypage/changePassword")
	  public String newPw(
			  					   @RequestParam(value="memId") String memId,
	                               @RequestParam(value="memPw") String memPw,
	                               @RequestParam(value="memNo") Long memNo,
	                               @RequestParam(value="newPw") String newPw)
	                             {
		 
		 System.out.println(memId);
		 System.out.println(memPw);
		 System.out.println(memNo);
		 System.out.println(newPw);
		 
		 mypageService.newPw(memId, memPw, memNo, newPw);
	      
	      return "redirect:/mypage/mypage";
	  }
	
	 
	//회원탈퇴
	 @RequestMapping("/mypage/deleteMember")
	 public String deleteMember(@RequestParam("memId") String memId, 
	                            RedirectAttributes redirectAttr, 
	                            SessionStatus sessionStatus) {
		 
	     String result = mypageService.deleteMember(memId);
	     
	     if (result != null) {
	         redirectAttr.addFlashAttribute("msg", "성공적으로 회원정보를 삭제했습니다.");
	         SecurityContextHolder.clearContext();
	     } else {
	         redirectAttr.addFlashAttribute("msg", "회원정보삭제에 실패했습니다.");
	     }

	     sessionStatus.setComplete();

	     return "redirect:mypage/mypage";
	 }
	 
	 
	// 찜목록-공연
	@GetMapping("/mypage/showJjim")
	public String showJjim(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
			@RequestParam(value = "amount", required = false, defaultValue = "10") int amount, HttpSession session,
			Model model, Criteria cri) {

		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		List<JjimVO> list = mypageService.showJjim(memNo, cri.getPageNum(), cri.getAmount());
		//int total = mypageService.getShowJjim(cri, memNo);

		model.addAttribute("jjim", list);
		//model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "mypage/showJjim";
	}

	// 찜목록-펀딩
	@GetMapping("/mypage/fundingJjim")
	public String fundingJjim(HttpSession session,
							  Model model) {

		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		List<JjimVO> list = mypageService.fundingJjim(memNo);
		//int total = mypageService.getfundingJjim(cri, memNo);

		System.out.println(list);
		model.addAttribute("funding", list);
		//model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "mypage/fundingJjim";
	}
	
	
		// 예매 결제목록조회
	   @GetMapping("/mypage/payment") 
	   public String payment( HttpSession session, Model model, Criteria cri) {
	
	      MemberVO memVO = (MemberVO) session.getAttribute("memVO");
	      Long memNo = memVO.getMemNo();
	         
	      List<PaymentVO> list = mypageService.paymentList(memNo, cri.getPageNum(), cri.getAmount());
	
	      int total = mypageService.getPaymentCount(memNo,cri);
	
	      model.addAttribute("paymentList", list);
	      model.addAttribute("pageMaker", new PageDTO(cri, total));
	      model.addAttribute("status", total);
	      
	      return "mypage/payment";
	   }
	   
	   // 예매 결제 취소시 결제상태 변경, 선택된 좌석 삭제, 예매목록 삭제, 팀 정산 빼기, 관리자 정산 빼기
      @PostMapping("/mypage/paymentDelete")
      @ResponseBody
      public String paymentDelete(@RequestParam(value = "memNo") Long memNo,
                             @RequestParam(value= "showNo") int showNo,
                             @RequestParam(value = "amountPrice") int amountPrice,
                             @RequestParam(value= "teamNum") int teamNum,
                             @RequestParam(value= "reservNo") int reservNo,
                             @RequestParam(value= "teamNo") int teamNo) {
         
         String msg = "success";
         
          mypageService.updatePayment(memNo, showNo, amountPrice, teamNum, reservNo, teamNo );
             
          return msg;
              
      }

		// 펀딩 결제목록조회
		@GetMapping("/mypage/fundingPayment") 
		public String fundingPayment( HttpSession session, Model model, Criteria cri) {
	
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();
			
			List<PaymentVO> list = mypageService.fundingPaymentList(memNo, cri.getPageNum(), cri.getAmount());
	
			int total = mypageService.getFundingPaymentCount(memNo,cri);
	
			model.addAttribute("fundingList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			model.addAttribute("status", total);
			
	
			return "mypage/fundingPayment";
		}
	
		 //펀딩 결제 취소시 결제상태 변경, 펀딩 누적금액 빼기, 팀 정산 빼기, 관리자 정산 빼기
		@PostMapping("/mypage/fundingPaymentDelete")
		@ResponseBody
		public String fundingDelete(@RequestParam(value = "memNo") Long memNo,
							 	    @RequestParam(value= "fundingNo") int fundingNo,
							 	    @RequestParam(value = "amountPrice") int amountPrice,
							 	    @RequestParam(value= "teamNum") int teamNum,
							 	    @RequestParam(value= "teamNo") int teamNo,
							 	    @RequestParam(value= "payNo") int payNo) {
			
			String msg = "success";
			
		    mypageService.updateFundingPayment(memNo, fundingNo, amountPrice, teamNum, teamNo, payNo );
		    	
		    return msg;
		        
		}
		
		// 공간대여 결제목록조회
		@GetMapping("/mypage/spacePayment") 
		public String spacePayment( HttpSession session, Model model, Criteria cri) {
	
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();
			
			List<PaymentVO> list = mypageService.spacePaymentList(memNo, cri.getPageNum(), cri.getAmount());
	
			int total = mypageService.getSpacePaymentCount(memNo,cri);
	
			model.addAttribute("spaceList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			model.addAttribute("status", total);
			
	
			return "mypage/spacePayment";
		}
		
		 // 공간대여 결제 취소시 결제상태 변경, 선택된 좌석 삭제, 예매목록 삭제, 팀 정산 빼기, 관리자 정산 빼기
		@PostMapping("/mypage/spacePaymentDelete")
		@ResponseBody
		public String spacePaymentDelete(@RequestParam(value = "memNo") Long memNo,
								 	     @RequestParam(value= "placeNo") int placeNo,
								 	     @RequestParam(value = "amountPrice") int amountPrice,
								 	     @RequestParam(value= "rentalNo") int rentalNo) {
			
			String msg = "success";
			
		    mypageService.updateSpacePayment(memNo, placeNo, rentalNo, amountPrice);
		    	
		    return msg;
		        
		}
		
		//환불-전체
		@GetMapping("/mypage/refund")
		public String refund(@RequestParam(value="pageNum", required=false, defaultValue = "1")int pageNum,
				                    @RequestParam(value="amount", required=false, defaultValue = "10")int amount,
				                    HttpSession session,
				                    Model model, 
				                    Criteria cri) {
			
			MemberVO memVO = (MemberVO)session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();
			
			List<MyPaymentVO> list = mypageService.refund(memNo, cri.getPageNum(),cri.getAmount());
			int total = mypageService.getRefund(cri, memNo);
			
			model.addAttribute("refund", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			model.addAttribute("status", total);
			
			return "mypage/refund";
		}
			

		
		//메세지함-목록
		@GetMapping("/mypage/messageList")
		public String messageList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
				@RequestParam(value = "amount", required = false, defaultValue = "10") int amount, HttpSession session,
				Model model, Criteria cri) {

			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();

			List<MessageVO> list = mypageService.messageList(memNo, cri.getPageNum(), cri.getAmount());
			int total = mypageService.getMessageTotal(cri, memNo);

			model.addAttribute("messageList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));

			return "mypage/messageList";
		}
		
		
		
		// QnA조회
		@GetMapping("/mypage/noticeList")
		public String noticeList(HttpSession session, Model model, Criteria cri) {
	
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();
	
			List<NoticeVO> list = mypageService.noticeListPaging(memNo, cri.getPageNum(), cri.getAmount());
			int total = mypageService.getNoticeTotal(cri, memNo);
	
			model.addAttribute("noticeList", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
	
			return "mypage/noticeList";
		}
		
		// 작성한글 -펀딩
		@GetMapping("/mypage/funding")
		public String funding(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
				@RequestParam(value = "amount", required = false, defaultValue = "10") int amount, HttpSession session,
				Model model, Criteria cri) {

			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();

			List<FundingVO> list = mypageService.funding(memNo, cri.getPageNum(), cri.getAmount());
			int total = mypageService.getFundingTotal(cri, memNo);

			model.addAttribute("funding", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));

			return "mypage/funding";
		}

		// 작성한글-공연
		@GetMapping("/mypage/show")
		public String show(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
				@RequestParam(value = "amount", required = false, defaultValue = "10") int amount, HttpSession session,
				Model model, Criteria cri) {

			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();
			List<ShowVO> list = mypageService.show(memNo, cri.getPageNum(), cri.getAmount());
			int total = mypageService.getShowTotal(cri, memNo);

			model.addAttribute("show", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));

			return "mypage/show";
		}

		// 작성한 리뷰
		@GetMapping("/mypage/myReply")
		public String myReply(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
				@RequestParam(value = "amount", required = false, defaultValue = "10") int amount, HttpSession session,
				Model model, Criteria cri) {

			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();

			List<ReplyVO> list = mypageService.myReply(memNo, cri.getPageNum(), cri.getAmount());
			int total = mypageService.getMyReplyTotal(cri, memNo);

			model.addAttribute("myReply", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			model.addAttribute("status", total);

			return "mypage/myReply";
		}
		//작성한 리뷰
		@GetMapping("/mypage/myReview")
		public String myReview(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
				@RequestParam(value = "amount", required = false, defaultValue = "10") int amount, HttpSession session,
				Model model, Criteria cri) {

			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();

			List<ReplyVO> list = mypageService.myReview(memNo, cri.getPageNum(), cri.getAmount());
			int total = mypageService.getMyReviewTotal(cri, memNo);

			model.addAttribute("myReview", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			model.addAttribute("status", total);
			
			return "mypage/myReview";
		}

		// 좋아하는 아티스트
		@GetMapping("/mypage/favorit")
		public String favorit (@RequestParam(value="pageNum", required=false, defaultValue="1")int pageNum,
					   			@RequestParam(value="amount", required=false, defaultValue="10")int amount,
					   			HttpSession session,
					   			Model model, 
					   			Criteria cri) {
					
			MemberVO memVO = (MemberVO)session.getAttribute("memVO");
			Long memNo = memVO.getMemNo();
			
			List<TeamVO> list = mypageService.favorit(memNo, cri.getPageNum(), cri.getAmount());
			int total = mypageService.getfavoritTotal(cri, memNo);
			
			model.addAttribute("favorit", list);
			model.addAttribute("pageMaker",  new PageDTO(cri, total));
			return null;

		}
		
		@GetMapping("/mypage/messageDetail")
		public String messageDetail(@RequestParam("msgNo") int msgNo,
									Model model) {
			
			System.out.println(msgNo);
			MessageVO msgInfo =  mypageService.getMsgInfo(msgNo);
			model.addAttribute("MsgInfo", msgInfo);
			
			return "mypage/messageDetail";
		}
		
		
		@GetMapping("/mypage/deleteMsg")
		@ResponseBody
		public RedirectView deleteMsg(@RequestParam("msgNo") int msgNo) {
			
			int result =mypageService.deleteMsg(msgNo);
			if(result ==1) {
				return new RedirectView("messageList");
			}
			 
			return null;
		}


		
		
		
	

}
