package com.popo.app.mypage.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.message.service.MessageVO;
import com.popo.app.mypage.mapper.MypageMapper;
import com.popo.app.mypage.service.JjimVO;
import com.popo.app.mypage.service.MyPaymentVO;
import com.popo.app.mypage.service.MypageService;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamVO;

@Service
public class MypageServiceImpl implements MypageService {

	@Autowired
	MypageMapper mypageMapper;
	
	@Autowired // 비밀번호 암호화
	BCryptPasswordEncoder bCryptPasswordEncoder;

	// 마이페이지 조회
	@Override
	public MemberVO mypage(Long memNo) {

		return mypageMapper.mypage(memNo);
	}

	// 마이페이지 수정
	@Override
	public int mypageMod(MemberVO memberVO) {

		return mypageMapper.mypageMod(memberVO);
	}

	//회원탈퇴
	@Override
	public String deleteMember(String memId) {
		
		return mypageMapper.deleteMember(memId);
	}

	//비밀번호 변경
	@Override
	public ModelAndView newPw(String memId, String memPw, Long memNo, String newPw) {
		
	MemberVO memberVO = mypageMapper.findByUserId(memId);

	try {
		  if (bCryptPasswordEncoder.matches(memPw, memberVO.getMemPw())) {
			  memberVO.setMemPw(bCryptPasswordEncoder.encode(newPw));
			  mypageMapper.newPw(memberVO.getMemPw(), memNo);				
		    return new ModelAndView("mypage/success", "message", "비밀번호가 변경되었습니다.");
		  } else {
		    return new ModelAndView("mypage/password", "error", "현재 비밀번호가 일치하지 않습니다.");
		  }
		} catch (Exception e) {
		  return new ModelAndView("mypage/password", "error", "비밀번호 변경 중 오류가 발생하였습니다.");
		}
	}

	// 내가 찜한 펀
	@Override
	public List<JjimVO> fundingJjim(Long memNo) {
		
		return mypageMapper.fundingJjim(memNo);
	}

//	@Override
//	public int getfundingJjim(Criteria cri, Long memNo) {
//		// TODO Auto-generated method stub
//		return 0;
//	}

	public List<JjimVO> showJjim(Long memNo, int pageNum, int amount) {
		
		return mypageMapper.showJjim(memNo, pageNum, amount);
	}

	@Override
	public int getShowJjim(Criteria cri, Long memNo) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public List<PaymentVO> paymentList(Long memNo, int pageNum, int amount) {
		return mypageMapper.paymentList(memNo, pageNum, amount);
	}

	@Override
	public int getPaymentCount(Long memNo, Criteria cri) {
		return mypageMapper.getPaymentCount(memNo, cri);
	}
	
	// 예매 결제 취소
	@Override
	public int updatePayment(Long memNo, int showNo, int amountPrice, int teamNum, int reservNo, int teamNo) {
		
		int count = mypageMapper.updatePayment(memNo, showNo, amountPrice, teamNum, reservNo, teamNo);
		mypageMapper.updateTeamPayment(teamNo,amountPrice,teamNum);
		mypageMapper.updateAdminPayment(amountPrice);
		mypageMapper.deleteSeat(memNo, showNo, reservNo);
		mypageMapper.deleteReserv(memNo, showNo, reservNo);	

		return count;
	}

	// 펀딩 결제
	@Override
	public List<PaymentVO> fundingPaymentList(Long memNo, int pageNum, int amount) {
		return mypageMapper.fundingPaymentList(memNo, pageNum, amount);
	}

	@Override
	public int getFundingPaymentCount(Long memNo, Criteria cri) {
		return mypageMapper.getFundingPaymentCount(memNo, cri);
	}

	
	// 펀딩 결제 취소
	@Override
	public int updateFundingPayment(Long memNo, int fundingNo, int amountPrice, int teamNum, int teamNo, int payNo) {
		
		int count = mypageMapper.updateFundingPayment(memNo, fundingNo, amountPrice, teamNum, teamNo, payNo);
		mypageMapper.updateFundingTeamPayment(teamNo,amountPrice,teamNum);
		mypageMapper.updateFundingAdminPayment(amountPrice);
		mypageMapper.minusFundingPrice(fundingNo, amountPrice);
		
		return count;
	}

	// 공간대여 결제목록
	@Override
	public List<PaymentVO> spacePaymentList(Long memNo, int pageNum, int amount) {
		return mypageMapper.spacePaymentList(memNo, pageNum, amount);
	}

	@Override
	public int getSpacePaymentCount(Long memNo, Criteria cri) {
		return mypageMapper.getSpacePaymentCount(memNo, cri);
	}

	// 공간대여 결제 취소
	@Override
	public int updateSpacePayment(Long memNo, int placeNo, int rentalNo, int amountPrice) {
		
		int count = mypageMapper.updateSpacePayment(memNo, placeNo, rentalNo, amountPrice);
		mypageMapper.updateSpaceRentalPay(placeNo, amountPrice);
		mypageMapper.updateSpaceAdminPay(amountPrice);
		mypageMapper.updateSpaceBorrower(placeNo);
		mypageMapper.deleteSpace(memNo, placeNo);
		
		return count;
	}
	
	// 환불펀딩
	@Override
	public List<MyPaymentVO> refund(Long memNo, int pageNum, int amount) {
		
		mypageMapper.refundFunding(memNo, pageNum, amount);
		
		mypageMapper.refundShow(memNo, pageNum,  amount);
		
		mypageMapper.refundPlace(memNo, pageNum, amount);
		
		return mypageMapper.refund(memNo, pageNum,amount);
	}
	

	@Override
	public int getRefund(Criteria cri, Long memNo) {
		mypageMapper.getRefundFunding( cri, memNo);
		mypageMapper.getRefundShow(cri, memNo);
		mypageMapper.getRefundPlace(cri,memNo);
		return mypageMapper.getRefund(cri, memNo);
	}


	// 메시지 목록
	@Override
	public List<MessageVO> messageList(Long memNo, int pageNum, int amount) {

		return mypageMapper.messageList(memNo, pageNum, amount);
	}

	// 메시지페이징
	@Override
	public int getMessageTotal(Criteria cri, Long memNo) {

		return mypageMapper.getMessageTotal(cri, memNo);
	}

	@Override
	public MessageVO messageDetail(int msgNo) {
		// TODO Auto-generated method stub
		return null;
	}

	// QnA게시판 조회
	@Override
	public List<NoticeVO> noticeListPaging(Long memNo, int pageNum, int amount) {

		return mypageMapper.noticeListPaging(memNo, pageNum, amount);
	}

	// QnA게시판 페이징
	@Override
	public int getNoticeTotal(Criteria cri, Long memNo) {

		return mypageMapper.getNoticeTotal(cri, memNo);
	}

	// 내가 쓴 펀딩 글 -페이징
	@Override
	public int getFundingTotal(Criteria cri, Long memNo) {

		return mypageMapper.getFundingTotal(cri, memNo);
	}

	// 내가쓴 글 목록-펀딩
	@Override
	public List<FundingVO> funding(Long memNo, int pageNum, int amount) {

		return mypageMapper.funding(memNo, pageNum, amount);
	}

	// 내가 쓴 공연글
	@Override
	public List<ShowVO> show(Long memNo, int pageNum, int amount) {

		return mypageMapper.show(memNo, pageNum, amount);
	}

	// 내가 쓴 공연 글 페이징
	@Override
	public int getShowTotal(Criteria cri, Long memNo) {

		return mypageMapper.getShowTotal(cri, memNo);
	}

	@Override
	public List<ReplyVO> myReply(Long memNo, int pageNum, int amount) {
		// TODO Auto-generated method stub
		return mypageMapper.myReply(memNo, pageNum, amount);
	}

	@Override
	public int getMyReplyTotal(Criteria cri, Long memNo) {
		// TODO Auto-generated method stub
		return mypageMapper.getMyReplyTotal(cri, memNo);
	}

	@Override
	public List<TeamVO> favorit(Long memNo, int pageNum, int amount) {
		// TODO Auto-generated method stub
		return mypageMapper.favorit(memNo, pageNum, amount);
	}

	@Override
	public int getfavoritTotal(Criteria cri, Long memNo) {
		// TODO Auto-generated method stub
		return mypageMapper.getfavoritTotal(cri, memNo);
	}

	public MessageVO getMsgInfo(int msgNo) {
		// TODO Auto-generated method stub
		return mypageMapper.getMsgInfo(msgNo);
	}

	@Override
	public int deleteMsg(int msgNo) {
		// TODO Auto-generated method stub
		return mypageMapper.deleteMsg(msgNo);
	}

	@Override
	public List<ReplyVO> myReview(Long memNo, int pageNum, int amount) {
		// TODO Auto-generated method stub
		return mypageMapper.myReview(memNo, pageNum, amount);
	}

	@Override
	public int getMyReviewTotal(Criteria cri, Long memNo) {
		// TODO Auto-generated method stub
		return mypageMapper.getMyReviewTotal(cri, memNo);
	}



}
