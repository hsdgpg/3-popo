package com.popo.app.mypage.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class MyPaymentVO {
	int payNo;
	String payType;
	Long memNo;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date payDate;
	int payPrice;
	String payStatus;
	int reservNo;
	int rentalNo;
	int fundingNo;
	
	String fundingName;
	String showName;
	String placeName;
	

	String name; //마이페이지-환불내역
}
