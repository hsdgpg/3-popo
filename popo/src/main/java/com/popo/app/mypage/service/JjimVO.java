package com.popo.app.mypage.service;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class JjimVO {
	 //찜
	int jjimNo;//찜번호
	Long memNo; //멤버 번호
	String jjimPostType;//게시글종류
	int jjimPostNo; //게시글 번호
	int jjimBoolean; //찜여부
	
	int fundingNo;
	String fundingName;
	String fundingArtist;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date fundingDay;
	
	String showName;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date showDay;
	
	
}
