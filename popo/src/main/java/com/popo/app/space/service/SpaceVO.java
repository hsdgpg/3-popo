package com.popo.app.space.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class SpaceVO {
	int placeNo;
	String placeType;
	String placeName;
	Long memNo;
	String placeRepPhoto;
	int placeCost;
	String placeOpen;
	String placeClose;
	String placeIntroduce;
	int placeSeat;
	String placeHoliday;
	String placeRefundRule;
	String placeTel;
	String addr;
	String detailAddress;
	String placeAddress;
	String placeApproval;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date placeApprovalDate;
	int placeStatus;
	
	int rn;
	String memName;
	
	int payPrice;
	
	
}
