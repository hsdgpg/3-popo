package com.popo.app.space.service.impl;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.space.mapper.SpaceMapper;
import com.popo.app.space.service.SpaceRentalVO;
import com.popo.app.space.service.SpaceReviewVO;
import com.popo.app.space.service.SpaceService;
import com.popo.app.space.service.SpaceVO;

@Service
public class SpaceServiceImpl implements SpaceService {

	@Autowired
	SpaceMapper spaceMapper;

	@Override
	public List<SpaceVO> spaceList(Criteria cri) {
		// TODO Auto-generated method stub
		return spaceMapper.spaceList(cri);
	}

	@Override
	public int getTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return spaceMapper.getTotalCount(cri);
	}

	@Override
	public SpaceVO spaceInfo(Integer placeNo) {
		// TODO Auto-generated method stub
		return spaceMapper.spaceInfo(placeNo);
	}
	
	@Override
	public SpaceVO spaceInfo2(Integer placeNo) {
		// TODO Auto-generated method stub
		return spaceMapper.spaceInfo(placeNo);
	}

	@Override
	public boolean spaceReg(SpaceVO spaceVO) {
		// TODO Auto-generated method stub
		return spaceMapper.spaceReg(spaceVO);
	}

	@Override
	public List<SpaceRentalVO> dateScheduleCheck(SpaceRentalVO srVO) {
		// TODO Auto-generated method stub	
		return spaceMapper.dateScheduleCheck(srVO);
	}

	@Override
	public List<SpaceReviewVO> spaceReviewList(Long memNo, int placeNo) {
		// TODO Auto-generated method stub
		return spaceMapper.spaceReviewList(memNo, placeNo);
	}

	@Override
	public int addReview(int placeNo, Long memNo, String commentContent, int commentScope) {
		// TODO Auto-generated method stub
		return spaceMapper.addReview(placeNo, memNo, commentContent, commentScope);
	}
	
	@Override
	public int deleteReview(Long memNo, int reviewNo) {
		return spaceMapper.deleteReview( memNo, reviewNo);
	}

	@Override
	public int reviewLike(Long memNo, int reviewNo, int placeNo) {
		spaceMapper.reviewUpCount(memNo, reviewNo, placeNo);
		
		return spaceMapper.reviewLike(memNo, reviewNo, placeNo);
	}

	@Override
	public int reviewUnLike(Long memNo, int reviewNo, int placeNo) {
		spaceMapper.reviewDownCount(memNo, reviewNo, placeNo);

		return spaceMapper.reviewUnLike(memNo, reviewNo, placeNo);
	}

	@Override
	public int rentalInsert(SpaceRentalVO spacerentalVO) {
		// 예매 추가
		int result = spaceMapper.rentalInsert(spacerentalVO);
		// 결제 내역 추가
		spaceMapper.spacePayment(spacerentalVO);
		// 공간을 빌리는 사람의 정보 업데이트
		spaceMapper.updateSpaceMember(spacerentalVO);
		// 맴버 정산
		spaceMapper.spaceSettlement(spacerentalVO);
		// 관리자 수수료
		spaceMapper.adminSettlement(spacerentalVO);

		return result;
	}
	
	@Override
	public int rentalCheck(Integer placeNo) {
		// TODO Auto-generated method stub
		
		int check= spaceMapper.rentalCheck(placeNo);
		
		return check;
		
	}

	@Override
	public void spaceDelete(Integer placeNo) {
		// TODO Auto-generated method stub				
		spaceMapper.spaceDelete(placeNo);
	}

	@Override
	public void spaceModify(SpaceVO spaceVO) {
		// TODO Auto-generated method stub
		spaceMapper.spaceModify(spaceVO);
	}

	@Override
	public List<PaymentVO> spaceDash(Long memNo) {
		
		 
		 
		 return spaceMapper.spaceDash(memNo);
	}

	@Override
	public Map<Integer, Integer> spaceRentalList(Long memNo) {
		// TODO Auto-generated method stub
		
		List<SpaceVO> rentalList=  spaceMapper.spaceRentalList(memNo);
		
		//System.out.println("==========================================서비스");
		//System.out.println(rentalList);
		Map<Integer, Integer> resultMap = new HashMap<>();
		
		for(int i=0; i<rentalList.size(); i++) {
			//System.out.println(rentalList.get(i).getPlaceNo());
			List<SpaceVO> result = spaceMapper.spaceRentalListDash(rentalList.get(i).getPlaceNo(), memNo);
			
			int payPrice = 0;
			int placeNo = rentalList.get(i).getPlaceNo();
			
			
			if (result != null && !result.isEmpty()) {
		        
		        for (SpaceVO space : result) {
		            if (space.getPlaceNo() == placeNo) {
		                payPrice = space.getPayPrice();
		                break;
		            }
		        }
		    }
			
		     resultMap.put(placeNo, payPrice);
		   
		}
		//place_no, pay_price
		//System.out.println("==============================================결과");
		//System.out.println(resultMap);
		
		
		return resultMap;
		//return spaceMapper.spaceRentalList(memNo);
	}

	@Override
	public List<SpaceVO> spaceRentalList1(Long memNo) {
		// TODO Auto-generated method stub
		return spaceMapper.spaceRentalList(memNo);
	}


}
