package com.popo.app.space.mapper;

import java.util.List;

import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.space.service.SpaceRentalVO;
import com.popo.app.space.service.SpaceReviewVO;
import com.popo.app.space.service.SpaceVO;

public interface SpaceMapper {

	List<SpaceVO> spaceList(Criteria cri);

	int getTotalCount(Criteria cri);

	SpaceVO spaceInfo(Integer placeNo);
	SpaceVO spaceInfo2(Integer placeNo);

	boolean spaceReg(SpaceVO spaceVO);

	List<SpaceRentalVO> dateScheduleCheck(SpaceRentalVO srVO);

	List<SpaceReviewVO> spaceReviewList(Long memNo, int placeNo);

	int addReview(int placeNo, Long memNo, String commentContent, int commentScope);
	// 공연 상세페이지 리뷰 삭제
	public int deleteReview(Long memNo, int reviewNo);
	// 리뷰 좋아요
	public int reviewLike(Long memNo,int reviewNo,int placeNo);
	// 리뷰 좋아요 해제
	public int reviewUnLike(Long memNo,int reviewNo,int placeNo);
	//리뷰 좋아요 수 +
	public void reviewUpCount(Long memNo,int reviewNo,int placeNo);
	//리뷰 좋아요 수 -
	public void reviewDownCount(Long memNo,int reviewNo,int placeNo);

	void spaceDelete(Integer placeNo);
	
	int rentalCheck(Integer placeNo);

	void spaceModify(SpaceVO spaceVO);

	int rentalInsert(SpaceRentalVO spacerentalVO);

	void spacePayment(SpaceRentalVO spacerentalVO);
	
	void spaceSettlement(SpaceRentalVO spacerentalVO);

	void adminSettlement(SpaceRentalVO spacerentalVO);
	
	// 공간을 빌리는 사람의 정보 업데이트
	void updateSpaceMember(SpaceRentalVO spacerentalVO);
	
	
	
	
	
	//대시보드
	List<PaymentVO> spaceDash(Long memNo);
	
	//공간 대여한 리스트
	List<SpaceVO> spaceRentalList(Long memNo);
	
	//공간별 sum 
	List<SpaceVO> spaceRentalListDash(int placeNo,Long memNo);
	
	
}
