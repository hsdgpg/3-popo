package com.popo.app.space.service;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class SpaceReviewVO {

	int reviewNo;					//리뷰번호
	int placeNo;					//공간번호
	Long memNo;						//리뷰 작성자
	String commentContent;			//리뷰 내용
	int commentHierarchy;			//계층
	int commentPno;					//부모댓글번호
	@DateTimeFormat(pattern="yyyy-mm-dd") 
	Date commentDate;				//작성일시 default sysdate
	int commentScope;				//별점
	String commentRStatus;			//신고처리상태
	int commentStatus;				//공개상태 default 0
	
	int likeBoolean;
	int likeCount;
	
	List<SpaceReviewVO> rereply;
	
	String name;
	String memName;
	
}
