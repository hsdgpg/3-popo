package com.popo.app.space.service;

import java.util.List;
import java.util.Map;

import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;

public interface SpaceService {

	List<SpaceVO> spaceList(Criteria cri);

	int getTotal(Criteria cri);

	SpaceVO spaceInfo(Integer placeNo);
	SpaceVO spaceInfo2(Integer placeNo);

	boolean spaceReg(SpaceVO spaceVO);

	List<SpaceRentalVO> dateScheduleCheck(SpaceRentalVO srVO);

	List<SpaceReviewVO> spaceReviewList(Long memNo, int placeNo);

	int addReview(int placeNo, Long memNo, String commentContent, int commentScope);

	public int deleteReview(Long memNo, int reviewNo);
	// 리뷰 좋아요
	public int reviewLike(Long memNo,int reviewNo,int placeNo);
	// 리뷰 좋아요 해제
	public int reviewUnLike(Long memNo,int reviewNo,int placeNo);

	int rentalInsert(SpaceRentalVO spaceRentalVO);
	
	int rentalCheck(Integer placeNo);

	void spaceDelete(Integer placeNo);

	void spaceModify(SpaceVO spaceVO);
	
	
	
	
	//대시보드
	public List<PaymentVO> spaceDash(Long memNo);
	//공간 대여한 리스트
	Map<Integer, Integer> spaceRentalList(Long memNo);
	List<SpaceVO> spaceRentalList1(Long memNo);


	
	
	
	
	
}
