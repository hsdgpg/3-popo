package com.popo.app.space.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.popo.app.member.service.MemberVO;
import com.popo.app.payment.service.PaymentVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.show.service.ShowReviewVO;
import com.popo.app.show.service.ShowVO;
import com.popo.app.space.service.SpaceRentalVO;
import com.popo.app.space.service.SpaceReviewVO;
import com.popo.app.space.service.SpaceService;
import com.popo.app.space.service.SpaceVO;
import com.popo.app.team.service.TeamVO;

@Controller
public class SpaceController {

	@Autowired
	SpaceService spaceService;
	
	// 공간 리스트
	@GetMapping("/page/spaceList")
	public String spaceList(Criteria cri,Model model) {
		
		List<SpaceVO> list = spaceService.spaceList(cri);		
		int total = spaceService.getTotal(cri);
		
		
		model.addAttribute("spaceList",list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));

		return "page/spaceList";
	}
	
//	@ResponseBody
//	@RequestMapping("/page/spaceList")
//	public String spaceListClone(Criteria cri,Model model) {
//		List<SpaceVO> list = spaceService.spaceList(cri);
//		
//		ObjectMapper objMapper = new ObjectMapper();
//		String json = "";
//		
//		try {
//			json = objMapper.writeValueAsString(list);
//			System.out.println(json);
//		}catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}	
//		
//		return json;		
//	}
	
	// 공간 상세정보
	@GetMapping("/page/spaceInfo")
	public String spaceInfo(@RequestParam("placeNo") Integer placeNo, Model model ) {		
		
		SpaceVO spaceVO = spaceService.spaceInfo(placeNo);
		model.addAttribute("spaceInfo",spaceVO);
		
		SpaceVO spaceVO2 = spaceService.spaceInfo2(placeNo);
		model.addAttribute("spaceInfo2",spaceVO2);
		
		return "page/spaceInfo";
	}	
	
	// 공간 등록
	@GetMapping("/page/spaceRegist")
	public String spaceRegist() {
		return "page/spaceRegist";
	}
	
	@ResponseBody
	@PostMapping("/page/spaceRegist")
	public RedirectView spaceReg(HttpSession session, SpaceVO spaceVO, BindingResult bindingResult,
							@RequestParam MultipartFile spaceImage) throws IOException {
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
				
		spaceVO.setMemNo(memVO.getMemNo()); 
		
//		if (bindingResult.hasErrors()) {
//	        List<ObjectError> errors = bindingResult.getAllErrors();
//	        for (ObjectError error : errors) {
		
	        	 if(spaceImage == null || spaceImage.isEmpty()){
	        		 
			        	String nullImage = "test/basic.jpg";
						spaceVO.setPlaceRepPhoto(nullImage);					
						spaceService.spaceReg(spaceVO);
						
						return new RedirectView("spaceList");
		    	    }

		    		String uploadDir = "test/";
		    		// 파일이름
		    		String fileName = spaceImage.getOriginalFilename();
		    		// 저장할 파일 경로
		    	    Path path = Paths.get(uploadDir + fileName);
		    	    // 파일 저장
		    	    Files.write(path, spaceImage.getBytes());
		    	    // DB에 저장할 파일 경로
		    	    String filePath = uploadDir + fileName;    	   
		    	    
		    	    spaceVO.setPlaceRepPhoto(filePath);				
					System.out.println(spaceVO);
					spaceService.spaceReg(spaceVO);		    
		    	 	
//    	    	}
//	        } else {
//		        // 유효성 검사 통과
//		    }
		
		return new RedirectView("spaceList");
	}	
	
	// 공간 수정
	@GetMapping("/page/spaceModifyForm")
	public String spaceModForm() {
		return "page/spaceModifyForm";
	}
	
	@PostMapping("/page/spaceModify")
	public RedirectView spaceModify(HttpSession session, SpaceVO spaceVO, BindingResult bindingResult,
			@RequestParam MultipartFile spaceImage) throws IOException{
		
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		spaceVO.setMemNo(memVO.getMemNo()); 

	        	 if(spaceImage == null || spaceImage.isEmpty()){
	        		 
			        	String nullImage = "test/basic.jpg";
						spaceVO.setPlaceRepPhoto(nullImage);					
						spaceService.spaceModify(spaceVO);
						
						return new RedirectView("spaceList");
		    	    }

		    		String uploadDir = "test/";
		    		// 파일이름
		    		String fileName = spaceImage.getOriginalFilename();
		    		// 저장할 파일 경로
		    	    Path path = Paths.get(uploadDir + fileName);
		    	    // 파일 저장
		    	    Files.write(path, spaceImage.getBytes());
		    	    // DB에 저장할 파일 경로
		    	    String filePath = uploadDir + fileName;    	   
		    	    
		    	    spaceVO.setPlaceRepPhoto(filePath);				
					System.out.println(spaceVO);
					spaceService.spaceModify(spaceVO);	
		    	    		
		
		return new RedirectView("spaceList");
		
	}
	
	// 공간 삭제 (비활성화)
	@GetMapping("/page/spaceInfo/delete")
	public void spaceDelete(@RequestParam(value="placeNo") Integer placeNo, HttpServletResponse response) {
		
		int check = spaceService.rentalCheck(placeNo);
		
		if(check == 0) { // 예약건이 없다 
			alert(response,"게시글을 삭제처리했습니다.","/boot/page/spaceList");// alert	
			spaceService.spaceDelete(placeNo);
		}else {
			onlyalert(response,"예약중인건이 존재하여 해당 삭제건을 처리하지 못했습니다.");
		}
		
//		System.out.println(placeNo);				
//		alert(response,"게시글을 삭제처리했습니다.","/boot/page/spaceList");// alert
		
		
	}
	
	
	// 공간 대여 일자 선택시 대여 가능시간 체크
	@PostMapping("/page/spaceInfo/dateScheduleCheck")
	@ResponseBody
	public List<SpaceRentalVO> dateScheduleCheck(@RequestParam(value = "dateSel" , required=false) String dateSel, 
			   									 @RequestParam(value = "placeNo" , required=false) Integer placeNo) {
		
		SpaceRentalVO srVO = new SpaceRentalVO();
		srVO.setDateSel(dateSel);
		srVO.setPlaceNo(placeNo);
		
		List<SpaceRentalVO> list = spaceService.dateScheduleCheck(srVO);
		
		System.out.println("아 되냐? 되냐?"+list);
		return list;
	}
	
	// 결제내역 추가, 예매내역 추가, 선택좌석 추가
	@PostMapping("/page/spacePayment")
	@ResponseBody
	public String spacePayment(@RequestBody SpaceRentalVO spaceRentalVO, Model model) {
		
		System.out.println("ㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇㅇ"+spaceRentalVO);
		
		int insertCount = spaceService.rentalInsert(spaceRentalVO);
		
		String msg = null;

		if (insertCount == 0) {
			msg = "fail";
		} else if (insertCount == 1) {
			msg = "success";			
		}

		return msg;
	}
	
	
	// 리뷰	
	
	// 리뷰 조회 
	@PostMapping("/page/reviewList")
	@ResponseBody
	public List<SpaceReviewVO> reviewList(@RequestParam(value="placeNo" , required=false) int placeNo,
								          @RequestParam(value = "memNo", required = false) Long memNo) {

		List<SpaceReviewVO> review = spaceService.spaceReviewList(memNo, placeNo);

		return review;
	}
	
	// 리뷰 등록
	@PostMapping("/page/addReview")
	@ResponseBody
	public int addReview(@RequestParam(value = "placeNo", required = false) int placeNo,
						 @RequestParam(value = "memNo", required = false) Long memNo,
						 @RequestParam(value = "commentContent", required = false) String commentContent,
						 @RequestParam(value = "commentScope", required = false) int commentScope) {
		
	    int addReview = spaceService.addReview(placeNo,memNo,commentContent,commentScope);
	    
	    return addReview;
	}
	
	// 리뷰 삭제
		@PostMapping("/page/deleteReview")
		@ResponseBody
		public int deleteReview(@RequestParam(value = "memNo") Long memNo,
							 	@RequestParam(value="reviewNo") int reviewNo) {
			
		    int deleteReview = spaceService.deleteReview(memNo,reviewNo);
		    	
		    return deleteReview;
		        
		}

		
		// 공연 상세페이지 리뷰 좋아요
		@PostMapping("/page/reviewLike")
		@ResponseBody
		public String reviewLike(@RequestParam(value="memNo" ) Long memNo,
							     @RequestParam(value="reviewNo") int reviewNo,
							     @RequestParam(value = "placeNo") int placeNo) {
		
			String msg ="success";
			
				spaceService.reviewLike(memNo,reviewNo, placeNo);
				
				return msg ;
			
		}
									
			
		// 공연 상세페이지 리뷰 좋아요해제
		@PostMapping("/page/reviewUnLike")
		@ResponseBody
		public String reviewUnLike(@RequestParam(value="memNo" ) Long memNo,
				   				   @RequestParam(value="reviewNo") int reviewNo,
				   				   @RequestParam(value = "placeNo") int placeNo) {
			
			spaceService.reviewUnLike(memNo,reviewNo, placeNo);
			
			String msg = "success";
			
			
			return msg;
		}
		
		
		
		
		//알림창 띄우고 페이지 이동
		public static void alert(HttpServletResponse response, String msg, String url) {
		    try {
		        response.setContentType("text/html; charset=utf-8");
		        PrintWriter w = response.getWriter();
		        w.write("<script>alert('"+msg+"');location.href='"+url+"';</script>\"");
		        // history.go(-1); 알림띄우고 이전페이지로 가고싶으면 위에 이거 추가
		        w.flush();
		        w.close();
		    } catch(Exception e) {
		        e.printStackTrace();
		    }
		}
		
		//알림창 띄우기만
		public static void onlyalert(HttpServletResponse response, String msg) {
		    try {
		        response.setContentType("text/html; charset=utf-8");
		        PrintWriter w = response.getWriter();
		        w.write("<script>alert('"+msg+"');history.go(-1);</script>");
		        // history.go(-1); 알림띄우고 이전페이지로 가고싶으면 위에 이거 추가
		        w.flush();
		        w.close();
		    } catch(Exception e) {
		        e.printStackTrace();
		    }
		}
		
	
		
		
		
		
		
		
		
		
		//=============================================
		//공간 대시보드 페이지 이동
		@GetMapping("/page/spaceDash")
		public String spaceDash(HttpServletRequest request, Model model) {
			HttpSession session = request.getSession();
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			
			Map<Integer, Integer> rentalList = spaceService.spaceRentalList(memVO.getMemNo());
			List<SpaceVO> spaceRentalList1 = spaceService.spaceRentalList1(memVO.getMemNo());
			
			model.addAttribute("rentalList", rentalList);
			model.addAttribute("rentalList1", spaceRentalList1);
			return "/page/spaceDash";
		}
		
		
		//대시보드
		@ResponseBody
		@PostMapping("/page/spaceDashList")
		public List<PaymentVO> spaceDashList(HttpServletRequest request, 
											Model model) throws JsonProcessingException{
			
			HttpSession session = request.getSession();
			MemberVO memVO = (MemberVO) session.getAttribute("memVO");
			 
			List<PaymentVO> list = spaceService.spaceDash(memVO.getMemNo());
			
//			Map<String, Object> result = new HashMap<>();
//			ObjectMapper objectMapper = new ObjectMapper(); // Jackson ObjectMapper 객체 생성
//			String json = objectMapper.writeValueAsString(list); // List<PaymentVO>를 JSON 형식으로 변환
//			result.put("list", json); // JSON 형식의 데이터를 Map에 추가
			
			 return list;
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}
