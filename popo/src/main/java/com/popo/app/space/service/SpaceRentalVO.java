package com.popo.app.space.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class SpaceRentalVO {
	int rentalNo;
	Long memNo;
	int	placeNo;
	
	String dateSel; // 달력에서 선택한 날짜
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date rentalStart;
//	String rentalStartDay;
	String rentalStartTime;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date rentalEnd;
//	String rentalEndDay;
	String rentalEndTime;
	
	String merchantUid;
	String impUid;
	
	int amountPrice;
	
	int payNo;
}
