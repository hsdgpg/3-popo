package com.popo.app.match.service;

import lombok.Data;

@Data
public class PositionVO {
	int positionNo;
	String teamName;
	String positionName;
}
