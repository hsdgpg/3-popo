package com.popo.app.match.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.popo.app.match.service.AccountVO;
import com.popo.app.match.service.JoinWaitingVO;
import com.popo.app.match.service.MatchService;
import com.popo.app.match.service.MatchVO;
import com.popo.app.match.service.PositionVO;
import com.popo.app.member.service.MemberService;
import com.popo.app.member.service.MemberVO;
import com.popo.app.messagingstompwebsocket.service.SpringChatVO;
import com.popo.app.port.service.PortService;
import com.popo.app.port.service.RecordVO;
import com.popo.app.practice.service.PracticeVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.team.service.TeamService;
import com.popo.app.team.service.TeamVO;

@Controller
public class MatchController {
	
	@Autowired
	MatchService matchService;
	
	@Autowired
	TeamService teamService;
	
	@Autowired
	MemberService memberService;
	
	@Autowired
	PortService portService;
	
	//매칭 글 리스트 페이징
	@GetMapping("/match/matchList") 
	public String matchList(Criteria cri,Model model,HttpSession session) {
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		MemberVO mem = matchService.selectMember(memVO.getMemNo());
		
		List<MatchVO> list = matchService.matchAllPagingList(cri);
		int total = matchService.getTotal(cri);
		
		model.addAttribute("matchList",list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		if(memVO != null) {
			model.addAttribute("session",mem);
			model.addAttribute("memNo",mem.getMemNo());
			model.addAttribute("teamNo",mem.getTeamNo());
			model.addAttribute("memRating",mem.getMemRating());
			return "match/matchList";
		}
		
		return "match/matchList";
	}
	

	
	//새로운 팀 등록 폼 
	@GetMapping("/match/newMatchInsertForm")
	public String newMatchInsertForm() {
		
		return "match/newMatchInsertForm";
	}
	
	//기존 팀 멤버영입 폼  oldMatchInsertForm
	@GetMapping("/match/oldMatchInsertForm")
	public String oldMatchInsertForm(HttpSession session,Model model) {
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		Long memNo = memVO.getMemNo();
		TeamVO teamVO = matchService.oldTeamInfo(memNo);
		
		String teamName = teamVO.getTeamName();
		String genreName = teamVO.getGenreName();
		int teamNum = teamVO.getTeamNum();
		model.addAttribute("teamNum",teamNum);
		model.addAttribute("teamName",teamName);
		model.addAttribute("genreName",genreName);
//		model.addAttribute("oldTeamInfo",teamVO);
		return "match/oldMatchInsertForm";
	}
	
	//기존에 생성된 팀 매칭글 등록 /match/oldMatchInsert
	@PostMapping("/match/oldMatchInsert")
	public String oldMatchInsert(MatchVO matchVO,HttpSession session) {
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		matchVO.setMemNo(memVO.getMemNo());
		
		matchService.matchOldInsert(matchVO);//기존팀 매칭글등록
		
		//팀 등록
		return "redirect:/match/matchList";
	}
	//새로운 팀 등록메소드
	@PostMapping("/match/newMatchInsert")
	public String matchInsert(MatchVO matchVO,HttpSession session) {
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		matchVO.setMemNo(memVO.getMemNo());
		matchVO.setMemId(memVO.getMemId());
		matchService.matchInsert(matchVO, memVO);//팀매칭글등록
		memVO.setMemRating("MR04");
		
		session.setAttribute("memVO", memVO);
		//팀 등록
		return "redirect:/match/matchList";
	}
	
	//매칭글에 해당하는 포지션 테이블에 인설트
	@ResponseBody
	@RequestMapping(value = "/match/positionInsert", method = RequestMethod.POST)
	public Object testTraditional(HttpSession session, HttpServletRequest req)
	        throws Exception {
	   Map<String, Object> resultMap = new HashMap<String, Object>();
	   // ajax를 통해 넘어온 배열 데이터 선언
	   String[] positions = req.getParameterValues("positions");
	   String[] teamName = req.getParameterValues("teamName");
	   try {
	       if(positions !=null && positions.length > 0) {
	            for(int i=0; i<positions.length; i++) {
	               System.out.println("ajax traditional result : " + i +" : "+ positions[i]);
	               
	               PositionVO vo = new PositionVO();
	               vo.setTeamName(teamName[0]);
	               vo.setPositionName(positions[i]);
	               
	               matchService.positionInsert(vo);
	       	    }
	            System.out.println("ajax traditional result : "+teamName[0]);
	       	    resultMap.put("result", "success");
	       	} else {
	      	    resultMap.put("result", "false");
	      	}
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return resultMap;
	}
	
	
	//매칭글 상세조회 페이지
	@RequestMapping(value="/match/matchSelect",method=RequestMethod.GET)
	public String matchSelect(@RequestParam(value="matchNo" ,required=false) Integer matchNo,
							  @RequestParam(value="matchTeamName" ,required=false) String matchTeamName,
							  Model model,HttpSession session) {
		
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		model.addAttribute("session",memVO);
		
		model.addAttribute("matchSelect",matchService.matchSelect(matchNo));  //매칭글
		
		model.addAttribute("position",matchService.positionSelect(matchTeamName)); //팀이 원하는 포지션 
		
		return "match/matchSelect";
	}
	
	//매칭 신청
	@ResponseBody
	@RequestMapping(value="/match/joinWaitInsert", method= RequestMethod.POST)
	public int joinWaitingInsert(JoinWaitingVO joinWaitingVO) {
		int result = matchService.joinWaitingInsert(joinWaitingVO);
		return result;
	}
	
	//매칭 신청 리스트
	@ResponseBody
	@PostMapping("/match/joinList")
	public List<JoinWaitingVO> joinList(@RequestParam(value="teamName" ,required=false) String teamName){
		List<JoinWaitingVO> vo = matchService.joinList(teamName);
		
		return vo;
	}
	
	//매칭 수락
	@ResponseBody
	@PostMapping("/match/joinYes")
	public int joinYes(@RequestParam(value="teamName" ,required=false) String teamName,
						@RequestParam(value="positionName" ,required=false) String positionName,
						@RequestParam(value="memId" ,required=false) String memId,
						@RequestParam(value="memAccountNo" ,required=false) String memAccountNo){
		MatchVO matchVO = new MatchVO();
		matchVO.setMatchTeamName(teamName);
		matchVO.setMemId(memId);
		int result = matchService.memberTeamUpdate(matchVO);//팀 등록시 멤버테이블에 팀번호 업데이트
		
		MemberVO memberVO = new MemberVO();
		memberVO.setPositionName(positionName);
		memberVO.setMemId(memId);
		matchService.memberPositionUpdate(memberVO); //팀 등록시 멤버 테이블에 포지션 업데이트
		
		RecordVO recordVO = new RecordVO();
		recordVO.setMemId(memId);
		recordVO.setTeamName(teamName);
		portService.recordInsert2(recordVO);  //record 테이블에 등록 (이력)
		
		AccountVO accountVO = new AccountVO();
		accountVO.setTeamName(teamName);
		accountVO.setMemId(memId);
		accountVO.setAccountNumber(memAccountNo);
		matchService.accountInsert2(accountVO); //계좌등록
		
		matchService.joinMessage(memId);//승인메세지 
		
		JoinWaitingVO joinWaitingVO = new JoinWaitingVO();
		joinWaitingVO.setMemId(memId);
		joinWaitingVO.setTeamName(teamName);
		matchService.joinWaitingDelete(joinWaitingVO);// 매칭 대기목록 제거 
		
		return result;
	}
	
		//매칭 거절
		@ResponseBody
		@PostMapping("/match/joinNo")
		public int joinNo(JoinWaitingVO joinWaitingVO){
			int result = matchService.joinWaitingDelete(joinWaitingVO);// 매칭 대기목록 제거 
			
			return result;
		}
		
		//매칭 글 삭제 업데이트
		@RequestMapping(value="/match/matchDelUpdate")
		public String matchDelUpdate(int matchNo,String teamName) {
			
			matchService.matchDelUpdate(matchNo,teamName);
			
			return "redirect:/match/matchList";
		}
		
		//채팅방 호출
		@RequestMapping("/match/chat")
		public String chat(Model model,HttpSession session,String teamName, String memId) {
			MemberVO memVO = (MemberVO)session.getAttribute("memVO");
			model.addAttribute("session",memVO);
			model.addAttribute("teamName",teamName);
			model.addAttribute("wkrtjd",memId);
			 
			List<SpringChatVO> list = matchService.SpringChatList(teamName);
			model.addAttribute("springChatList",list);
			
			return "match/chat";
		}
		
		//채팅 내용 저장
		@ResponseBody
		@PostMapping("/match/chatInsert")
		public int chatInsert(SpringChatVO springChatVO){
			
			int result = matchService.chatInsert(springChatVO);// 채팅내역 등록
			
			return result;
		}
		
		//매칭 거절
		@ResponseBody
		@GetMapping("/match/chatSelect")
		public List<SpringChatVO> chatSelect(String teamName){
			List<SpringChatVO> list = matchService.SpringChatList(teamName);
			
			return list;
		}
		
}
