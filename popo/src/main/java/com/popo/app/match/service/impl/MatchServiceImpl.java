package com.popo.app.match.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.popo.app.match.mapper.MatchMapper;
import com.popo.app.match.service.AccountVO;
import com.popo.app.match.service.JoinWaitingVO;
import com.popo.app.match.service.MatchService;
import com.popo.app.match.service.MatchVO;
import com.popo.app.match.service.PositionVO;
import com.popo.app.member.mapper.MemberMapper;
import com.popo.app.member.service.MemberVO;
import com.popo.app.messagingstompwebsocket.service.SpringChatVO;
import com.popo.app.port.mapper.PortMapper;
import com.popo.app.port.service.RecordVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.team.mapper.TeamMapper;
import com.popo.app.team.service.TeamVO;

@Service
public class MatchServiceImpl implements MatchService {
	
	@Autowired
	MatchMapper matchMapper;
	@Autowired
	TeamMapper teamMapper;
	@Autowired
	PortMapper portMapper;
	@Autowired
	MemberMapper memberMapper;
	
	@Override
	public List<MatchVO> selectAllmatch() {
		// TODO Auto-generated method stub
		return matchMapper.selectAllmatch();
	}

	@Transactional  //하나의 트랜젝션으로 묶어줌
	@Override
	public int matchInsert(MatchVO matchVO,MemberVO memVO) {
		
		// TODO Auto-generated method stub
		int result = matchMapper.matchInsert(matchVO);
		
		TeamVO vo = new TeamVO();
		vo.setTeamName(matchVO.getMatchTeamName());
		vo.setTeamTarget(matchVO.getMatchTarget());
		vo.setTeamSpecificity(matchVO.getMatchSpecificity());
		vo.setTeamNum(matchVO.getMatchPeople());
		vo.setGenreName(matchVO.getMatchGenreName());
		teamMapper.teamInsert(vo); //팀등록
		
		AccountVO accountVO = new AccountVO();
		accountVO.setMemNo(memVO.getMemNo());
		accountVO.setTeamName(matchVO.getMatchTeamName());
		accountVO.setAccountNumber(memVO.getMemAccountNo());
		matchMapper.accountInsert(accountVO); //계좌테이블 인설트
		
		System.out.println(matchVO.getMatchTeamName() +"///"+matchVO.getMemNo());
		matchMapper.memberTeamUpdate(matchVO);//팀 등록시 멤버테이블에 업데이트
		
		memberMapper.memberRatingUpdaet(memVO.getMemNo());//글 작성자 팀 리더로 update
		memberMapper.leaderPosition(memVO.getMemNo());
		RecordVO recordVO = new RecordVO();
		recordVO.setMemNo(memVO.getMemNo());
		recordVO.setTeamName(matchVO.getMatchTeamName()); //팀 이력
		
		portMapper.recordInsert(recordVO);
		
		return result;
	}

	@Override
	public int positionInsert(PositionVO positionVO) {
		// TODO Auto-generated method stub
		return matchMapper.positionInsert(positionVO);
	}

	@Override
	public MatchVO matchSelect(int matchNo) {
		// TODO Auto-generated method stub
		return matchMapper.matchSelect(matchNo);
	}

	@Override
	public List<PositionVO> positionSelect(String teamName) {
		// TODO Auto-generated method stub
		return matchMapper.positionSelect(teamName);
	}

	@Override
	public int joinWaitingInsert(JoinWaitingVO joinWaitingVO) {
		// TODO Auto-generated method stub
		return matchMapper.joinWaitingInsert(joinWaitingVO);
	}

	@Override
	public List<JoinWaitingVO> joinList(String teamName) {
		// TODO Auto-generated method stub
		return matchMapper.joinList(teamName);
	}

	@Override
	public int memberTeamUpdate(MatchVO matchVO) {
		// TODO Auto-generated method stub
		return matchMapper.memberTeamUpdate(matchVO);
	}

	@Override
	public int memberPositionUpdate(MemberVO memeberVO) {
		// TODO Auto-generated method stub
		return matchMapper.memberPositionUpdate(memeberVO);
	}
	
	@Transactional
	@Override
	public int joinWaitingDelete(JoinWaitingVO joinWaitingVO) {
		// TODO Auto-generated method stub
		matchMapper.noMessage(joinWaitingVO.getMemId());
		return matchMapper.joinWaitingDelete(joinWaitingVO);
	}


	@Override
	public int accountInsert2(AccountVO accountVO) {
		// TODO Auto-generated method stub
		return matchMapper.accountInsert2(accountVO);
	}
	
	@Transactional  //하나의 트랜젝션으로 묶어줌
	@Override
	public int matchDelUpdate(int matchNo,String teamName) {
		// TODO Auto-generated method stub
		//포지션 삭제
		matchMapper.positionDelete(teamName);
		matchMapper.joinWaitingDelete2(teamName);
		matchMapper.springChatDelete(teamName);
		return matchMapper.matchDelUpdate(matchNo);
	}

	@Override
	public List<MatchVO> matchAllPagingList(Criteria cri) {
		// TODO Auto-generated method stub
		return matchMapper.matchAllPagingList(cri);
	}

	@Override
	public int getTotal(Criteria cri) {
		// TODO Auto-generated method stub
		return matchMapper.getTotalCount(cri);
	}

	@Override
	public TeamVO oldTeamInfo(Long memNo) {
		// TODO Auto-generated method stub
		return matchMapper.oldTeamInfo(memNo);
	}

	@Override
	public int joinWaitingDelete2(String teamName) {
		// TODO Auto-generated method stub
		return matchMapper.joinWaitingDelete2(teamName);
	}

	@Override
	public int matchOldInsert(MatchVO matchVO) {
		// TODO Auto-generated method stub
		int result = matchMapper.matchInsert(matchVO);
		return result;
	}

	@Override
	public int chatInsert(SpringChatVO springChatVO) {
		// TODO Auto-generated method stub
		return matchMapper.chatInsert(springChatVO);
	}

	@Override
	public List<SpringChatVO> SpringChatList(String teamName) {
		// TODO Auto-generated method stub
		return matchMapper.SpringChatList(teamName);
	}

	@Override
	public MemberVO selectMember(Long memNo) {
		// TODO Auto-generated method stub
		return matchMapper.selectMember(memNo);
	}

	@Override
	public int joinMessage(String memId) {
		// TODO Auto-generated method stub
		return matchMapper.joinMessage(memId);
	}

	@Override
	public int noMessage(String memId) {
		// TODO Auto-generated method stub
		return matchMapper.noMessage(memId);
	}

	
 
}
