package com.popo.app.match.service;

import lombok.Data;

@Data
public class JoinWaitingVO {
	int joinWaitingNo;
	String teamName;
	String memId;
	String positionName;
	String memAccountNo;
}
