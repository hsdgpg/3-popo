package com.popo.app.match.service;

import lombok.Data;

@Data
public class AccountVO {
	/*
	 *  MEM_NO         NOT NULL NUMBER(20)    
		TEAM_NO        NOT NULL NUMBER(20)    
		ACCOUNT_NUMBER          VARCHAR2(300) 
		ACCOUNT_PRICE           NUMBER(10)    
		GRADE                   VARCHAR2(20)  
	 */
	Long memNo;
	String memId;
	int teamNo;
	String teamName;
	String accountNumber;
}
