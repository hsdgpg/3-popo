package com.popo.app.match.mapper;

import java.util.List;

import com.popo.app.match.service.AccountVO;
import com.popo.app.match.service.JoinWaitingVO;
import com.popo.app.match.service.MatchVO;
import com.popo.app.match.service.PositionVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.messagingstompwebsocket.service.SpringChatVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.team.service.TeamVO;

public interface MatchMapper {
	public List<MatchVO>selectAllmatch(); //매칭글 목록
	
	public int matchInsert(MatchVO matchVO); //매칭글 등록
	
	public int positionInsert(PositionVO positionVO);//팀매칭 게시글 등록후 -> 팀 생성 -> 팀 상세조회했을때 게시글에 맞는 포지션이 나오게 
	
	public MatchVO matchSelect(int matchNo);//매칭글 상세 조회 
	
	public List<PositionVO> positionSelect(String teamName); //팀이구하는 포지션 가져올거임
	
	public int joinWaitingInsert(JoinWaitingVO joinWaitingVO); //매칭 신청
	
	public List<JoinWaitingVO> joinList(String teamName); // 매칭 신청 리스트
	
	public int memberTeamUpdate(MatchVO matchVO); //멤버의 팀 칼럼 업데이트
	
	public int memberPositionUpdate(MemberVO memberVO); //멤버 신청 수락시 신청한 사람 포지션 업데이트
	
	public int joinWaitingDelete(JoinWaitingVO joinWaitingVO);// 매칭 대기목록 제거
	
	public int accountInsert(AccountVO accountVO); //팀등록시 계좌테이블에 insert
	
	public int accountInsert2(AccountVO accountVO); //승인시 계좌테이블에 insert
	
	public int springChatDelete(String teamName); // 채팅내역 삭제 
	
	public int matchDelUpdate(int matchNo); //매칭글 삭제 업데이트
	
	public List<MatchVO> matchAllPagingList(Criteria cri); //전체페이징
	
	public int getTotalCount(Criteria cri);//전체건수
	
	public int positionDelete(String teamName);//포지션 삭제
	
	public TeamVO oldTeamInfo(Long memNo);//기존에 있던팀이 멤버 영입글 쓸때 가져올 데이터
	
	public int joinWaitingDelete2(String teamName);// 글삭제시 매칭 대기목록 제거
	
	public int chatInsert(SpringChatVO springChatVO);// 채팅내역 등록
	
	public List<SpringChatVO> SpringChatList(String teamName); //채팅내역 가져오기
	
	public MemberVO selectMember(Long memNo);
	
	public int joinMessage(String memId);// 승인메세지
	
	public int noMessage(String memId);//반려메세지
	
}
