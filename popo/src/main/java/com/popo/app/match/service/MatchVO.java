package com.popo.app.match.service;

import java.util.Date;

import lombok.Data;

@Data
public class MatchVO {
	/*
	MATCH_NO          NOT NULL NUMBER(5)     
	MEM_NO            NOT NULL NUMBER(5)     
	MATCH_QUALIFI              VARCHAR2(100) 
	MATCH_ARGET                VARCHAR2(100) 
	MATCH_SPECIFICITY          VARCHAR2(100) 
	MATCH_PEOPLE      NOT NULL NUMBER(3)     
	MATCH_DATE                 DATE          
	MATCH_TEAM_NAME   NOT NULL VARCHAR2(24)  
	MATCH_GENRE_NAME  NOT NULL VARCHAR2(15)  
	MATCH_STATUS               NUMBER(1)
	 */
	int matchNo;
	Long memNo;
	String matchName;
	String matchQualifi;
	String matchTarget;
	String matchSpecificity;
	int matchPeople;
	Date matchDate;
	String matchTeamName;
	String matchGenreName;
	int matchStatus;
	String memId;
	
}
