package com.popo.app.teampage.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamVO;
import com.popo.app.teampage.mapper.TeamPageMapper;
import com.popo.app.teampage.service.TeamPageService;

@Service
public class TeamPageServiceImpl implements TeamPageService{

	@Autowired
	TeamPageMapper teamMapper;
	
	//생산계획 전체 조회
	public List<ReplyVO> getProductionAllPlanList(int teamNo){
		
		
		
		return teamMapper.getProductionAllPlanList(teamNo);
	}

	@Override
	public List<ShowVO> getDdayPlan(int teamNo) {
		return teamMapper.getDdayPlan(teamNo);
	}

	@Override
	public List<ShowVO> getPlan(int teamNo) {
		return teamMapper.getPlan(teamNo);
	}

	@Override
	public List<NoticeVO> teamNoticePagingList1(int teamNo, int pageNum) {
		return teamMapper.teamNoticePagingList1(teamNo, pageNum);
	}

	@Override
	public int teamNoticepageTotalCount(int teamNo,Criteria cri) {
		return teamMapper.teamNoticepageTotalCount(teamNo,cri);
	}

	@Override
	public int noticeInsert(int memNo, String noticeName, String noticeContent, int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.noticeInsert(memNo, noticeName, noticeContent, teamNo);
	}

	@Override
	public NoticeVO teamNoticeDetail(int noticeNo, int teamNo) {
		
		return teamMapper.teamNoticeDetail(noticeNo, teamNo);
	}

	@Override
	public void NoticeViewCount(int noticeNo, int teamNo) {
		teamMapper.NoticeViewCount( noticeNo, teamNo);
		
	}

	@Override
	public int NoticeReplyTotalCount(int teamNo,int noticeNo,Criteria cri) {
		return teamMapper.NoticeReplyTotalCount(teamNo, noticeNo,cri);
	}

	@Override
	public List<FundingVO> FundingList(int teamNo,Criteria cri) {
		// TODO Auto-generated method stub
		return teamMapper.FundingList(teamNo,cri.getPageNum(), cri.getAmount());
	}

	@Override
	public List<TeamVO> getTeam(int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.getTeam(teamNo);
	}

	@Override
	public int updateTeamNotice(String teamTarget, String teamSpecificity, String teamImage, int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.updateTeamNotice(teamTarget,teamSpecificity,teamImage,teamNo);
	}

	@Override
	public int noticeMod(NoticeVO noticeVO) {
		// TODO Auto-generated method stub
		return teamMapper.noticeMod(noticeVO);
	}

	@Override
	public int noticeDelete(int noticeNo) {
		// TODO Auto-generated method stub
		return teamMapper.noticeDelete(noticeNo);
	}

	@Override
	public List<FundingVO> FundingResult(int fundingNo) {
		// TODO Auto-generated method stub
		return teamMapper.FundingResult(fundingNo);
	}

	

	@Override
	public List<TeamVO> getFundingListByMonth(int teamNo) {
		
		return teamMapper.getFundingListByMonth(teamNo);
	}

	@Override
	public List<TeamVO> getFundingAllSum(int teamNo) {
		
		return teamMapper.getFundingAllSum(teamNo);
	}

	@Override
	public int deleteTeam(int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.deleteTeam(teamNo);
	}

	@Override
	public int deleteTeam1(int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.deleteTeam1(teamNo);
	}

	@Override
	public List<TeamVO> getShowAllSum(int teamNo) {
		return teamMapper.getShowAllSum(teamNo);
	}

	@Override
	public List<TeamVO> getShowMonthSum(int teamNo) {
		return teamMapper.getShowMonthSum(teamNo);
	}

	@Override
	public List<ReplyVO> NoticeReplyIs(int noticeNo, int teamNo, int pageNum, int amount) {
		// TODO Auto-generated method stub
		return teamMapper.NoticeReplyIs(noticeNo, teamNo, pageNum, amount);
	}

	@Override
	public void reportNoticeReply(ReplyReportVO replyReportVO) {
		teamMapper.reportNoticeReply(replyReportVO);
	}

	@Override
	public List<MemberVO> getTeamMember(int teamNo) {
		// TODO Auto-generated method stub
		return teamMapper.getTeamMember(teamNo);
	}

	@Override
	public int deleteTeamMember(int memNo) {
		// TODO Auto-generated method stub
		return teamMapper.deleteTeamMember(memNo);
	}

	@Override
	public int teamNumMinus(int memNo) {
		System.out.println("Service =============================");
		System.out.println(memNo);
		
		MemberVO memInfo = teamMapper.findInfo(memNo);
		teamMapper.teamNumMinus(memInfo.getTeamNo());
		teamMapper.teamRecordUpdate(memInfo.getMemNo(), memInfo.getTeamNo());
		
		
		return teamMapper.deleteTeamMember(memNo);
	}

	@Override
	public ShowVO getShow2(int showNo, int memNo) {
		// TODO Auto-generated method stub
		return teamMapper.getShow2(showNo,memNo);
	}

	@Override
	public int FundingTotal(int teamNo,Criteria cri) {
		// TODO Auto-generated method stub
		return teamMapper.FundingTotal(teamNo,cri);
	};


	
}
