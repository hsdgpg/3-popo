package com.popo.app.teampage.service;

import java.util.List;
import java.util.Map;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamVO;


public interface TeamPageService {

	//대시보드
	public List<TeamVO> getFundingAllSum(int teamNo);
	public List<TeamVO> getFundingListByMonth(int teamNo);
	//공연 총 매출
	public List<TeamVO> getShowAllSum(int teamNo);
	public List<TeamVO> getShowMonthSum(int teamNo);
	
	
	//팀 관리 =====================================================
	public List<TeamVO> getTeam(int teamNo);
	public List<MemberVO> getTeamMember(int teamNo);
	//팀 정보 수정
	public int updateTeamNotice(String teamTarget, String teamSpecificity, String teamImage, int teamNo);
	//팀 삭제 
	public int deleteTeam(int teamNo);
	public int deleteTeam1(int teamNo);
	//팀 멤버 삭제
	public int deleteTeamMember(int memNo);
	public int teamNumMinus(int memNo);
		
		
	//펀딩========================================================
	public List<FundingVO> FundingList(int teamNo,Criteria cri);
	public List<FundingVO> FundingResult(int fundingNo);	
	public int FundingTotal(int teamNo,Criteria cri);
	
	//공연 일정================================================
	//계획 전체 조회
	public List<ReplyVO> getProductionAllPlanList(int teamNo);	
	//d-day 일정
	public List<ShowVO> getDdayPlan(int teamNo);
	//공연예정
	public List<ShowVO> getPlan(int teamNo);
	// 찜 데이터가 있는 여부
	public ShowVO getShow2(int showNo, int memNo);
	//공지사항==================================================
	//페이징
	public List<NoticeVO> teamNoticePagingList1(int teamNo, int pageNum);  //전체 페이징
	//전체건수
	public int teamNoticepageTotalCount(int teamNo,Criteria cri);
	//글쓰기
	public int noticeInsert(int memNo, String noticeName, String noticeContent, int teamNo);
	// 수정
	public int noticeMod(NoticeVO noticeVO); 
	//삭제(업데이트
	public int noticeDelete(int noticeNo); 
	//상세페이지
	public NoticeVO teamNoticeDetail(int noticeNo, int teamNo);//단건조회.
	//공지사항 조회수 업데이트
	public void NoticeViewCount(int noticeNo, int teamNo);
	//댓글 전체 건수
	public int NoticeReplyTotalCount(int teamNo,int noticeNo,Criteria cri);
	//공지 댓글 신고	
	public void reportNoticeReply(ReplyReportVO replyReportVO);
	//공지사항 게시판 댓글 조회
	public List<ReplyVO> NoticeReplyIs(int noticeNo,int teamNo,int pageNum, int amount);		
		

}
