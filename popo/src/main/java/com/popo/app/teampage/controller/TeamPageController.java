package com.popo.app.teampage.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import com.popo.app.funding.service.FundingVO;
import com.popo.app.member.service.MemberVO;
import com.popo.app.notice.service.NoticeService;
import com.popo.app.notice.service.NoticeVO;
import com.popo.app.replyReport.service.ReplyReportVO;
import com.popo.app.show.service.Criteria;
import com.popo.app.show.service.PageDTO;
import com.popo.app.show.service.ShowService;
import com.popo.app.show.service.ShowVO;
import com.popo.app.team.service.ReplyVO;
import com.popo.app.team.service.TeamService;
import com.popo.app.team.service.TeamVO;
import com.popo.app.teampage.service.TeamPageService;

//지은 팀 관리
@Controller
public class TeamPageController {

	@Autowired
	TeamPageService teamService;
	
	@Autowired
	TeamService tService;
	
	@Autowired
	NoticeService noticeService;
	
	@Autowired
	ShowService showService;

	

	//팀 대시보드=====================================================================================================
	@GetMapping("/teampage/teampageDash")
	public String teampageDash(Criteria cri, 
								Model model, 
								HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
    	
		//이번달 펀딩 리스트, 이번달 펀딩 금액, 총 펀딩수익금액
		List<TeamVO> FunMonth = teamService.getFundingListByMonth(memVO.getTeamNo());
		model.addAttribute("FundingListMonth",FunMonth);
		List<TeamVO> FunAllsum = teamService.getFundingAllSum(memVO.getTeamNo());
		model.addAttribute("FundingAllSum", FunAllsum);


		//이번달 공연 리스트, 이번달 공연 금액, 총 공연수익
		List<TeamVO> showAll = teamService.getShowAllSum(memVO.getTeamNo());
		model.addAttribute("ShowAllSum", showAll);
		List<TeamVO> showMonth = teamService.getShowMonthSum(memVO.getTeamNo());
		model.addAttribute("ShowMonthSum", showMonth);
		
	
		return "teampage/teampageDash";
		}
	
	
	
	
	
	
	
	
	
	
	//사진 업로드
	@ResponseBody
	@PostMapping("/teampage/testI")
	public RedirectView  testI( Model model,
								TeamVO teamVO,BindingResult bindingResult,
								@RequestParam MultipartFile teamImage,
								HttpServletRequest request) throws IOException{
							
		if (bindingResult.hasErrors()) {
	        List<ObjectError> errors = bindingResult.getAllErrors();
	        for (ObjectError error : errors) {
	        	HttpSession session = request.getSession();
	    		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
	    		//null 확인
	        	 if(teamImage == null || teamImage.isEmpty()){
		    	        String nullImage = "test/basic.jpg";
		    	    	teamVO.setTeamImage(nullImage);
		    	    	teamService.updateTeamNotice(teamVO.getTeamTarget(), teamVO.getTeamSpecificity(),teamVO.getTeamImage(),memVO.getTeamNo());
		    	        return new RedirectView("teamPage");
		    	    }
	        	//업로드 경로
	    		String uploadDir = "test/";
	    		// 파일이름
	    		String fileName = teamImage.getOriginalFilename();
	    		// 저장할 파일 경로
	    	    Path path = Paths.get(uploadDir + fileName);
	    	    // 파일 저장
	    	    Files.write(path, teamImage.getBytes());
	    	    // DB에 저장할 파일 경로
	    	    String filePath = uploadDir + fileName;
	    	    
	            teamVO.setTeamImage(filePath);
	            teamService.updateTeamNotice(teamVO.getTeamTarget(), teamVO.getTeamSpecificity(),teamVO.getTeamImage(),memVO.getTeamNo());
	        }
	        // 오류 처리
	    } else {
	        // 유효성 검사 통과
	    }
		
		
		
		  //return new RedirectView("boot/teampage/teamPage");
		return new RedirectView("teamPage");
		
	}
	
	
	
	
	//펀딩 결과==========================================================================
	@GetMapping("/teampage/teamFundingResult")
	public String teamFundingResult(Criteria cri, 
									Model model, 
									HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");

		List<FundingVO> list =teamService.FundingList(memVO.getTeamNo(),cri);
		int total = teamService.FundingTotal(memVO.getTeamNo(),cri);
		model.addAttribute("FundingList",list);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		
		return "teampage/fundingResult";
		
		}
	
	@PostMapping("/teampage/teamFundingResult1")
	public void teamFundingResult1(Criteria cri, 
									Model model, 
									HttpServletRequest request,
									int fundingNo) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		List<FundingVO> list = teamService.FundingResult(fundingNo);
	    model.addAttribute("FundingResult",list);
		
	    //return "teampage/fundingResult";
	   
		}
	
	//달력 테스트
	@GetMapping("/teampage/testPlan")
	public String testPlan(Model model,
								HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		//team_no 팀이 결성되었을때 
		if(memVO.getTeamNo() >0 ) {
			List<ShowVO> list = teamService.getDdayPlan(memVO.getTeamNo());
			List<ShowVO> plan = teamService.getPlan(memVO.getTeamNo());
	
			System.out.println("===================================================");
			System.out.println(list);
			model.addAttribute("DdayPlan", list);
			model.addAttribute("Plan", plan);
	
			return "teampage/testPlan";
		}
		
		return "team/noneTeam";
	}
	
	
	
	
	// 공연일정 =====================================================================================================
	@GetMapping("/teampage/teampagePlan")
	public String teampagePlan( Model model,
								HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		MemberVO memInfo = tService.findMemberInfo(memVO.getMemNo());
		session.setAttribute("memVO", memInfo);
		MemberVO memVO1 = (MemberVO) session.getAttribute("memVO");
		
		//team_no 팀이 결성되었을때 
		if(memVO1.getTeamNo() >0 ) {
			List<ShowVO> list = teamService.getDdayPlan(memVO1.getTeamNo());
			List<ShowVO> plan = teamService.getPlan(memVO1.getTeamNo());
	
			System.out.println("===================================================");
			System.out.println(list);
			model.addAttribute("DdayPlan", list);
			model.addAttribute("Plan", plan);
	
			return "teampage/teampagePlan";
		}
		
		return "team/noneTeam";
	}

	@ResponseBody
	@GetMapping("/teampage/getPlan")
	public HashMap<String, Object> getPlan(Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		
		HashMap<String, Object> map = new HashMap<String, Object>();

		List<ReplyVO> list = teamService.getProductionAllPlanList(memVO.getTeamNo());
		map.put("planlist", list);
		
		return map;
	}
	
	// 상세화면
		@GetMapping("/teampage/showDetail")
		public String showDetail(@RequestParam(value="showNo", required = true) String showNo,
							     @RequestParam(value = "memNo", required = true) String memNo, 
							     Model model) {
			
			ShowVO vo = showService.getShow(Integer.parseInt(showNo));
			// 찜 여부
			//ShowVO vo2 = showService.getShow2(showNo,memNo);
			ShowVO vo2 = teamService.getShow2(Integer.parseInt(showNo), Integer.parseInt(memNo));
			// 예매기간 지났는지 여부
			int vo3 = showService.showDateStatus(Integer.parseInt(showNo));

			model.addAttribute("show", vo);
			model.addAttribute("show2", vo2);
			model.addAttribute("showStatus", vo3);

			return "show/showDetail";
		}

	
	
	//공지사항 ========================================================================================================
	@GetMapping("/teampage/teampageNotice")
	public String teamNotice(Criteria cri,Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamVO teamInfo = (TeamVO) session.getAttribute("teamInfoForHistory");
		MemberVO memVO = (MemberVO)session.getAttribute("memVO");
		
		if( teamInfo == null  || teamInfo.getTeamNo() == 0) {
			
			List<NoticeVO> list = noticeService.teamNoticePagingList(memVO.getTeamNo(),cri.getPageNum());
			List<NoticeVO> reply = noticeService.CountNoticeReply(memVO.getTeamNo());
			int total = noticeService.teamNoticepageTotalCount(memVO.getTeamNo(),cri);
			
			model.addAttribute("CountReply", reply);
			model.addAttribute("teamNotice", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			return "teampage/teampageNotice";
		
		}else {
			List<NoticeVO> list = noticeService.teamNoticePagingList(teamInfo.getTeamNo(),cri.getPageNum());
			List<NoticeVO> reply = noticeService.CountNoticeReply(teamInfo.getTeamNo());
			int total = noticeService.teamNoticepageTotalCount(teamInfo.getTeamNo(),cri);
			
			//model.addAttribute("CurrentMemId", memId);
			model.addAttribute("CountReply", reply);
			model.addAttribute("teamNotice", list);
			model.addAttribute("pageMaker", new PageDTO(cri, total));
			return "teampage/teampageNotice";
		}
		
		
	}

	// 공지사항 글쓰기
	@GetMapping("/teampage/NoticeInsertForm")
	public String NoticeInsertForm() {
		return "teampage/noticeInsertForm";
	}

	@PostMapping("/teampage/noticeInsert")
	public String practiceInsert(NoticeVO noticeVO, HttpSession session) {

		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		int memId = Long.valueOf(memVO.getMemNo()).intValue();

		teamService.noticeInsert(memId,noticeVO.getNoticeName(),noticeVO.getNoticeContent(),memVO.getTeamNo());

		return "redirect:/teampage/teampageNotice";
	}

	// 글 수정
		@RequestMapping(value = { "/teampage/noticeModForm" })
		public String noticeModForm(@RequestParam(value = "noticeNo") Integer noticeNo,
				@RequestParam(value = "noticeName", required = false) String noticeName,
				@RequestParam(value = "memNo", required = false) Integer memNo,
				@RequestParam(value = "noticeContent", required = false) String noticeContent, Model model) {

			model.addAttribute("noticeNo", noticeNo);
			model.addAttribute("noticeName", noticeName);
			model.addAttribute("memNo", memNo);
			model.addAttribute("noticeContent", noticeContent);

			return "teampage/teampageNoticeModForm";
		}
		
		
		@PostMapping("/teampage/noticeMod")
		public String noticeMod(NoticeVO noticeVO) {
			teamService.noticeMod(noticeVO);
			return "redirect:/teampage/teampageNotice";
		}
	
		// 삭제
		@RequestMapping(value = { "/teampage/teampageNoticeDelete" })
		public String NoticeDelete(@RequestParam(value = "noticeNo") Integer noticeNo) {
			teamService.noticeDelete(noticeNo);
			return "redirect:/teampage/teampageNotice";
		}
		

	// 팀 공지사항 상세페이지
	@RequestMapping(value = { "/teampage/teamNoticeDetail" })
	public String get(NoticeVO noticeVO,
					@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
					  Model model,
					  HttpServletRequest request, Criteria cri) {
		
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		int total = teamService.NoticeReplyTotalCount(memVO.getTeamNo(), noticeNo,cri);

		teamService.NoticeViewCount(noticeVO.getNoticeNo(),memVO.getTeamNo());

		NoticeVO vo = teamService.teamNoticeDetail(noticeVO.getNoticeNo(), memVO.getTeamNo());

		model.addAttribute("teamNoticeboard", vo);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		model.addAttribute("NoticeNo", noticeVO.getNoticeNo());
		
		return "teampage/teampageNoticeDetail";

	}
	
	//글쓰기 수정 삭제 만들어야 함.
	//댓글 이동
	@GetMapping("/teampage/reply")
	public String reply(Model model,
			            Criteria cri ,
						@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
						HttpServletRequest request) {
		HttpSession session = request.getSession();
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");

		int total = teamService.NoticeReplyTotalCount(teamVO.getTeamNo(), noticeNo,cri);
		model.addAttribute("pageMaker", new PageDTO(cri, total));
		return "team/Reply";
	}
	
	//공지 댓글 신고
		@PostMapping("/teampage/reportNoticeReply")
		@ResponseBody
		public void reportNoticeReply(ReplyReportVO replyReportVO, Model model) {
			try {
				teamService.reportNoticeReply(replyReportVO);
			} catch (Exception e) {
				// 예외 처리 코드
				e.printStackTrace();
			}
		}

	
	
	@ResponseBody
	@PostMapping("/teampage/replyList")
	public List<ReplyVO> replyList( Criteria cri, 
									@RequestParam(value = "noticeNo", required = false) Integer noticeNo,
									@RequestParam(value = "pageNum", required = false) Integer pageNum, 
									HttpServletRequest request,
									Model model) {
		// 댓글 5개만 가져오기.
		HttpSession session = request.getSession();
		TeamVO teamVO = (TeamVO)session.getAttribute("teamList");
		List<ReplyVO> reply = teamService.NoticeReplyIs(noticeNo, teamVO.getTeamNo(),pageNum, 5);

		String msg;
		if (reply != null) {
			// model.addAttribute("AjaxreplyList", reply);

			return reply;
		} else {
			msg = "fail";
		}

		return null;
	}

	
	
	
	
	//팀 관리=====================================================================================================
	@GetMapping("/teampage/teamPage")
	public String teamPage(Criteria cri, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		List<TeamVO> teamNotice = teamService.getTeam(memVO.getTeamNo());
		model.addAttribute("teamNotice", teamNotice);
		
		return "teampage/teamPage";
		}
	
	//팀 정보 수정 - 팀 이름 제외하기
	@GetMapping("/teampage/teamPageMod")
	public String teamPageMod(	Criteria cri, 
								Model model, 
								HttpServletRequest request,
								TeamVO teamVO) {
		//teamImage, teamSpecificity,teamTarget,teamNo
		//이미지, 팀 소개, 팀 목표 수정, 팀장르
		HttpSession session = request.getSession();
		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
		List<TeamVO> teamNotice = teamService.getTeam(memVO.getTeamNo());
		List<MemberVO> memberList = teamService.getTeamMember(memVO.getTeamNo());
		model.addAttribute("teamNotice", teamNotice);
		model.addAttribute("memberList", memberList);
		
		return "teampage/teamPageMod";
		}
	
	
	//팀 탈퇴
//	@PostMapping("/teampage/deleteTeam")
//	@ResponseBody
//	public void deleteTeam(int teamNo,
//							HttpServletRequest request) {
//		
//		HttpSession session = request.getSession();
//		MemberVO memVO = (MemberVO) session.getAttribute("memVO");
//
//		
//		teamService.deleteTeam(teamNo);
//		teamService.deleteTeam1(teamNo);
//		
//		memVO.setTeamNo(0);
//		session.setAttribute("memVO", memVO);
//				
//	}
//	
	
	@PostMapping("/teampage/deleteTeamMember")
	@ResponseBody
	public void deleteTeamMember(int memNo) {
		teamService.teamNumMinus(memNo);
		//teamService.deleteTeamMember(memNo);
		
	}
	
	
	
	
	

}
