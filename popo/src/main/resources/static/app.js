/**
 * 
 */
var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/boot/chatServer');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        let teamName = $('input[id="teamName"]').val()
        
        $.ajax({
            url : '/boot/match/chatSelect',
            type:'get',
            data:{
                "teamName" : teamName
            },
            success : function(result){
                console.log(result)
                $.each(result,function(idx,data){
                    if(data.teamName == teamName){
                        let my = $('input[id="memId"]').val()
                        let boss = $('input[id="wkrtjd"]').val()
                        if(my == data.memId){
                            $("#greetings").append("<tr class='myMsg'><td class='msg'>"+ data.chatContent + "</td></tr>");
                        }else if(my != data.memId){
                            if(boss == data.memId){
                                $("#greetings").append("<tr><td class='anotherMsg'><span class='anotherName'>👑" + data.memId +'👑</span><span class="msg">'+ data.chatContent + "</span></td></tr>");
                            }else if(boss != data.memId){
                                $("#greetings").append("<tr><td class='anotherMsg'><span class='anotherName'>" + data.memId +'</span><span class="msg">'+ data.chatContent + "</span></td></tr>");
                            }
                        }
                    }
                })
            }
        })
        stompClient.subscribe('/topic/chat', function (sendVO) {
           
             
            showSend(JSON.parse(sendVO.body).memId,JSON.parse(sendVO.body).content); //json 으로 자동으로보낸거 파싱해서 받음
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/app/hello", {}, JSON.stringify({'memId': $("#memId").val(),'content': $("#content").val()}));  //메세지보낼떠 쓰는 주소
    showSend($("#memId").val(),$("#content").val())
    $('#content').val('')
}
function showSend(memId,content) {
    console.log(memId,content)
    let my = $('input[id="memId"]').val()
    let boss = $('input[id="wkrtjd"]').val()
        if(my == memId){
            $("#greetings").append("<tr class='myMsg'><td class='msg'>"+ content+ "</td></tr>");
        }else if(my != memId){
            if(boss == memId){
                $("#greetings").append("<tr><td class='anotherMsg'><span class='anotherName'>👑"+memId +'👑</span><span class="msg">'+chatContent+"</span></td></tr>");
            }else if(boss != memId){
                $("#greetings").append("<tr><td class='anotherMsg'><span class='anotherName'>"+memId +'</span><span class="msg">'+chatContent+"</span></td></tr>");
            }
        }
    let teamName = $('input[id="teamName"]').val()
    $.ajax({
        url : '/boot/match/chatInsert',
        type:'post',
        data:{
            "teamName" : teamName,
            "memId" : memId,
            "chatContent" : content
        },
        success : function(result){
            console.log(result)
        }
    })
    
    
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});