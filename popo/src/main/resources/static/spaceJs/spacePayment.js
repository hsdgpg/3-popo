// 공간 결제

$(document).ready(function () {


    // 결제
    $('#payment').on('click', function (e) {

        e.preventDefault();

        var IMP = window.IMP;
        IMP.init("imp20852873"); // "iamport" 대신 발급받은 "가맹점 식별코드"를 사용합니다.

        //IMP.request_pay(param, callback) //호출
        IMP.request_pay({ // param
            pg: "kakaopay", // PG사 선택
            pay_method: "kakaopay", // 지불 수단
            merchant_uid: 'merchant_' + new Date().getTime(), //가맹점에서 구별할 수 있는 고유한id
            name: $('#placeName').val(), // 상품명
            amount: parseInt($('.payAmount').text()), // 가격
            buyer_email: "test@gmail.com",
            buyer_name: "tester", // 구매자 이름
            buyer_tel: "010-4242-4242", // 구매자 연락처 
            buyer_addr: "서울특별시 강남구 신사동", // 구매자 주소지
            buyer_postcode: "01181", // 구매자 우편번호
            // buyer_email: $('#memEmail').val(),
            // buyer_name: $('#memName').val(), // 구매자 이름
            // buyer_tel: $('#memTel').val(), // 구매자 연락처 
            // buyer_addr: $('#memAddr1').val()+$('#memAddr2').val(), // 구매자 주소지
            // buyer_postcode: "01181", // 구매자 우편번호
        }, function (rsp) { // callback
            if (rsp.success) {
                alert("결제성공")

                var impUid = rsp.imp_uid;
                var merchantUid = rsp.merchant_uid;
                console.log(impUid);
                console.log(merchantUid);
                console.log($('#rentalEnd').val());
                console.log($('#memNo').val());

                $.ajax({
                    url: '/boot/page/spacePayment',
                    type: 'post',
                    async : false,
                    data: JSON.stringify({
                        "placeNo": $('#placeNo').val(),
                        "rentalStartTime": $('#rentalStart').val(),
                        "rentalEndTime": $('#rentalEnd').val(),
                        "amountPrice": parseInt($('.payAmount').text()),
                        "memNo": $('#memNo').val(),
                        "merchantUid" : merchantUid,
                        "impUid" : impUid
                    }),
                    contentType: 'application/json',
                    dataType: 'text',
                    success: function (result) {

                        if (result == 'success') {
                            alert('통신 성공');

                            location.href = "/boot/page/spaceList"
                        }

                    },
                    error: function () {
                        alert('통신 실패');
                    }
                });

            } else {
                alert("결제실패")
            }
        });

    })




    // // 환불
    // $('#payment2').on('click', function (e) {

    //     e.preventDefault();

    //     var IMP = window.IMP;
    //     IMP.init("imp20852873"); // "iamport" 대신 발급받은 "가맹점 식별코드"를 사용합니다.

    //     // IMP.request_pay(param, callback) 호출
    //     IMP.request_pay({ // param
    //         pg: "inicis", // PG사 선택
    //         pay_method: "card", // 지불 수단
    //         merchant_uid: 'merchant_' + new Date().getTime(), //가맹점에서 구별할 수 있는 고유한id
    //         name: $('#showName').val(), // 상품명
    //         amount: parseInt($('#seatPrice').text()), // 가격
    //         buyer_email: "test@gmail.com",
    //         buyer_name: "tester", // 구매자 이름
    //         buyer_tel: "010-4242-4242", // 구매자 연락처 
    //         buyer_addr: "서울특별시 강남구 신사동", // 구매자 주소지
    //         buyer_postcode: "01181", // 구매자 우편번호
    //     }, function (rsp) { // callback
    //         if (rsp.success) {
    //             alert("결제성공")

    //             var imp_uid = rsp.imp_uid;
    //             var merchant_uid = rsp.merchant_uid;
    //             var access_token = '6fd59f6a98319493e0b64a6886650f9adefd522a';


    //             // 환불 요청 보내기
    //             $.ajax({
    //                 url: 'https://api.iamport.kr/payments/cancel',
    //                 type: 'post',
    //                 headers: {
    //                     "Authorization": "Bearer {" + access_token + "}"
    //                 },
    //                 data: JSON.stringify({
    //                     "imp_uid": imp_uid,
    //                     "merchant_uid": merchant_uid,
    //                     "reason": '고객의 변심으로 취소합니다.'
    //                 }),
    //                 contentType: 'application/json',
    //                 dataType: 'text',
    //                 success: function (result) {
    //                     alert('환불 처리가 완료되었습니다.');
    //                     location.href = "http://localhost:8081/boot/show/showList";
    //                 },
    //                 error: function () {
    //                     alert('환불 처리에 실패하였습니다.');
    //                 }
    //             });

    //         } else {
    //             alert("결제실패")
    //         }
    //     });

    // })



})