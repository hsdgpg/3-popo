/**
 * 
 */

$(document).ready(function () {

    // paging 관련.
    var actionForm2 = $('#actionForm2');
  
    $('.col-lg-12 a').on('click', function (e) {
      e.preventDefault();
      actionForm2.find("input[name='pageNum']").val($(this).attr('href'))
      actionForm2.submit();
  
    })
  
    // 검색버튼 관련.
    var searchForm2 = $("#searchForm2");
  
    $("#searchForm2 button").on('click', function (e) {
      
      searchForm2.find("input[name='pageNum']").val("1");
  
      e.preventDefault();
  
      searchForm2.submit();
    });
    })