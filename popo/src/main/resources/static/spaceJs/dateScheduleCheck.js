// 날짜 선택시 스케줄 체크

// list.rentalStartTime ~ list.rentalEndTime 사이값과 같은 값들 css로 hidden 처리
// style="visibility : hidden"

let stLiHidden = [];
let edLiHidden = [];
let rentalList = [];
let todaydate = 0;

let startTimeSelc = 0;
let cost = 0;

var rentalSt = '';
var rentalEnd = '';

// 이전에 displaynone 시켰던 li번호 저장


// 날짜 선택시의 동작
function dateScheduleCheck(dateSel, placeNo, placeCost) {
    $.ajax({
        url: "/boot/page/spaceInfo/dateScheduleCheck",
        type: 'post',
        data: {
            dateSel: dateSel,
            placeNo: placeNo,
            placeCost : placeCost
        },
        success: function (list) {
            rentalList = list;
            todaydate = dateSel;
            cost = placeCost
            
            let now = new Date(); // 현재 시간정보            
            let year = now.getFullYear(); // 년도
            let month = now.getMonth() + 1; // 월
            let date = now.getDate(); // 날짜
            let hours = now.getHours(); // 시간
            let today = year + '-' + (month <= 9 ? "0" + month : month) + '-' + (date <= 9 ? "0" + date : date); // YYYY-MM-DD

            console.log(today);

            $('.payAmount').empty(); // 날짜 선택시 결제 금액 초기화
            $('.ulT1').css("display", "inline-block"); // 날짜 선택시 시작시간 선택 활성화
            $('.ulT2').css("display", "none"); // 날짜 재선택시 종료시간 선택 비활성화

            if (stLiHidden != null) { // 배열에 값 있으면 실행
                for (i = 0; i < stLiHidden.length; i++) {
                    $('li.st' + stLiHidden[i]).css("display", "inline-block"); // display none들 다시 복구
                }
                stLiHidden = []; // 다썼으니 다시 비우자
            }

            if (today == dateSel) { // 선택한 날짜와 시스템상 현재 날짜 비교해서 같으면 아래 실행
                for (i = 0; i <= hours; i++) { // 현재 시간 이전의 시작시간은 선택불가하도록 비활성화
                    $('li.st' + i).css("display", "none");
                    stLiHidden.push(i);
                }
            }


            if (rentalList.length != 0) { // 받아온 예약내역 있으면 실행
                for (a = 0; a < rentalList.length; a++) {

                    let start = rentalList[a].rentalStartTime.substr(11, 2); // 대여시작시간
                    let end = rentalList[a].rentalEndTime.substr(11, 2); // 대여종료시간


                    for (i = 0; i <= 24; i++) { // 0시부터 24시까지
                        if (i >= start && i < end) { // i가 start랑 end 사이일때 ( 이미 대여한 시간일때 )

                            $('li.st' + i).css("display", "none"); // style="visibility : hidden"
                            $('li.ed' + i + 1).css("display", "none");

                            stLiHidden.push(i); // displaynone 시킨 번호 저장 
                            edLiHidden.push(i);
                        }
                    }
                }
            }

        },
        error: function () {
            console.log("망했어요");
        }

    })

}


//시작시간 선택시의 동작
function StartScheduleCheck(stTime) {
    startTimeSelc = stTime.substr(0, 2);
    let now = new Date(); // 현재 시간정보            
    let year = now.getFullYear(); // 년도
    let month = now.getMonth() + 1; // 월
    let date = now.getDate(); // 날짜
    let hours = now.getHours(); // 시간
    let today = year + '-' + (month < 9 ? "0" + month : month) + '-' + date; // YYYY-MM-DD

    rentalSt = todaydate +' '+ stTime;
    // $('#payment').before('<input type="hidden" th:value="'+rentalSt+'" id="rentalStart">');
    $('#rentalStart').attr('value',rentalSt);


    if (edLiHidden != null) { // 배열에 값 있으면 실행
        for (i = 0; i < edLiHidden.length; i++) {
            $('li.ed' + edLiHidden[i]).css("display", "inline-block"); // display none들 다시 복구
        }
        edLiHidden = []; // 다썼으니 다시 비우자
    }


    $('.ulT2').css("display", "inline-block"); // 시작시간 클릭 시 displaynone 상태였던 종료시간ul inlineblock으로 변경

    if (today == todaydate) { // 선택한 날짜와 시스템상 현재 날짜 비교해서 같으면 아래 실행
        for (i = 0; i <= hours; i++) { // 현재 시간 이전의 시작시간은 선택불가하도록 비활성화
            $('li.ed' + i).css("display", "none");
            edLiHidden.push(i);
        }
    }

    // 선택한 시작시간 이전시간은 모두 지운다
    for (i = 0; i <= 24; i++) { // 0시부터 24시까지
        if (i <= startTimeSelc) {
            $('li.ed' + i).css("display", "none"); // style="visibility : hidden" 
            edLiHidden.push(i); // displaynone 시킨 번호 저장                         
        }
    }

    if (rentalList.length == 0) { // 이전에 대여 기록이 없을때

        $('li.ed' + startTimeSelc).css("display", "none")

    } else { // 대여기록 존재할때

        for (a = 0; a < rentalList.length; a++) {

            let start = rentalList[a].rentalStartTime.substr(11, 2); // 이전대여시작시간  

            // 선택한 시작시간 이후 대여한 시간 있으면 시작시간과 연속되지 않으므로 그 이후의 선택지 지우기
            if (startTimeSelc <= start) {

                for (let i = 24; i > start; i--) {
                    $('li.ed' + i).css("display", "none")
                    edLiHidden.push(i);
                }
            }

        }

    }
}


function calculation(endTime) {

    rentalEnd = todaydate +' '+ endTime;
    // $('#payment').before('<input type="hidden" th:value="'+rentalEnd+'" id="rentalEnd">');
    
    $('#rentalEnd').attr('value',rentalEnd);
    console.log(rentalEnd);

    let pay = endTime.substr(0, 2) - startTimeSelc;
    console.log(pay);
    $('.payAmount').empty();
    $('.payAmount').append(pay*cost);



}