(function( $ ){
    $.fn.timeselect1 = function( options ){

        this.addClass('timeselect1');

        var settings = $.extend({
            format: "12h", // 12h or 24h
            interval: 30, // in minutes
            startHour: 10,
            endHour: 22,
            hideSeconds:true,
            showSeparator:true
        }, options);

        var times = [];

        for(var i = settings.startHour; i <= settings.endHour; i++)
        {
            var mode = '';//AM

            if(i > 11) {
                mode = 'PM';
            }

            var time1 = i;

            if(settings.format == '12h'){
                if(time1 > 12){
                    time1 = time1 - 12;
                }
            }

            for(var j = 0; j < 60; j+= settings.interval){
                var fulltime = ('00'+time1).substr(-2,2) + ':'+('00'+j).substr(-2,2);

                if(settings.hideSeconds === false) fulltime += ':00';

                if(settings.format == '12h') fulltime += ' '+mode;

                if(i != settings.endHour) {
                    times.push(fulltime);
                }else{
                    if(j == 0) times.push(fulltime);
                }
            }
        }

        var template = '<input class="inputT" id="start" type="text" name="timeselect1" oninput="start()" value=""/><ul class="ulT1" style="display:none">';

        var morningSeparator = false;
        for(var i = 0; i < times.length; i++)
        {
            if(settings.showSeparator === true) {
                var t = times[i].split(':');

                if (t[0] < 12 && (t[1].trim() == "00 AM" || t[1].trim() == "00") && morningSeparator == false) template += '<li class="separator">Morning<hr/></li>'; morningSeparator = true;
                if (t[0] == 12 && (t[1].trim() == "00 PM" || t[1].trim() == "00")) template += '<li class="separator">Afteroon<hr/></li>';
                if (t[0] == 6 && (t[1].trim() == "00 PM" || t[1].trim() == "00")) template += '<li class="separator">Evening<hr/></li>';
            }

            template += '<li class="st'+times[i].substring(0,2)+'" data-time1="'+times[i]+'" onclick=StartScheduleCheck("'+times[i]+'")>'+times[i]+'</li>';
        }

        template += '<br class="clr"/></ul>';
        this.append(template);

        // $('input[name=timeselect]').focus(function(){
        //     $('ul').show();
        // });

        $('li').click(function(){
            $('input[name=timeselect1]').val($(this).data('time1'));
        })

        // $(document).mouseup(function(e){
        //     var container = $('#'+$(this).find('.timeselect').prop('id'));
        //     if (!container.is(e.target) && container.has(e.target).length === 0){
        //         $("ul").hide();
        //     }
        // });


        return this;
    };

    $.fn.timeselect2 = function( options ){

        this.addClass('timeselect2');

        var settings = $.extend({
            format: "12h", // 12h or 24h
            interval: 30, // in minutes
            startHour: 10,
            endHour: 22,
            hideSeconds:true,
            showSeparator:true
        }, options);

        var times = [];

        for(var i = settings.startHour; i <= settings.endHour; i++)
        {
            var mode = '';//AM

            if(i > 11) {
                mode = 'PM';
            }

            var time = i;

            if(settings.format == '12h'){
                if(time > 12){
                    time = time - 12;
                }
            }

            for(var j = 0; j < 60; j+= settings.interval){
                var fulltime = ('00'+time).substr(-2,2) + ':'+('00'+j).substr(-2,2);

                if(settings.hideSeconds === false) fulltime += ':00';

                if(settings.format == '12h') fulltime += ' '+mode;

                if(i != settings.endHour) {
                    times.push(fulltime);
                }else{
                    if(j == 0) times.push(fulltime);
                }
            }
        }

        var template = '<input class="inputT" id="end" type="text" name="timeselect2" oninput="end()" value=""/><ul class="ulT2" style="display:none">';

        var morningSeparator = false;
        for(var i = 0; i < times.length; i++)
        {
            if(settings.showSeparator === true) {
                var t = times[i].split(':');

                if (t[0] < 12 && (t[1].trim() == "00 AM" || t[1].trim() == "00") && morningSeparator == false) template += '<li class="separator">Morning<hr/></li>'; morningSeparator = true;
                if (t[0] == 12 && (t[1].trim() == "00 PM" || t[1].trim() == "00")) template += '<li class="separator">Afteroon<hr/></li>';
                if (t[0] == 6 && (t[1].trim() == "00 PM" || t[1].trim() == "00")) template += '<li class="separator">Evening<hr/></li>';
            }

            template += '<li class="ed'+times[i].substring(0,2)+'" data-time="'+times[i]+'" onclick=calculation("'+times[i]+'")>'+times[i]+'</li>';
        }

        template += '<br class="clr"/></ul>';
        this.append(template);

        // $('input[name=timeselect]').focus(function(){
        //     $('ul').show();
        // });

        $('li').click(function(){
            $('input[name=timeselect2]').val($(this).data('time'));
        })

        // $(document).mouseup(function(e){
        //     var container = $('#'+$(this).find('.timeselect').prop('id'));
        //     if (!container.is(e.target) && container.has(e.target).length === 0){
        //         $("ul").hide();
        //     }
        // });


        return this;
    };
}( jQuery ));


function start(){
    var start = $('#start').val(); //id값이 "id"인 입력란의 값을 저장
    $.ajax({
        url:"", //Controller에서 요청 받을 주소
        type:'post', //POST 방식으로 전달
        data:{start:start},
        success:function(){ //컨트롤러에서 넘어온 cnt값을 받는다 
            console.log(start.val());
        },
        error:function(){
            $('.id_ok').css("display", "none");
        }
    });
};

function end(){
    var end = $('#end').val(); //id값이 "id"인 입력란의 값을 저장
    $.ajax({
        url:"/boot/page/join/idCheck", //Controller에서 요청 받을 주소
        type:'post', //POST 방식으로 전달
        data:{memId:memId},
        success:function(){ //컨트롤러에서 넘어온 cnt값을 받는다 
            console.log(end.val());
        },
        error:function(){
            
        }
    });
};