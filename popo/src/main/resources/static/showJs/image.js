/**
 * 
 */

$(document).ready(function(){
		// 파일크기지정 , 확장자제한
		var regex = new RegExp("(.*?)\(exe|sh|zip|alz)$");
		var maxSize = 5 * 1024 * 1024; //5MB
		
		$('#uploadBtn').on('click',function(e){
			
			var formData = new FormData(); // 객체: form활용 같은 기능
			
			var inputFile = $('input[name="imageName"]')[0];
			console.log(inputFile);
			var files = inputFile.files; // [0,1]
			
				if(!checkExtension(files.name, files.size)){
					return false;
				}
				formData.append("imageName", files);
			
			// Ajax 활용 파일 전송
			$.ajax({
				url : '/boot/show/uploadImage',
				type : 'post',
				data : formData, // file전송 : multipart/form-data, application/x-www-form-urlencoded : key=value$key=value
				contentType : false,
				processData : false,
				dataType : 'json', //결과값의 포맷 지정
				success : function(result){
					console.log(result);
										
				},
				error : function(err){
					console.log(err);
				}
			});
			
			//파일체크
			function checkExtension(fileName, fileSize){
				if(fileSize > maxSize){
					alert('파일사이즈 초과');
					return false;
				}
				if(regex.test(fileName)){
					alert('해당 없음');
					return false;
				}
				return true;
			}
		})
 
    })