
$(document).ready(function () {

  // 좌석 열 값
  let arr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'N', 'M', 'O'];
  
  // 총 금액
  var amountPrice = 0;
    
  
  /*---------------------------------------
    // 좌석초기화 
    --------------------------------------*/
  seatInit();
  
  function seatInit() {
    
    // 날짜 
    let val2 = $('#date').text();
    $('#showDate').text(val2);
  
    // 시간 
    let val3 = $('#time').text();
    $('#showTime').text(val3);
  
    // 좌석 알파벳 지정
    for (let i = 0; i < arr.length; i++) {
      $('.ticketing tr:eq(' + i + ') td:eq(0)').css('color', 'black').text(arr[i]);
    }
  
   /*------------------------------------
    // 알파벳 자리, 통로 자리 셋팅 
    --------------------------------------*/
    for (let i = 0; i < 15; i++) {
  
      $('.ticketing tr:eq(' + i + ') td:eq(0)').css('background-color', 'white').css('border-top', '1px solid white');
      $('.ticketing tr:eq(' + i + ') td:eq(0)').css('background-color', 'white').css('border-bottom', '1px solid white');
      $('.ticketing tr:eq(' + i + ') td:eq(0)').css('background-color', 'white').css('border-left', '1px solid white');
  
      $('.ticketing tr:eq(' + i + ') td:eq(3)').css('background-color', 'white').css('border-top', '1px solid white');
      $('.ticketing tr:eq(' + i + ') td:eq(3)').css('background-color', 'white').css('border-bottom', '1px solid white');
      $('.ticketing tr:eq(' + i + ') td:eq(12)').css('background-color', 'white').css('border-top', '1px solid white');
      $('.ticketing tr:eq(' + i + ') td:eq(12)').css('background-color', 'white').css('border-bottom', '1px solid white');
    
    }
  
  
    // S석 좌석 지정
    $(".ticketing").find("tr:lt(3)").find("td:gt(3):lt(8)").addClass('default03');
    $(".ticketing").find("tr:lt(3)").find("td:gt(0):lt(2)").addClass('default03');
    $(".ticketing").find("tr:lt(3)").find("td:gt(12):lt(2)").addClass('default03');
  
    // A석 좌석 지정
    $(".ticketing").find("tr:gt(2):lt(6)").find("td:gt(3):lt(8)").addClass('default04');
    $(".ticketing").find("tr:gt(2):lt(6)").find("td:gt(0):lt(2)").addClass('default04');
    $(".ticketing").find("tr:gt(2):lt(6)").find("td:gt(12):lt(2)").addClass('default04');
  
    // B석 좌석 지정
    $(".ticketing").find("tr:gt(8)").find("td:gt(3):lt(8)").addClass('default02');
    $(".ticketing").find("tr:gt(8)").find("td:gt(0):lt(2)").addClass('default02');
    $(".ticketing").find("tr:gt(8)").find("td:gt(12):lt(2)").addClass('default02');
  }
  
  
  
  /*--------------------------------------------------------------------------------------------
    좌석에따라 함수 실행하기 -> hasClass로 클래스 포함 여부를 확인후 실행한다.
    --------------------------------------------------------------------------------------------*/
    for (let i = 0; i < 15; i++) {
      for( let k=0; k < 15; k++) {
        
        if($('.ticketing tr:eq(' + i + ') td:eq(' + k + ')').hasClass("default02") == true) {
        // B 좌석
        reserv(i,k, '#bPrice', 'default02')
        } else if($('.ticketing tr:eq(' + i + ') td:eq(' + k + ')').hasClass("default03") == true){
  
           // S 좌석
        reserv(i,k,'#sPrice', 'default03')
  
        } else if($('.ticketing tr:eq(' + i + ') td:eq(' + k + ')').hasClass("default04") == true) {
  
           // A 좌석
        reserv(i,k, '#aPrice', 'default04')
  
        }
       
         // 결제된 좌석은 재선택 불가
         preventRerv(i,k);
  
      }
    }
  
  /*--------------------------------------------------------------------------------------------
    좌석 선택 or 해제, 가격 +- , 결제된 좌석 선택불가, 선택인원 초과시 추가 선택 불가
    --------------------------------------------------------------------------------------------*/
    // 매개변수 -> 좌석 찾는값, 가격, 좌석 클래스
    function reserv(i,k, price, className){

      // 해당 좌석 변수에 값 저장
          var td=$('.ticketing tr:eq(' + i + ') td:eq(' + k + ')')
      
          td.on('click', function() {
            console.log('click');
            // 클릭한 좌석이 선택된 좌석이 아닐시 실행          
            if(td.hasClass('clicked') == false) {
              //인원수 초과일때 조건  ( 선택된 인원수와 선택된 좌석이 같으면 더이상 선택못함, 선택된 인원수가 0 미만이어도 선택못함)          
              if( parseInt($('#pNum').val()) == $('.clicked').length || parseInt($('#pNum').val()) < 0 ){
                Swal.fire({
                icon: 'error',                      
                title: '인원초과',         
            });
                return
              }
  
              // 결제된 좌석의 클래스가 아닐 경우 금액이 들어감.
              if(td.hasClass('default01') == false) {
  
              amountPrice += parseInt($(price).text()); 
              $('#seatPrice').text(amountPrice);
            }
  
            //좌석해제  
            } else { 
              
              // 좌석이 선택된 상태에서 다시 클릭시 해당 좌석의 금액이 빠짐.
              amountPrice -= parseInt($(price).text()); 
              
              $('#seatPrice').text(amountPrice);
            }  // end of if 좌석해제
  
            // 결제된 좌석이 아닐 경우 실행
            if(td.hasClass('default01') == false) {
  
            // toggleClass를 사용하여 클릭시 좌석이 선택되고 해제됨(색깔이 변함)
            td.toggleClass('clicked'+ ' ' + className)
  
          }
          // 선택시 좌석의 값이 들어가고, 좌석선택 해제시 좌석의 값이 빠진다.
            seatSelectFNC();
          }) // end of on click
            
          
        }  
    
    /*-------------------------------------
    // 예약좌석 선택불가
    --------------------------------------*/
  function preventRerv(i,k){

    // 알파벳과 숫자가 합쳐진 좌석의 이름.
    var seatName = ($('.ticketing tr:eq(' + i + ') td:eq(0)').text() + $('.ticketing tr:eq(' + i + ') td:eq(' + k + ')').text());
  
    // DB에 저장된 선택된 좌석의 값과 클릭한 좌석의 값을 indexOf 함수를 사용해 비교하여 0 이상의 숫자를 반환할 경우
    // 기존에 설정된 클래스를 모두 지우고 선택된 좌석의 클래스를 추가한다. ( 재선택 불가능하게 막음 )
    if(forbidSeat.indexOf(seatName) >= 0 ) {
      $('.ticketing tr:eq(' + i + ') td:eq(' + k + ')').removeClass('default02 default03 default04').addClass('default01');
    
    }
  }
  
  /*------------------------------------
     선택된 좌석 값 넣기,빼기  
    --------------------------------------*/
  function seatSelectFNC (){
  
    var spanTag = '';
    var seatSelect = $('#seatSelect')
  
    // 선택된 좌석의 클래스로 반복문을 사용해 좌석이 선택될때마다 좌석값을 누적으로 더해서 출력한다.
    $('.clicked').each(function(i,item) {
  
        spanTag += $(item).parent().children().first().text()
                +  $(item).text() + ',';
  
    }) 
    seatSelect.text(spanTag);
      
  }
      
  /*--------------------------------------
      총 인원수  
    --------------------------------------*/
  $('#pNum').on('change', function () {
    
    // 인원수를 음수값으로 선택 불가
    if( $('#pNum').val() < 0) {
        
        $('#pNum').val(0);

    }

    // 선택한 좌석 수보다 인원수가 작은값이 들어갈 수 없다.
    if( parseInt($('#pNum').val()) < $('.clicked').length ) {

      $('#pNum').val($('.clicked').length);

    }
    
    $('#pNum2').text($('#pNum').val());
  
  })
  
  
  /*--------------------------------------
      결제 대기내역 추가(장바구니 담기)  
    --------------------------------------*/
    $('#waitingPayment').on('click', function (e) {
  
      e.preventDefault();
  
      // 장바구니 버튼을 눌렀을때 인원수가 0 이하이거나 좌석 가격이 0이하이면 alert창을 띄움
      if( $('#pNum').val() <= 0 || Number($('#seatPrice').text()) <= 0) {

        Swal.fire({
          icon: 'error',                      
          title: '결제정보를 입력해 주세요.',         
      });
        return;
  
      }
  
      $.ajax({
        url:'/boot/payment/waitingPayment',
        type:'post',
        data: JSON.stringify({
            "showNo" : $('#showNo').val(),
            "showDate"  :  new Date($('#date').text()),
            "amountPrice" :  parseInt($('#seatPrice').text()),
            "peopleNum" :  $('#pNum').val(),
            "seatNo" : $('#seatSelect').text(),
            "memNo" : $('#memNo').val(),
            "payStatus" : 0
        }),
        contentType : 'application/json',
        dataType : 'text',
        success: function(result){
            
          if(result == 'success'){
          Swal.fire({
            icon: 'success',                      
            title: '장바구니에 넣었을 경우 10분안에 결제가 되지 않으면 자동으로 취소처리 됩니다. 결제 취소시간은 마이페이지 결제내역에서 확인가능합니다.',         
        });


          location.href = "/boot/show/showList"
            }
            
        },
        error: function(){
          Swal.fire({
            icon: 'error',                      
            title: '통신 실패.',         
        });
        }
      });
      
      })
  
  
  
  // 결제
    $('#payment').on('click', function (e) {
  
      e.preventDefault();
  
    // 장바구니 버튼을 눌렀을때 인원수가 0 이하이거나 좌석 가격이 0이하이면 alert창을 띄움
      if( $('#pNum').val() <= 0 || Number($('#seatPrice').text()) <= 0) {
     
        Swal.fire({
          icon: 'error',                      
          title: '결제정보를 입력해 주세요.',         
      });
        return;
  
      }
  
      var IMP = window.IMP;
      IMP.init("imp20852873"); // "iamport" 대신 발급받은 "가맹점 식별코드"를 사용합니다.
  
      // IMP.request_pay(param, callback) 호출
      IMP.request_pay({ // param
        pg: "kakaopay", // PG사 선택
        pay_method: "kakaopay", // 지불 수단
        merchant_uid: 'merchant_' + new Date().getTime(), //가맹점에서 구별할 수 있는 고유한id
        name: $('#showName').val(), // 상품명
        amount: parseInt($('#seatPrice').text()), // 가격
        buyer_email: $('#memEmail').val(), // 구매자 이메일
        buyer_name: $('#memName').val(), // 구매자 이름
        buyer_tel: $('#memTel').val(), // 구매자 연락처 
        buyer_addr: $('#memAddress').val(), // 구매자 주소지
      }, function (rsp) { // callback
        if (rsp.success) {
          Swal.fire({
            icon: 'success',                      
            title: '결제 성공.',         
        });
  
          var impUid = rsp.imp_uid;
          var merchantUid = rsp.merchant_uid; 
          console.log(merchantUid);
          
          $.ajax({
            url:'/boot/show/showPayment',
            type:'post',
            data: JSON.stringify({
              "showNo" : $('#showNo').val(),
              "showDate"  :  new Date($('#date').text()),
              "amountPrice" :  parseInt($('#seatPrice').text()),
              "peopleNum" :  $('#pNum').val(),
              "seatNo" : $('#seatSelect').text(),
              "memNo" : $('#memNo').val(),
              "payStatus" : 1,
              "teamNo" : $('#teamNo').val(),
              "teamNum" : $('#teamNum').val(),
              "merchantUid" : merchantUid,
              "impUid" : impUid
              
        
          }),
            contentType : 'application/json',
            dataType : 'text',
            success: function(result){
                
              if(result == 'success'){                        
  
              location.href = "/boot/show/showList"
                }
                
            },
            error: function(){
              Swal.fire({
                icon: 'error',                      
                title: '통신 실패.',         
            });
            }
        });
  
        } else {
          Swal.fire({
            icon: 'error',                      
            title: '결제 실패.',         
        });
        }
      });
  
    })
  
  })