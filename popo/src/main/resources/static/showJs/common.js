/**
 * 
 */
 
 
$(document).ready(function () {

  // paging 관련.
  var actionForm = $('#actionForm');

  $('.col-lg-12 a').on('click', function (e) {
    e.preventDefault();
    actionForm.find("input[name='pageNum']").val($(this).attr('href'))
    actionForm.submit();

  })

  // 검색버튼 관련.
  var searchForm = $("#searchForm");

  $("#searchForm button").on('click', function (e) {



    if (!searchForm.find("input[name='keyword']").val()) {
      alert("키워드를 입력하세요");
      return false;
    }

    searchForm.find("input[name='pageNum']").val("1");

    e.preventDefault();

    searchForm.submit();
  });
  })