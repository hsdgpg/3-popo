$(document).ready(function() {

  var radioVal ='';
  $('#payment').click(function(){
    radioVal = $('input[name="shop"]:checked').val();
    return
  })




   $('#payment').on('click', function (e) {

    e.preventDefault();

    if(radioVal == null || radioVal==''){
      alert("금액을 선택해주세요")
      return;
    }
    
    var IMP = window.IMP;
    IMP.init("imp20852873"); // "iamport" 대신 발급받은 "가맹점 식별코드"를 사용합니다.

    // IMP.request_pay(param, callback) 호출
    IMP.request_pay({ // param
      pg: "kakaopay", // PG사 선택
      pay_method: "kakaopay", // 지불 수단
      merchant_uid: 'merchant_' + new Date().getTime(), //가맹점에서 구별할 수 있는 고유한id
      name: $('#fundingName').text(), // 상품명
      amount: radioVal, // 가격
      buyer_email: "test@gmail.com",
      buyer_name: "tester", // 구매자 이름
      buyer_tel: "010-0000-0000", // 구매자 연락처 
      buyer_addr: "서울특별시 강남구 신사동", // 구매자 주소지
      buyer_postcode: "01181", // 구매자 우편번호
    }, function (rsp) { // callback
      if (rsp.success) {
        alert("결제성공")
        
        var impUid = rsp.imp_uid;
        var merchantUid = rsp.merchant_uid;
     
        $.ajax({
          url:'/boot/funding/fundingPayment',
          type:'post',
          data: JSON.stringify({
              "fundingNo" : parseInt($('#fundingNo').text()),
              "memNo"  : $('#memNo').val(),
              "fundingPrice" : $('input[name="shop"]:checked').val(),
              "teamNo" : $('#teamNo').val(),
              "teamNum" : $('#teamNum').val(),
              "merchantUid" : merchantUid,
              "impUid" : impUid
          }),
          contentType : 'application/json',
          dataType : 'text',
          success: function(result){
              
            if(result == 'success'){
            alert('통신 성공'); 
                      
            location.href = "/boot/funding/fundingList"
              }
              
          },
          error: function(){
              alert('통신 실패');
          }
      });

      } else {
        alert("결제실패")
      }
    });

  })
})