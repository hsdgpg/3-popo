/**
 * 
 */

function position() {

    console.log('position test');
}

window.onload = function() {

        function previewImage() {
            var preview = document.getElementById('preview');
            var file = document.getElementById('profile-img').files[0];
            
            var reader = new FileReader();
            reader.onloadend = function() {
                preview.src = reader.result;
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }

        var profileImgInput = document.getElementById('profile-img');
        var previewImg = document.getElementById('preview-img');
		
		//FileReader객체 생성
        profileImgInput.addEventListener('change', function(event) {
            var file = event.target.files[0];
            var reader = new FileReader();

            reader.addEventListener('load', function() {
                previewImg.src = reader.result;
                previewImg.style.display = 'block';
            });

            if (file) {
                reader.readAsDataURL(file);
            }
        });
        
        
}



function TeamOut(memNo){
    confirm1('', '해당 팀원을 탈퇴시키겠습니까??',memNo);

}


var confirm1 = function(msg, title, memNo) {
    swal({
        title : title,
        text : msg,
        type : "warning",
        showCancelButton : true,
        confirmButtonClass : "btn-danger",
        confirmButtonText : "예",
        cancelButtonText : "아니오",
        closeOnConfirm : false,
        closeOnCancel : true
    }, function(isConfirm) {
        if (isConfirm) {
            deleteTeamMember(memNo);
        }else{
            swal.close();
        }

    });
}


function deleteTeamMember(memNo){
    $.ajax({
                    url : '/boot/teampage/deleteTeamMember',
                    type : 'post',
                    data : {
                        "memNo" : memNo
                    },
                    success : function() {
                        
                         Swal.fire('탈퇴 완료되었습니다.', '감사합니다.', 'success');
                         
                         //swal('', '해당 팀원을 탈퇴시켰습니다.', "success");
                         location.reload();
                    },
                    error : function() {
                        alert('ajax 댓글달기 실패');
                        console.log('실패')
                    }
                })
}

    
function TeamOut1(memNo){
    Swal.fire({
           title: '정말로 팀원을 탈퇴시키겠습니까?',
           text: '다시 되돌릴 수 없습니다. 신중하세요.',
           icon: 'warning',
           
           showCancelButton: true, // cancel버튼 보이기. 기본은 원래 없음
           confirmButtonColor: '#3085d6', // confrim 버튼 색깔 지정
           cancelButtonColor: '#d33', // cancel 버튼 색깔 지정
           confirmButtonText: '승인', // confirm 버튼 텍스트 지정
           cancelButtonText: '취소', // cancel 버튼 텍스트 지정
           
           reverseButtons: true, // 버튼 순서 거꾸로
           
        }).then(result => {
           // 만약 Promise리턴을 받으면,
           if (result.isConfirmed) { // 만약 모달창에서 confirm 버튼을 눌렀다면
               deleteTeamMember(memNo);
               
           }
           
        });
}
