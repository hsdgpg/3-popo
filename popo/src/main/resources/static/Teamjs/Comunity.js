/**
 * 팀 히스토리 소통
 */

 $(document).ready(function () {

        // paging 관련.
        var actionForm = $('#actionForm');

        $('.paging').on('click', function (e) {
        e.preventDefault();
        actionForm.find("input[name='pageNum']").val($(this).attr('href'))
        actionForm.submit();

        })

       
      noticeList();

      })// $(document).ready 끝
    
    
    



 
        $(".heart-click").on('click',function(){
            var memNo = $('#teamLike').val();
            var emptyHeart = $('.fa-regular fa-heart');
            var fullHeart = $('.fa-solid fa-heart');
            //var no = 1;
            //비회원일때
            console.log("memNo"+memNo)

            if(memNo<0){
                alert('로그인 후 이용할 수 있습니다.');
            }

                 //빈하트 눌렀을때
                if( $(this).children('i').attr('class') == "fa-regular fa-heart" ){
                    //JSON.stringify()
                    $.ajax({
                        url:'/boot/team/teamLike',
                        type:'post',
                        contentType: false,
                        processData: false,
                        success: function(result){
                            $('.fa-regular.fa-heart').attr('class','fa-solid fa-heart');
                           
                        },
                        error: function(){
                            alert('ajax 실패');
                        }
                    });
                    
            
            //꽉찬 하트 눌렀을때
            //$(this).children('i').attr('class') == "fa-solid fa-heart"
            }else if($(this).children('i').attr('class') == "fa-solid fa-heart"){

                var no = 0;


                $.ajax({
                        url:'/boot/team/teamUnLike',
                        type:'post',
                        contentType: false,
                        processData: false,
                        success: function(result){
                            $('.fa-solid.fa-heart').attr('class','fa-regular fa-heart');
                               
                        },
                        error: function(){
                            alert('ajax 실패');
                        }
                
            })

        }
            
        });

        //로그인을 하지 않은 상태에서 하트 클릭하면 로그인 해야한다는 알림창 띄우기
        



 // paging 관련.
 var actionForm = $('#actionForm');
 var currentPageNum = $('.currentPageNum').val();

 function replyPaging(){

   console.log('페이징 가보자고.');

   $.ajax({
       url:'/boot/team/communicate3',
       type:'post',
       data:{
           "currentPageNum" : currentPageNum,
           "pageNum" :num
       },
       success: function(result){
       
               noticeList();

       },
       error: function(){
           alert('ajax 댓글페이징 실패');
           console.log('실패')
       }
       })
   
   }
   // })


 //댓글리스트
 function noticeList(){
          
    //console.log('ajax 전')
    var str = "";

    $.ajax({
    url:'/boot/team/communicate1',
    type:'post',
    data:{
        "teamNo" : 1
    },
    success: function(result){
    
        var mem = $('#memName1').val();
        var memRating = $('#memRating').val();
        if(result != null){

            $('.ShowReply').empty();
            //location.reload();
            $.each(result, function (index, item) {
                //날짜 포맷.
                var td=new Date(item.commentDate).toISOString().substring(0,10);	
                
               
                str += "<div class='ComuDiv'>";
                str += "<input type='hidden' value='"+item.memNo+"' name='itemMemNo' id='itemMemNo' class='itemMemNo'>";
                str += "<input type='hidden' value='"+item.commentNo+"' name='itemCommentNo' class='itemCommentNo'>";
                

                str += "<div style='display: flex; margin-left: 30px;'>";
                str += "<p style='width: 100px;'  >"+item.name+"</p>";
                str += "<p>"+td+"</p>";
                str += "</div>";
                str += "<div style='background-color: #eee; height: 80px; width: 100%;' class='contentArea'><br>";
                str += "<p style='margin-left: 60px;' class='replyContent' >"+item.commentContent+"</p></div><br> "; 
                //수정 버튼 클릭시 textarea show
              //   str += "<div><textarea style='width: 600px; display:none;' name='content1' class='content1' placeholder='"+item.commentContent+"'>"+item.commentContent+"</textarea></div>";
                str += "<div class='ComutextArea'><textarea style='width: 600px; display:none;' name='content1' class='content1' placeholder='"+item.commentContent+"'>"+item.commentContent+"</textarea></div>";
                if(mem==item.memNo){
                
                str += "<div style='justify-content: flex-end; margin-left: 30px; text-align:right ;'>";  
                str += "<input type='hidden' value='"+item.commentNo+"' class='itemCommentNo'>";
                str += "<a style='display: inline-block; margin-right: 5px;' id='ReplyMod11'>";
                str += "<button id='ReplyMod' class='ReplyMod' onclick='ButtonChange("+item.commentNo+",this)'>수정</button><button class='DeleteReply' onclick='deleteReply(this)'>삭제</button></a>";
                str += "<button  class='DoReplyMod' style='display:none;' onclick='modReply(this)'>확인</button>";
                str += "</div>";
                      
               }

               if(memRating=='MR04'){               
                    str += "<div style='justify-content: flex-end; margin-left: 30px; text-align:right ;'>";
                    str += "<button onclick='deleteReply1("+item.commentNo+")' style='background-color:white; border:none; '>X</button>";
                    str += "</div>";                                                
               }
               
            //    else{
            //     str += "<div style='justify-content: flex-end; margin-left: 30px; text-align:right ;'>";
            //     str += " <a style='display: inline-block; margin-right: 5px;'><button class='ReplyMod' id='AlertReply' onclick='reportReply(this)'>신고</button></a>";
            //     str += "</div>";
                  
            //    }

               
               //대댓글 구현되어야 하는 영역

                str += "</div>";
                
            })
          
               
           
            $('.ShowReply').append(str);
            
           
            }else{
                console.log('ajax 통신은 됬는데 실패')
            }
        
    },
    error: function(){
      
        console.log('통신실패')
    }
    })


}



function deleteReply1(commentNo){

    //var commentNo = $(btn).parent().parent().parent('div.ComuDiv').children().find('input.itemCommentNo').val();
    console.log(commentNo);       

    Swal.fire({
        title: '댓글 삭제',
        text: '해당 댓글을 삭제하시겠습니까? 되돌릴 수 없습니다.',
        icon: 'info',
        
        showCancelButton: true, // cancel버튼 보이기. 기본은 원래 없음
        confirmButtonColor: '#3085d6', // confrim 버튼 색깔 지정
        cancelButtonColor: '#d33', // cancel 버튼 색깔 지정
        confirmButtonText: '삭제', // confirm 버튼 텍스트 지정
        cancelButtonText: '확인', // cancel 버튼 텍스트 지정
        
        reverseButtons: true, // 버튼 순서 거꾸로
        
     }).then(result => {
        // 만약 Promise리턴을 받으면,
        if (result.isConfirmed) { // 만약 모달창에서 confirm 버튼을 눌렀다면
        
            $.ajax({
                url:'/boot/team/ComudeleteReply2',
                type:'post',
                data:{
                    "commentNo" :commentNo
                },
                success: function(result){
                
                    Swal.fire('삭제되었습니다.',  'success');
                    location.reload();											 
                   
                },
                error: function(){
                    alert('ajax 댓글달기 실패');
                    console.log('실패')
                }
                })
        
        }else if (result.isDismissed) { // 만약 모달창에서 cancel 버튼을 눌렀다면
             // ...실행
         }
     });
 


   
}



	//댓글 추가
	function addReply(){
	
	    $.ajax({
	    url:'/boot/team/ComuaddReply2',
	    type:'post',
	    data:{
	        "commentContent" : $('#content').val()
	    },
	    success: function(result){
	        //댓글 입력 완료 
	        if(result != null){
	            noticeList();
	            $('#content').replaceWith('<textarea placeholder="내용을 입력해 주세요" style="width: 600px;" name="content" id="content">');
	        }else {
	            alert('내용을 입력해주세요');
	        }
	    },
	    error: function(){
	        alert('ajax 댓글달기 실패');
	        console.log('실패')
	    }
	    })
	 
	 
	
	}



      //댓글 수정버튼 클릭시 버튼 변환
      function ButtonChange(commentNo,btn){

        //댓글내용 숨김, textarea show
        var textarea = $(this).parent().siblings().find('.ComutextArea textarea');

          //find
          $(btn).parent().parent().parent('div.ComuDiv').children('div.ComutextArea').children('textarea.content1').css('display','');
          $(btn).parent().parent().parent('div.ComuDiv').children('div.contentArea').css('display','none');
          $(btn).hide(); // 수정 버튼 숨기기
          $(btn).parent().find('.DeleteReply').hide(); // 삭제 버튼 숨기기

          $(btn).parent().siblings('button.DoReplyMod').css('display','');

    }

     
    function deleteReply(btn){
       
        var commentNo = $(btn).parent().parent().parent('div.ComuDiv').children().find('input.itemCommentNo').val();
       

        $.ajax({
        url:'/boot/team/ComudeleteReply',
        type:'post',
        data:{
            "commentNo" :commentNo
        },
        success: function(result){
        
            noticeList();    
  
        },
        error: function(){
            alert('ajax 댓글달기 실패');
            console.log('실패')
        }
        })

    }


      
    function modReply(btn){

        console.log('ModReply 함수 ajax 실행 전 ')
        
        console.log($(btn).parent().parent('div.ComuDiv').children('div.ComutextArea').children('textarea').val())
        
        
        console.log($(btn).parent().parent('div.ComuDiv').find('input.itemCommentNo').val())
        console.log( $(btn).parent().parent('div.ComuDiv').find('input.itemMemNo').val())

        var text = $(btn).parent().parent('div.ComuDiv').children('div.ComutextArea').children('textarea').val();
        var commentNo = $(btn).parent().parent('div.ComuDiv').find('input.itemCommentNo').val();
        var memNo = $(btn).parent().parent('div.ComuDiv').find('input.itemMemNo').val();
        
        //수정 AJAX
        $.ajax({
        url:'/boot/team/ComuModReply',
        type:'post',
        data:{
            
            "itemMemNo" :memNo,
            //"itemTeamNo" : $('#itemTeamNo').val(),
            "commentContent" :text,
            "commentNo" : commentNo
        },
        success: function(result){
              console.log('수정성공');
              noticeList();    
        },
        error: function(){
            alert('ajax 댓글달기 실패');
            console.log('실패')
        }
        })


        }//end of modReply()

        



