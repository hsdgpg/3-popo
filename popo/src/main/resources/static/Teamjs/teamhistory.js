/**
 * 
 */



    
    $(document).ready(function () {

    	  // paging 관련.
    	  var actionForm = $('#actionForm');

    	  $('.col-lg-12 a').on('click', function (e) {
    	    e.preventDefault();
    	    actionForm.find("input[name='pageNum']").val($(this).attr('href'))
    	    actionForm.submit();

    	  })

    })
    
 
        //팀 좋아요
        $(".heart-click").on('click',function(){
            var memNo = $('#teamLike').val();
            var emptyHeart = $('.fa-regular fa-heart');
            var fullHeart = $('.fa-solid fa-heart');
            //비회원인 경우
            if(memNo<0){
                alert('로그인 후 이용할 수 있습니다.');
            }
                 //좋아요
                if( $(this).children('i').attr('class') == "fa-regular fa-heart" ){
                    //JSON.stringify()
                    $.ajax({
                        url:'/boot/team/teamLike',
                        type:'post',
                        contentType: false,
                        processData: false,
                        success: function(result){
                            $('.fa-regular.fa-heart').attr('class','fa-solid fa-heart');
                        },
                        error: function(){
                            alert('ajax 실패');
                        }
                    });
            
	            //좋아요 해제
	            //$(this).children('i').attr('class') == "fa-solid fa-heart"
	            }else if($(this).children('i').attr('class') == "fa-solid fa-heart"){
	                var no = 0;
	                $.ajax({
	                        url:'/boot/team/teamUnLike',
	                        type:'post',
	                        contentType: false,
	                        processData: false,
	                        success: function(result){
	                            $('.fa-solid.fa-heart').attr('class','fa-regular fa-heart');
	                        },
	                        error: function(){
	                            alert('ajax 실패');
	                        }
	            })
	        }
	     });

      
