/**
 * 
 */

$(document).ready(
    function() {
        var replyNo = $('#Reply').data("no");
        $("#Reply").load('/boot/team/reply?noticeNo=' + replyNo,
                function() {
                    notice(1, replyNo);
                })
                
           
                
    })// $(document).ready 끝




const editor = new toastui.Editor({
el : document.querySelector('#viewer'),
initialEditType : "markdown",
initialValue : document.querySelector("#viewers").value
});

// paging 관련.
var actionForm = $('#actionForm');
var currentPageNum = $('.currentPageNum').val();



$('#reportInModal').on('click', function(){
    var noticeNo = $('#NoticeNo').val();
    var memNo = $('#memNo').val();
    var postReportContent = $('#postReportContent').val();
    var postReportReason = $('input.reportReason:checked').val();
    
    
     $.ajax({
         
         type: "post",
         url: "/boot/team/reportTeamNotice",
         data: {	"noticeNo" : noticeNo,
                     "memNo" : memNo,
                     "postReportReason" : postReportReason,
                      "postReportContent" : postReportContent},
         error : function(error){
             console.log(error);
         }
         
       });
     
     $('#reportModal').css('display', 'none');
     
    
})


$('#buttonList').on('click', function() {
    location.href = '/boot/team/teamNotice';
})

   
$('#reportBtn').on('click', function(){
     $('#reportModal').css('display', 'block');
     $('.label').css('margin', '0 auto');
     $('.label').css('display', 'flex');
     $('.label').css('margin-left', '220px');
     $('.textArea').css('display', 'flex');
     
})

$('#reportClose').on('click', function(){
     $('#reportModal').css('display', 'none');
})



 //하트 누를때 로그인 한 회원의 id를 전달해서 team_like db update 필요 
 $(".heart-click").on('click',function(){
    var memNo = $('#teamLike').val();
    var emptyHeart = $('.fa-regular fa-heart');
    var fullHeart = $('.fa-solid fa-heart');
    //var no = 1;
    //비회원일때
    console.log("memNo"+memNo)

    if(memNo<0){
        alert('로그인 후 이용할 수 있습니다.');
    }

         //빈하트 눌렀을때
        if( $(this).children('i').attr('class') == "fa-regular fa-heart" ){
            //JSON.stringify()
            $.ajax({
                url:'/boot/team/teamLike',
                type:'post',
                contentType: false,
                processData: false,
                success: function(result){
                    $('.fa-regular.fa-heart').attr('class','fa-solid fa-heart');
                   
                },
                error: function(){
                    alert('ajax 실패');
                }
            });
            
    
    //꽉찬 하트 눌렀을때
    //$(this).children('i').attr('class') == "fa-solid fa-heart"
    }else if($(this).children('i').attr('class') == "fa-solid fa-heart"){

        var no = 0;


        $.ajax({
                url:'/boot/team/teamUnLike',
                type:'post',
                contentType: false,
                processData: false,
                success: function(result){
                    $('.fa-solid.fa-heart').attr('class','fa-regular fa-heart');
                       
                },
                error: function(){
                    alert('ajax 실패');
                }
        
    })

}
    
});

//로그인을 하지 않은 상태에서 하트 클릭하면 로그인 해야한다는 알림창 띄우기



