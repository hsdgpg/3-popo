/**
 * 
 */


 //https://gimmesome.tistory.com/175
        //하트 누를때 로그인 한 회원의 id를 전달해서 team_like db update 필요 
        $(".heart-click").on('click',function(){
            var memNo = $('#teamLike').val();
            var emptyHeart = $('.fa-regular fa-heart');
            var fullHeart = $('.fa-solid fa-heart');
            //var no = 1;
            //비회원일때
            console.log("memNo"+memNo)

            if(memNo<0){
                alert('로그인 후 이용할 수 있습니다.');
            }

                 //빈하트 눌렀을때
                if( $(this).children('i').attr('class') == "fa-regular fa-heart" ){
                    //JSON.stringify()
                    $.ajax({
                        url:'/boot/team/teamLike',
                        type:'post',
                        contentType: false,
                        processData: false,
                        success: function(result){
                            $('.fa-regular.fa-heart').attr('class','fa-solid fa-heart');
                           
                        },
                        error: function(){
                            alert('ajax 실패');
                        }
                    });
                    
            
            //꽉찬 하트 눌렀을때
            //$(this).children('i').attr('class') == "fa-solid fa-heart"
            }else if($(this).children('i').attr('class') == "fa-solid fa-heart"){

                var no = 0;


                $.ajax({
                        url:'/boot/team/teamUnLike',
                        type:'post',
                        contentType: false,
                        processData: false,
                        success: function(result){
                            $('.fa-solid.fa-heart').attr('class','fa-regular fa-heart');
                               
                        },
                        error: function(){
                            alert('ajax 실패');
                        }
                
            })

        }
            
        });

        //로그인을 하지 않은 상태에서 하트 클릭하면 로그인 해야한다는 알림창 띄우기
        

        const onUploadImage = async (blob, callback) => {
            const formData = new FormData();
            formData.append("image", blob);
            formData.append("uri", window.location.pathname);
            
             $.ajax({
              type: "post",
              url: "/boot/page/image",
              async: true,
              data: formData,
              processData: false,
              contentType: false,
              success: function (data) {
                imageURL = data;
                callback(imageURL, "image");
                console.log(imageURL)
              },
              error: function (request, status, error) {
                alert(request + ", " + status + ", " + error);
              },
            });
          };
   
          const editor = new toastui.Editor({
            el: document.querySelector("#editor"),
            height: "500px",
            initialEditType: "wysiwyg",
            placeholder: "내용을 입력해주세요",
            language: "ko-kr",
            initialValue: document.querySelector("#content").value,
            hooks: {
              addImageBlobHook: onUploadImage
            }
          });
          
          
          var formObj = $('form[role="form"]')
           $('button[type="submit"]').on('click',function(e){
               e.preventDefault()//기본기능 차단
               console.log('submit click')
               console.log(editor.getHTML())
           var str = "<input type='hidden' name='noticeContent' value='"+editor.getHTML()+"'>"
           formObj.append(str).submit()
       })
          