/**
 * 
 */


$(document).ready(function () {
        
    // 모달창 숨기기
    $('#reportModal').css('display', 'none');
    $('.modal1').css('display', 'none');

  noticeList(1);



  })// $(document).ready 끝





 //하트 누를때 로그인 한 회원의 id를 전달해서 team_like db update 필요 
 $(".heart-click").on('click',function(){
    var memNo = $('#teamLike').val();
    var emptyHeart = $('.fa-regular fa-heart');
    var fullHeart = $('.fa-solid fa-heart');
    //var no = 1;
    //비회원일때
    console.log("memNo"+memNo)

    if(memNo<0){
        alert('로그인 후 이용할 수 있습니다.');
    }

         //빈하트 눌렀을때
        if( $(this).children('i').attr('class') == "fa-regular fa-heart" ){
            //JSON.stringify()
            $.ajax({
                url:'/boot/team/teamLike',
                type:'post',
                contentType: false,
                processData: false,
                success: function(result){
                    $('.fa-regular.fa-heart').attr('class','fa-solid fa-heart');
                   
                },
                error: function(){
                    alert('ajax 실패');
                }
            });
            
    
    //꽉찬 하트 눌렀을때
    //$(this).children('i').attr('class') == "fa-solid fa-heart"
    }else if($(this).children('i').attr('class') == "fa-solid fa-heart"){

        var no = 0;


        $.ajax({
                url:'/boot/team/teamUnLike',
                type:'post',
                contentType: false,
                processData: false,
                success: function(result){
                    $('.fa-solid.fa-heart').attr('class','fa-regular fa-heart');
                       
                },
                error: function(){
                    alert('ajax 실패');
                }
        
    })

}
    
});

//로그인을 하지 않은 상태에서 하트 클릭하면 로그인 해야한다는 알림창 띄우기


$('#reportInModal').on('click', function(){
    var noticeNo = $('#NoticeNo').val();
    var memNo = $('#memNo').val();
    var postReportContent = $('#postReportContent').val();
    var postReportReason = $('input.reportReason:checked').val();
    
    
     $.ajax({
         
         type: "post",
         url: "/boot/team/reportPractice",
         data: {	"noticeNo" : noticeNo,
                     "memNo" : memNo,
                     "postReportReason" : postReportReason,
                      "postReportContent" : postReportContent},
         success: function(){ alert('신고완료되었습니다.')},
         error : function(error){
             console.log(error);
         }
         
       });
     
     $('#reportModal').css('display', 'none');
     
    
})

$('#buttonList').on('click', function(){
    location.href='/boot/team/practice';
})

$('.button1').on('click', function(){
     $('#reportModal').css('display', 'block');
     $('.label').css('margin', '0 auto');
     $('.label').css('display', 'flex');
     $('.label').css('margin-left', '220px');
     $('.textArea').css('display', 'flex');
     
     
    
})

$('#reportClose').on('click', function(){
     $('#reportModal').css('display', 'none');
})




  const editor = new toastui.Editor({
                  el: document.querySelector('#viewer'),
                  initialEditType: "markdown",
                  initialValue: document.querySelector("#viewers").value
                  });



  // paging 관련.
  var actionForm = $('#actionForm');
  var currentPageNum = $('.currentPageNum').val();

  //페이징 이전,다음 ===> 미완성.
  function replyPaging(num){

  console.log('페이징 가보자고.');

  $.ajax({
      url:'/boot/team/prareplyList2',
      type:'post',
      data:{
         
          "noticeNo" :  $('#noticeNo').val(),
          "pageNum" :num,
          "amount" : $('#amount').val()
      },
      success: function(result){
      
       
          if(result != null){
              console.log('페이징 가보자고.');
              noticeList(num);

          }else {
              alert('내용을 입력해주세요');
          }

      },
      error: function(){
          alert('ajax 댓글페이징 실패');
          console.log('실패')
      }
      })
   
  }
 // })







  //댓글리스트 ajax 구현
  function noticeList(pageNo){
     
      let replyList = document.getElementById('ShowReply');
      var str = "";
      var str1 = "";
      $.ajax({
      url:'/boot/team/prareplyList',
      type:'post',
      data:{
          "noticeNo" :  $('#replyNoticeNo').val(),
          "pageNum" : pageNo
      },
      success: function(result){
      
          var mem = $('#memName1').val();
          var memNo1 = $('#memNo1').val();
          
          if(result != null){

              $('.ShowReply').empty();
              $.each(result, function (index, item) {
                  //날짜 포맷.
                  var td=new Date(item.commentDate).toISOString().substring(0,10);	
                 
                  str += "<div class='ComuDiv'>";
                  str += "<input type='hidden' value='"+item.memNo+"' name='itemMemNo'  class='itemMemNo'>";
                  str += "<input type='hidden' value='"+item.TeamNo+"' name='itemTeamNo'  class='itemTeamNo'>";
                  str += "<input type='hidden' value='"+item.commentNo+"' name='itemCommentNo'  class='itemCommentNo'>";

                  str += "<div style='display: flex; margin-left: 30px;'>";
                  str += "<p style='width: 100px;'  >"+item.name+"</p>";
                  str += "<p>"+td+"</p>";
                  //str += "<p>[[${#dates.format(td, 'yyyy-MM-dd')}]]</p>"
                  str += "</div>";
                  str += "<div style='background-color: #eee; height: 80px; width: 100%;' class='contentArea'><br>";
                  str += "<p style='margin-left: 60px;' class='replyContent' >"+item.commentContent+"</p></div><br> "; 
                  //수정 버튼 클릭시 textarea show
                  str += "<div class='ComutextArea'><textarea style='width: 600px; display:none;' name='content1' class='content1' placeholder='"+item.commentContent+"'>"+item.commentContent+"</textarea></div>";
                  if(memNo1==item.memNo){
                  str += "<div style='justify-content: flex-end; margin-left: 30px; text-align:right ;'>";  
                  str += "<a style='display: inline-block; margin-right: 5px;' class='ReplyMod11'>";
                  str += "<button id='ReplyMod' class='ReplyMod' onclick='ButtonChange("+item.commentNo+",this)'>수정</button><button class='DeleteReply' onclick='deleteReply(this)'>삭제</button></a>";
                  str += "<button class='DoReplyMod' style='display:none;' onclick='modReply(this)'>확인</button>";
                  str += "</div>";
      
                 }else{
                  str += "<div style='justify-content: flex-end; margin-left: 30px; text-align:right ;'>";
                  str += " <a style=' margin-right: 5px;'><button class='button' id='AlertReply' onclick='reportReply(this)'>신고</button></a>";
                  str += "</div>";
                 }
                  
                //모달창
                  str += "<div class='modal1' style='text-align:center; margin: 0 auto; '>";
                  str += "<div class='modal-content1'>";
                  str += "<h2>신고하기</h2><br>";
                  str += "<label style='display: flex; margin: 0 auto; margin-left:220px;'><input type='radio' class='reportReason1' name='commentReportReason'  value='부적절한 내용'>부적절한 내용</label>";
                  str += "<label style='display: flex; margin: 0 auto; margin-left:220px;'><input type='radio' class='reportReason1' name='commentReportReason'  value='음란한 내용'>음란한 내용</label>";
                  str += "<label style='display: flex; margin: 0 auto; margin-left:220px;'><input type='radio' class='reportReason1' name='commentReportReason'  value='폭력적인 내용'>폭력적인 내용</label>";
                  str += "<label style='display: flex; margin: 0 auto; margin-left:220px;'><input type='radio' class='reportReason1' name='commentReportReason'  value='광고'>광고</label>";
                  str += "<br><textarea name='reportContent' id='commentReportContent' rows='4' cols='50' placeholder='신고하실 내용을 입력해 주세요'></textarea>";
                  str += "<div>";
                  str += "<button type='submit' class='reportInModal1'>신고하기</button>";
                  str += "<button type='button' class='reportClose1'>닫기</button>";
                  str += " </div> </div>";
                  str += "</div>";
                  //모달창 끝
                  
                  str += "</div>";
                  
              })
            
                 
             
              $('.ShowReply').append(str);
             
              }else{
                  console.log('ajax 통신은 됬는데 실패')
              }
      },
      error: function(){
        
          console.log('통신실패')
      }
      })


  }

  $('.modal1').on('click', function(){
          $('.modal1').css('display', 'block');
          $('.reportReason1').css('display', 'block');
         
 })
 
  
//댓글 신고
  function reportReply(btn){
    
      var commentNo = $(btn).parent().parent().parent('div.ComuDiv')
                      .children('input.itemCommentNo').val();
      
      // var memNo = $(btn).parent().parent().parent('div.ComuDiv')
      // 			.children('input.itemMemNo').val();
      var practiceNo1 = $('#NoticeNo').val();
      
      
      // console.log(commentNo + 'commentNo');
          //console.log(memNo + 'memNo');
      
      $(btn).parents(".ComuDiv").find(".modal1").css('display', 'block');
      $(btn).parents(".ComuDiv").find(".modal1").children('radio.reportReason1').css('display', 'block');
    
          

      $(document).on('click', '.reportInModal1', function(){
          // ...
          var memNo = $(btn).parent().parent().parent('div.ComuDiv')
                  .children('input.itemMemNo').val();
           var commentReportReason =   $(btn).parents(".ComuDiv").find(".modal1").find("input[name='commentReportReason']:checked").val();
           var commentReportContent = $(btn).parents(".ComuDiv").find(".modal1").children('.modal-content1').children('textarea').val();
           
          
           console.log(memNo);
          console.log(commentReportReason);
          

            $.ajax({
                
                type: "post",
                url: "/boot/team/reportPracticeReply",
                data: {	"noticeNo" : practiceNo1,
                            "memNo" : memNo,
                            "commentNo" : commentNo,
                            "commentReportReason" : commentReportReason,
                             "commentReportContent" : commentReportContent},
                success: function(){ alert('신고완료되었습니다.')},
                error : function(error){
                    console.log(error);
                }
                
              });
            
            $('.modal1').css('display', 'none');
          
      });

      $(document).on('click', '.reportClose1', function(){
          // ...
          $('.modal1').css('display', 'none');
      });
      
      
  }
  


  //댓글 추가
  function addReply(){


      $.ajax({
      url:'/boot/team/pracaddReply2',
      type:'post',
      data:{
          "noticeNo" :  $('#replyNoticeNo').val(),
          //유저에 대한 정보도 넘겨줘야함
          "commentContent" : $('#content').val()
      },
      success: function(result){
      
              noticeList(1);
              $('#content').replaceWith('<textarea placeholder="내용을 입력해 주세요" style="width: 600px;" name="content" id="content">');
      },
      error: function(){
          alert('ajax 댓글달기 실패');
          console.log('실패')
      }
      })        
  }
 

  //댓글 수정버튼 클릭시 버튼 변환
  function ButtonChange(commentNo,btn){
      var textarea = $(this).parent().siblings().find('.ComutextArea textarea');

      //find
      $(btn).parent().parent().parent('div.ComuDiv').children('div.ComutextArea').children('textarea.content1').css('display','');
      $(btn).parent().parent().parent('div.ComuDiv').children('div.contentArea').css('display','none');
      $(btn).hide(); // 수정 버튼 숨기기
      $(btn).parent().find('.DeleteReply').hide(); // 삭제 버튼 숨기기

      $(btn).parent().siblings('button.DoReplyMod').css('display','');

  }

  function deleteReply(btn){
     console.log('삭제 테스트')
      var commentNo = $(btn).parent().parent().parent('div.ComuDiv').children('input.itemCommentNo').val();
      //var memNo = $(btn).parent().parent().parent('div.ComuDiv').children('input.itemMemNo').val();
      
      $.ajax({
      url:'/boot/team/pracdeleteReply',
      type:'post',
      data:{
          "noticeNo" :  $('#replyNoticeNo').val(),
          "commentNo" :commentNo
      },
      success: function(result){
          
          noticeList(1);    

      },
      error: function(){
          alert('ajax 댓글달기 실패');
          console.log('실패')
      }
      })



  }

  
  function modReply(btn){

      var commentNo = $(btn).parent().parent('div.ComuDiv').children('input.itemCommentNo').val();
      //var memNo = $(btn).parent().parent('div.ComuDiv').children('input.itemMemNo').val();
      var text = $(btn).parent().parent('div.ComuDiv').children('div.ComutextArea').children('textarea').val();
      var teamNo = $(btn).parent().parent('div.ComuDiv').children('input.itemTeamNo').val();


      //수정 AJAX
      $.ajax({
      url:'/boot/team/pracmodReply',
      type:'post',
      data:{
          "noticeNo" :  $('#replyNoticeNo').val(),
          "teamNo" :teamNo,
          "commentContent" : text,
          "commentNo" : commentNo
      },
      success: function(result){
      
              noticeList(1);    
      },
      error: function(){
          alert('ajax 댓글달기 실패');
          console.log('실패')
      }
      })


      }//end of modReply()



