/**
 * 
 */


const onUploadImage = async (blob, callback) => {
    const formData = new FormData();
    formData.append("image", blob);
    formData.append("uri", window.location.pathname);
    
     $.ajax({
      type: "post",
      url: "/boot/page/image",
      async: true,
      data: formData,
      processData: false,
      contentType: false,
      success: function (data) {
        imageURL = data;
        callback(imageURL, "image");
        console.log(imageURL)
      },
      error: function (request, status, error) {
        alert(request + ", " + status + ", " + error);
      },
    });
  };

  const editor = new toastui.Editor({
    el: document.querySelector("#editor"),
    height: "500px",
    initialEditType: "wysiwyg",
    placeholder: "내용을 입력해주세요",
    language: "ko-kr",
    hooks: {
      addImageBlobHook: onUploadImage
    }
  });
  
  
  var formObj = $('form[role="form"]')
   $('button[type="submit"]').on('click',function(e){
       e.preventDefault()//기본기능 차단
       console.log('submit click')
       console.log(editor.getHTML())
   var str = "<input type='hidden' name='noticeContent' value='"+editor.getHTML()+"'>"
   formObj.append(str).submit()
})
  